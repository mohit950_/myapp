import React, { Component } from "react";
import {Link} from "react-router-dom";
class NavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <i className="fas fa-book-open mx-3"></i>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
            <Link className="nav-link" to="/home?serachText=Harry Potter">
            Harry Potter
            </Link>
            </li>
            <li className="nav-item">
            <Link className="nav-link" to="/home?serachText=Agatha Christie">
            Agatha Christie
            </Link>
            </li>
            <li className="nav-item">
            <Link className="nav-link" to="/home?serachText=Premchand">
            Premchand
            </Link>
            </li>
            <li className="nav-item">
            <Link className="nav-link" to="/home?serachText=Jane Austen">
            Jane Austen
            </Link>
            </li>
            <li className="nav-item">
            <Link className="nav-link" to="/mybooks">
            My Books
            </Link>
            </li>
            <li className="nav-item">
            <Link className="nav-link" to="/employee">
            Settings
            </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;