import React, {Component} from "react";
import { Route,Switch,Redirect } from "react-router-dom";
import NavBar from "./1navbar.jsx"
import pic from "./library.jpg";
import Home from "./1home.jsx"
import MyBooks from "./mybooks.jsx"
class MainComponent extends Component{
    state={
        addItems:[],
    }
    addItems = (id) => {
        let s1 = {...this.state}
        let index = s1.addItems.findIndex(fi=> fi===id)
        if(index>=0){
            s1.addItems.splice(index,1)
        }else{
            s1.addItems.push(id)
        }
        // this.setState(s1)
    }
    render() {
        return(
            <div className="fluid-container">
                <NavBar />
                <Switch>
                    <Route path="/home"
                    render={(props) => <Home {...props} addItems={this.state.addItems}/>}
                    />
                    <Route path="/mybooks"
                    render={(props) => <MyBooks {...props} addItems={this.state.addItems}/>}
                    />
                </Switch>
            </div>
        )
    }
}
export default MainComponent;