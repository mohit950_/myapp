import React, {Component} from "react";
import pic from "./library.jpg";
import queryString from "query-string"
import httpServices from "./httpServices";
class Home extends Component{
    state={
        data:{},
        book: "",
        fil: {language:"",filter:"",type:""},
        view: 0,
        startIndex: 0,
        maxResults: 8,
        size: 8,
        lang: ["English","French","Hindi","Spanish","Chinese"],
        filt: ["full","partial","free e-Books","paid e-Books"],
        typ: ["all","books","magzines"],
        addItems: [],
        nav: ""
    }
    async bookGet() {
        let queryParams = queryString.parse(this.props.location.search)
        let {serachText,maxResults=8,startIndex=0} = queryParams
        let searchstr = this.makeSearchString(queryParams)
        console.log((`/books/v1/volumes?q=${serachText}`))
        let response = await httpServices.get(`/books/v1/volumes?q=${serachText}`)
        let {data} = response
        this.setState({ data: data ,view:1,nav: serachText})
    }
    AddBook = (it) => {
        let s1 = {...this.state}
        let index = s1.addItems.findIndex(fi=> fi.id===it.id)
        if(index>=0){
            s1.addItems.splice(index,1)
            this.props.addItems.splice(index,1)
        }else{
            s1.addItems.push(it)
            this.props.addItems.push(it)
        }
        this.setState(s1)
    }
    handleChange = (e) => {
        let {currentTarget:input} = e;
        let s1 = {...this.state}
        input.name==="book"
        ?s1.book = input.value
        // :input.name==="language"
        // ?(input.value==="English" ? input.value="en" : input.value==="French" ? input.value="fr" : input.value==="Hindi" ? input.value="hi" : input.value==="Spanish" ? input.value="sp" : input.value==="Chines" ? input.value="chi": "")
        :s1.fil[input.name] = input.value;
        console.log(s1.fil)
        this.setState(s1)
        let {fil,startIndex,book,maxResults} = this.state
        let query = {book: book, language: fil.language, filter: fil.filter, type: fil.type, startIndex: startIndex, maxResults: maxResults}
        this.callURL("/home",query)
        this.getData()
    }
    handleSubmit = (e) => {
        e.preventDefault();
        let {fil,startIndex,book,maxResults} = this.state
        let query = {book: book, language: fil.language, filter: fil.filter, type: fil.type, startIndex: startIndex, maxResults: maxResults}
        this.callURL("/home",query)
        this.getData()
    }
    callURL = (url,options)=> {
        console.log(options)
        let seachstring = this.makeSearchString(options)
        console.log(seachstring)
        console.log(url,seachstring)
        this.props.history.push({
            pathname: url,
            search: seachstring,
        })
    }
    makeSearchString = (options) => {
        console.log(options)
        let {book,startIndex,maxResults,filter,type,language} = options
        let searchString=""
        searchString = this.addQueryString(searchString,"searchText",book)
        searchString = this.addQueryString(searchString,"filter",filter)
        searchString = this.addQueryString(searchString,"language",language)
        searchString = this.addQueryString(searchString,"type",type)
        searchString = this.addQueryString(searchString,"startIndex",startIndex)
        searchString = this.addQueryString(searchString,"maxResults",maxResults)
        console.log(searchString)
        return searchString
    }
    addQueryString = (str,name,value) =>
        value
        ?str
        ?`${str}&${name}=${value}`
        :`${name}=${value}`
        :str;

    makeString = (options) => {
        console.log(options)
        let {book,startIndex,maxResults,filter,type,language} = options
        let searchString=""
        searchString = this.addQueryString(searchString,"q",book)
        searchString = this.addQueryString(searchString,"filter",filter)
        searchString = this.addQueryString(searchString,"language",language)
        searchString = this.addQueryString(searchString,"type",type)
        searchString = this.addQueryString(searchString,"startIndex",startIndex)
        searchString = this.addQueryString(searchString,"maxResults",maxResults)
        console.log(searchString)
        return searchString
    }

    
    async getData () {
        let {fil,startIndex,book,maxResults} = this.state
        let query = {book: book, language: fil.language, filter: fil.filter, type: fil.type, startIndex: this.state.startIndex, maxResults: this.state.maxResults}
        this.makeSearchString(query)
        let searchstr = this.makeString(query)
        console.log(httpServices.get(`/books/v1/volumes?q=Sherlock Homes`))
        let response = await httpServices.get(`/books/v1/volumes?${searchstr}`)
        let { data } = response;
        console.log(response)
        console.log(data)
        this.setState({ data:data,view:1 })

    }
    handlePage = (incr) => {
        let s1 = {...this.state};
        s1.startIndex = s1.startIndex+incr
        s1.maxResults = s1.maxResults+incr
        this.setState(s1)
        this.getData()
    }
    render() {
        let {fil,view,data,lang,filt,typ,startIndex,maxResults,size,nav} = this.state;
        let {book,language,filter,type} = fil
        let {items=[]} = data;

        let queryParams = queryString.parse(this.props.location.search)
        let {serachText} = queryParams
        if(serachText!==nav) this.bookGet()
        // let items1 = items.length>size ? items.filter((lt, index) => index >= startIndex && index <= startIndex)
        // : items;
        console.log(queryParams)
        const im  = {
            position: "relative",
            left: "38%",
            borderRadius: "70%",
            marginBottom: "10px"
        }
        return(
            <div className="container">
                {view===0
                ?
                <React.Fragment>
                <img style={im} src={pic}></img>
                <br/>
                <div className="row">
                    <div className="col-2">
                    </div>
                    <div className="col-6 text-center">
                        <input type="text" name="book" value={book} onChange={this.handleChange} className="form-control" placeholder="Search Book"/>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-primary" onClick={this.handleSubmit}>Search</button>
                    </div>
                </div>
                </React.Fragment>
                :view===1
                ?
                    (
                <div className="row">
                    <div className="col-3">
                    <br/><div className="row bg-light border">
                                <h6>Language</h6>
                        </div>
                            {this.makeRadio(lang,"language")}<br/>
                        <div className="row bg-light border">
                                <h6>Filter</h6>
                        </div>
                            {this.makeRadio(filt,"filter")}<br/>
                        <div className="row bg-light border">
                            <h6>Print Type</h6>
                        </div>
                            {this.makeRadio(typ,"type")}
                    </div>
                    <div className="col-9">
                        <div className="row">
                            <h4 className="text-warning text-center">{book} Books</h4>
                            <h6 className="text-success">{startIndex+1}-{maxResults} Entries</h6>
                            {items.map(it=>
                                <React.Fragment>
                                    <div className="col-3 bg-success border">
                                        {/* <img src={it.volumeInfo.imageLinks.thumbnail} /><br/> */}
                                        {it.volumeInfo.title}<br/>
                                        {it.volumeInfo.authors}<br/>

                                        {this.state.addItems.findIndex(fi=> fi.id===it.id)>=0
                                        ?<button className="btn btn-secondary" onClick={()=>this.AddBook(it)}> Remove to MyBooks</button>
                                        :<button className="btn btn-secondary" onClick={()=>this.AddBook(it)}> Add to MyBooks</button>}
                                    </div>
                                </React.Fragment>
                                )}
                        </div>
                        <div className="row">
                            <div className="col-2">
                                {startIndex>0
                                ?<button className="btn btn-primary" onClick={()=>this.handlePage(size)}>Prev</button>
                                :""}
                            </div>
                            <div className="col-8"></div>
                            <div className="col-2">
                                {maxResults<40}
                                <button className="btn btn-primary" onClick={()=>this.handlePage(size)}>Next</button>
                            </div>
                        </div>
                    </div>
                </div>
                    )

                : ""}

            </div>
        )
    }
    makeRadio = (arr,name) => {
        return(
            <div className="row">

                {arr.map(ar=>
                <React.Fragment>
                <div className="col-12 border">
                    <input type="radio" value={ar} name={name} className="check-input" onChange={this.handleChange} />
                    <label className="form-check-label">{ar}</label>
                </div>
                </React.Fragment>
                )}
            </div>
        )
    }
}
export default Home;