import React, {Component} from "react";
import pic from "./library.jpg";
import httpServices from "./httpServices";
class Book extends Component{
    state={
        lang: ["English","French","Hindi","Spanish","Chinese"],
        filt: ["Full Volume","Partial Volume","Free Google e-Books","Paid Google e-Books"],
        typ: ["All","Bokks","Magzines"]
    }
    render() {
        let {lang,filt,typ} = this.state;
        return(
            <div className="container">
                <div className="row">
                    <div className="col-3">
                    <br/><div className="row bg-light border">
                                <h6>Language</h6>
                        </div>
                            {this.makeRadio(lang,"lang")}<br/>
                        <div className="row bg-light border">
                                <h6>Filter</h6>
                        </div>
                            {this.makeRadio(filt,"filter")}<br/>
                        <div className="row bg-light border">
                            <h6>Print Type</h6>
                        </div>
                            {this.makeRadio(typ,"type")}
                    </div>
                    <div className="col-9">
                        <div className="row">
                            <h4 className="text-warning text-center">Book Name</h4>
                            <h6 className="text-success">Entries</h6>
                            {lang.map(ln=>
                                <React.Fragment>
                                    <div className="col-3">{ln}</div>
                                    <div className="col-3">{ln}</div>
                                    <div className="col-3">{ln}</div>
                                    <div className="col-3">{ln}</div>
                                </React.Fragment>
                                )}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    makeRadio = (arr,name) => {
        return(
            <div className="row">

                {arr.map(ar=>
                <React.Fragment>
                <div className="col-12 border">
                    <input type="radio" value={ar} name={name} className="check-input" />
                    <label className="form-check-label">{ar}</label>
                </div>
                </React.Fragment>
                )}
            </div>
        )
    }
}
export default Book;