import React, {Component} from "react";
import httpServices from "./httpServices";
class MyBooks extends Component{
    state = {
        data:[]
    }
    AddBook = (it) =>{
        console.log(this.props.addItems)
        let index = this.props.addItems.findIndex(fi=> fi.id===it.id)
        if(index>=0) this.props.addItems.splice(index,1)
        this.props.history.push("/mybooks")
    }
    render(){
        console.log(this.props.addItems)
        return(
            <div className="fluid-container">
                {this.props.addItems.length===0
                ?<h4 className="bg-success text-center text-warning">No Books added to My Books</h4>
                :<h4 className="bg-success text-center text-warning">My Books</h4>
                }
                <div className="row">
                    <div className="col-2"></div>
                    {this.props.addItems.map(it=>
                        <div className="col-3 bg-success border">
                        <img className="mx-5"src={it.volumeInfo.imageLinks.thumbnail} /><br/>
                        {it.volumeInfo.title}<br/>
                        {it.volumeInfo.authors}<br/>
                        {this.props.addItems.findIndex(fi=> fi.id===it.id)>=0
                        ?<button className="btn btn-secondary" onClick={()=>this.AddBook(it)}> Remove to MyBooks</button>
                        :<button className="btn btn-secondary" onClick={()=>this.AddBook(it)}> Add to MyBooks</button>}
                        </div>
                    )}
                    </div>
            </div>
        )
    }
}
export default MyBooks;