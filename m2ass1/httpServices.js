import axios from "axios"
// https://www.googleapis.com
const baseURL = "https://www.googleapis.com";

function get(url) {
    console.log(axios.get(baseURL + url),"AXIOS")
    return axios.get(baseURL + url)
}

function post(url,obj) {
    console.log(baseURL + url , obj)
    return axios.post(baseURL + url , obj)
}

export default {
    get,
    post,
}