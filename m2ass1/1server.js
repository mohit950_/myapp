var express = require("express");
let app = express();
app.use(express.json());
app.use(function (req, res, next){
    res.header("Access-Control-Allow-Origin","*");
    res.header(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTION, PUT, DELETE, PATCH, HEAD"
    );
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});
var port = process.env.PORT || 2410;
app.listen(port, () => console.log(`Node app listening on port ${port}!`))

app.get("/books/v1/volumes", function(req,res){
    console.log("books")
    let searchText = req.query.searchText;
    res.send("books")
})