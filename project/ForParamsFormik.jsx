import React, { Component } from "react";
import { Formik, Field, Form, FieldArray, ErrorMessage } from "formik";
import * as yup from "yup";

class ListForm extends Component {
  render() {
    const { ParamsArr } = this.props;
    // const { index } = this.props.match.params;
    let list = ParamsArr;
    return (
      <Formik
        initialValues={{
          name: list.key || "",
          items: list.valueParam || "",
          qty: list.qty || "",
        }}
        // validationSchema={ListValidationSchema}
        onSubmit={(values, index) => {
          console.log(values, index, "form 16");
          this.props.onSubmit(values, this.props.match.params.index);
          this.props.history.push("/");
        }}
      >
        {({ values, errors }) => (
          <Form>
            {console.log(errors, 33, "mohit")}
            <h6>Details of Shopping Lists</h6>
            <div className="form-group">
              <label>Name</label>
              <Field
                type="text"
                name="name"
                className="form-control"
                placeholder="Name of List"
              />
            </div>
            <div className="form-group text-danger">
              <ErrorMessage name="name" />
            </div>

            <FieldArray
              name="items"
              render={(arrayHelpers) => (
                <div>
                  {values.items.map((p1, index) => (
                    <div className="row mb-2" key={index}>
                      <div className="col-7">
                        <Field
                          name={`items[${index}].item`}
                          className="form-control"
                          type="text"
                          placeholder="Product Name"
                        />
                      </div>
                      <div className="col-2 align-middle">
                        <button
                          className="btn btn-warning mr-2"
                          type="button"
                          onClick={() => arrayHelpers.remove(index)}
                        >
                          Delete
                        </button>
                      </div>
                      <div className="form-group">
                        <label>Quantity</label>
                        <Field
                          type="number"
                          name="qty"
                          className="form-control"
                          placeholder="Name of List"
                        />
                      </div>
                      <div className="form-group text-danger">
                        <ErrorMessage name="qty" />
                      </div>
                    </div>
                  ))}
                  <button
                    className="btn btn-success mr-2"
                    type="button"
                    onClick={() => arrayHelpers.push("")}
                  >
                    Add Items to Shopping List
                  </button>
                  <div className="form-group text-danger">
                    {typeof errors.items === "string"
                      ? errors.items
                      : errors.items
                      ? errors.items.reduce(
                          (acc, curr) => (acc ? acc : curr ? curr.item : acc),
                          ""
                        )
                      : ""}
                  </div>
                </div>
              )}
            />
            <div className="form-group">
              <button type="submit" className="btn btn-primary m-2">
                {index ? "Update" : "Add"}
              </button>
            </div>
          </Form>
        )}
      </Formik>
    );
  }
}
export default ListForm;














<Formik
        initialValues={{
        }}
        // validationSchema={ListValidationSchema}
        onSubmit={(values, index) => {
          console.log(values, index, "form 16");
          this.props.onSubmit(values, this.props.match.params.index);
          this.props.history.push("/");
        }}
      >
        {({ values, errors }) => (
                <Form>
                <FieldArray
              name="items"
              render={(arrayHelpers) => (
                <div>
                  {values.items.map((p1, index) => (
                    <div className="row mb-1" key={index}>
                      <div className="col-4">
                        <Field
                          name={`items[${index}].item`}
                          className="form-control"
                          type="text"
                          placeholder="Product Name"
                        />
                      </div>
                      {/* <div className="col-2 align-middle">
                        <button
                          className="btn btn-warning mr-2"
                          type="button"
                          onClick={() => arrayHelpers.remove(index)}
                        >
                          Delete
                        </button>
                      </div> */}
                      <div className="form-group col-4">
                        {/* <label>Value</label> */}
                        <Field
                          type="number"
                          name="qty"
                          className="form-control"
                          placeholder="Name of List"
                        />
                      </div>

                      <div className="form-group col-4">
                        {/* <label>Description</label> */}
                        <Field
                          type="number"
                          name="qty"
                          className="form-control"
                          placeholder="Name of List"
                        />
                      </div>
                    </div>
                  ))}
                  {/* <button
                    className="btn btn-success mr-2"
                    type="button"
                    onClick={() => arrayHelpers.push("")}
                  >
                    Add Items to Shopping List
                  </button> */}
                  
                </div>
              )}
            />
                </Form>
                )}
                </Formik>
