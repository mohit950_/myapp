import axios from "axios"
// const baseURL = "http://localhost:2410";
const baseURL = "https://mysterious-headland-18196.herokuapp.com";

function get(url) {
    // console.log(axios.get(url),"AXIOS")
    console.log(baseURL + url,"AXIOS")
    return axios.get(baseURL + url)
}

function post(url,obj) {
    console.log(baseURL + url , obj)
    return axios.post(baseURL + url , obj)
}

export default {
    get,
    post,
}

