
import React, {Component} from "react";
import {Modal,Button} from "react-bootstrap"
class ModalsavedURL extends Component{
  state={
    txt : this.props.txt
  }
  
  render(){
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h6 id="contained-modal-title-vcenter" className="text-secondary">
            Saved Url's
          </h6>
        </Modal.Header>
        <Modal.Body>
          
              {this.props.modalSave.length>=1
            ?
            <React.Fragment>
            <div className="row">
            <div className="col-4">REQUEST</div>
            <div className="col-8">URLS</div>
          </div>
          <div className="row"> 
            {this.props.modalSave.map(ur=>
            <React.Fragment>
            <div className="col-4">{ur.request}</div>
            <div className="col-8">{ur.url}</div>
            </React.Fragment>
            )}
          </div>
          </React.Fragment>

            : "No request are Saved Yet" }
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
    
export default ModalsavedURL;