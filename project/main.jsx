import React, {Component} from "react";
import httpservices from "./httpservices"
import ModalPOpup from "./modal_popup"
import ModalsavedURL from "./savedurlPOPUP"
import {Formik,Form,FieldArray,Field} from "formik"
class MainComponent extends Component{
    state={
        txt: "",
        Body: false,
        Headers: false,
        Params: true,
        ParamsArr: [],
        request: "get",
        options:[{name:"GET",value:"get"},{name: "POST",value:"post"},{name:"PUT",value:"put"},{name: "DELETE",value:"delete"}],
        bodyOptions: ["none","raw"],
        bodyInput: "",
        bodySelector: "none",
        data:"",
        headersKey: "",
        headersValue: "",
        headersDescription: "",
        sampleJSOn: '{"name":"Jack Smith","age": 22}',

        modalPopUp: false,
        modalSave: [],
        savedURL: false,

    }
    handleChange = (e) => {
        let {currentTarget:input} = e;
        let s1 = {...this.state}
        s1[input.name] = input.value;
        console.log(s1.bodyInput,"bodyinput")
        

        s1.ParamsArr.splice(0)
        if(s1.ParamsArr.length===0){
            let index1 = s1.txt.indexOf("?",0)
            let jsonPrams={key:"",value:""}
            if(index1>=0){
                let indexEqual1 = s1.txt.indexOf("=",index1+1)
                let indexAnd = s1.txt.indexOf("&",indexEqual1)
                console.log(indexAnd)
                console.log(indexEqual1)
                if(indexAnd>indexEqual1) jsonPrams = {key : s1.txt.substring(index1+1,indexEqual1), value: s1.txt.substring(indexEqual1+1,indexAnd)}
                else if(indexAnd<indexEqual1) jsonPrams = {key : s1.txt.substring(index1+1,indexEqual1), value: s1.txt.substring(indexEqual1+1,s1.txt.length)}
                s1.ParamsArr.push(jsonPrams)
            }
        }
        if(s1.txt.indexOf("&",0)>=1){
            let startPose=0
            for(let i=0; i<s1.txt.match(/&/gi).length; i++){    
                console.log(startPose,"startPose")
                let indexAnd = s1.txt.indexOf("&",startPose)
                console.log(indexAnd,"indexAnd")
                if(s1.ParamsArr.length>=1){
                    let jsonPrams={key:"",value:""}
                    console.log(jsonPrams,"JSONPARAMS")
                    if(indexAnd>=0){
                        let indexEqual1 = s1.txt.indexOf("=",indexAnd+1)
                        let indexAnd1 = s1.txt.indexOf("&",indexAnd+1)
                        console.log(indexAnd1,"indexAnd1")
                        console.log(indexEqual1,"indexEqual1")

                        if(indexAnd1>indexEqual1) jsonPrams = {key : s1.txt.substring(indexAnd+1,indexEqual1), value: s1.txt.substring(indexEqual1+1,indexAnd1)}
                        else if(indexAnd1<indexEqual1) jsonPrams = {key : s1.txt.substring(indexAnd+1,indexEqual1), value: s1.txt.substring(indexEqual1+1,s1.txt.length)}

                        console.log(jsonPrams,68)
                        s1.ParamsArr.push(jsonPrams)
                    }
                    startPose = startPose+indexAnd+1
                }
            }
        }
        console.log(s1.ParamsArr,75)


        this.setState(s1)
    }
    show = (str) => {
        let s1 = {...this.state}
        if(str==="Headers"){
            s1.Params = false
            s1.Body = false
            s1.Headers = true
        }
        else if(str==="Body"){
            s1.Params = false
            s1.Headers = false
            s1.Body = true
        }
        else if(str==="Params"){
            s1.Headers = false
            s1.Body = false
            s1.Params = true
        }
        this.setState(s1)
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let {request,txt,bodyInput,headersKey,headersValue,headersDescription,ParamsArr} = this.state
        // ParamsArr.splice(0)
        // if(ParamsArr.length===0){
        //     let index1 = txt.indexOf("?",0)
        //     let jsonPrams;
        //     if(index1>=0){
        //         let indexEqual1 = txt.indexOf("=",index1+1)
        //         let indexAnd = txt.indexOf("&",indexEqual1)
        //         console.log(indexAnd)
        //         console.log(indexEqual1)
        //         if(indexAnd>indexEqual1) jsonPrams = {key : txt.substring(index1+1,indexEqual1), value: txt.substring(indexEqual1+1,indexAnd)}
        //         else if(indexAnd<indexEqual1) jsonPrams = {key : txt.substring(index1+1,indexEqual1), value: txt.substring(indexEqual1+1,txt.length)}
        //         ParamsArr.push(jsonPrams)
        //     }
        // }
        // if(txt.indexOf("&",0)>=1){
        //     let startPose=0
        //     for(let i=0; i<txt.match(/&/gi).length; i++){    
        //         console.log(startPose,"startPose")
        //         let indexAnd = txt.indexOf("&",startPose)
        //         console.log(indexAnd,"indexAnd")
        //         if(ParamsArr.length>=1){
        //             let jsonPrams;
        //             console.log(jsonPrams,"JSONPARAMS")
        //             if(indexAnd>=0){
        //                 let indexEqual1 = txt.indexOf("=",indexAnd+1)
        //                 let indexAnd1 = txt.indexOf("&",indexAnd+1)
        //                 console.log(indexAnd1,"indexAnd1")
        //                 console.log(indexEqual1,"indexEqual1")

        //                 if(indexAnd1>indexEqual1) jsonPrams = {key : txt.substring(indexAnd+1,indexEqual1), value: txt.substring(indexEqual1+1,indexAnd1)}
        //                 else if(indexAnd1<indexEqual1) jsonPrams = {key : txt.substring(indexAnd+1,indexEqual1), value: txt.substring(indexEqual1+1,txt.length)}

        //                 console.log(jsonPrams)
        //                 ParamsArr.push(jsonPrams)
        //             }
        //             startPose = startPose+indexAnd+1
        //         }
        //     }
        // }

        console.log(bodyInput,"bodyInput2")
        // let body1 = {}
        let head = {key : headersKey, value: headersValue, description: headersDescription}
        if(bodyInput){
            let body1 = JSON.parse(bodyInput)
            let sendData = {request: request,url: txt,body: body1,headers:head}
            this.postData("/request",sendData)
        }
        else{
            let sendData = {request: request,url: txt,headers:head}
            this.postData("/request",sendData)
        }
        this.setState({Params:true})
        // let head = {key: headersKey,value:headersValue,description: headersDescription}
        // console.log(head)
        // console.log(body1,41)
        // let sendData = {request: request,url: txt,body: body1}
        // this.postData("/request",sendData)
        // console.log(this.state.txt)
        // if(this.state.request==="GET"){
        //     this.getData(this.state.txt)
        // }
        // else if(this.state.request==="POST"){
        //     this.postData(this.state.txt)
        // }
    }
    async postData(url,sendData){
        let response = await httpservices.post(url,sendData)
        console.log(response,"response")
        let {data} = response
        // console.log(data)
        let data1 = JSON.stringify(data)
        this.setState({data:data1})
    }
    onHide = () => {
        this.setState({modalPopUp:false})
    }
    onHideSavedUrl = () => {
        this.setState({savedURL:false})
    }

    save = (requestTitle) =>{
        console.log(requestTitle)
        let s1 = {...this.state}
        s1.modalSave.push(requestTitle)
        this.setState(s1)
        this.onHide()

        // console.log(requestTitle,91)
        // let {modalSave} = this.state;
        // let modalSave1 = [...modalSave]
        // console.log(modalSave1,"modalSave1 94")
        // modalSave1 = modalSave1.push(requestTitle)
        // console.log(modalSave1,"modalSave1")
        // this.setState({modalSave:modalSave1})
    }
    savedURL = () => {
        let s1 = {...this.state}
        if(s1.savedURL===false) s1.savedURL=true
        else s1.savedURL=false;
        this.setState(s1)
    }
    render() {
        let {txt,options,request,bodyOptions,Params,Headers,Body,data,bodySelector,sampleJSOn,ParamsArr} = this.state
        // let data1 = JSON.parse(data)
        let disable = txt ? false : true;
        console.log(this.state.modalSave)
        return(
            <div className="container">
                {console.log(bodySelector)}
                <h4 className="text-center text-warning"><i className="fab fa-quinscape"></i> Mini Postman <i className="fab fa-quinscape"></i></h4>
                <div className="row">
                    <div className="col-9">
                        {txt ? <h6 className="bg-light" style={{fontWeight:"bold"}}>{txt}</h6> : <h6 className="bg-light" style={{fontWeight:"bold"}}>Untitled Request</h6>}
                    </div>
                    <div className="col-1">
                        <button className="btn btn-outline-secondary btn-sm" onClick={()=>this.setState({modalPopUp:true})}>
                        <i className="fas fa-save"></i> Save
                        </button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-outline-secondary btn-sm" onClick={()=>this.savedURL()}>Saved Url's</button>
                    </div>
                </div><br/>
                
                {/* ///////////////////////////////////// */}
                {/* {modalPopUp ? <ModalPOpup /> : ""} */}

                <ModalPOpup show={this.state.modalPopUp} onHide={this.onHide} txt={txt} request={request} save={this.save}/>
                <ModalsavedURL show={this.state.savedURL} onHide={this.onHideSavedUrl} modalSave={this.state.modalSave}/>

                {/* /////////////////////////////////////// */}
<div className="container">
    <div className="row">
        <div className="col-10">
                <div class="wrapper">
                    <div class="search_box">
                        <div class="dropdown">

                        <select className="bg-light form-control" style={{border:"none"}} id="request" name="request" onChange={this.handleChange}>
                            {options.map(opt=><option value={opt.value}>{opt.name}</option>)}<i className="fa-arrow-down"></i>
                        </select>
                            {/* <div class="default_option">All</div>  
                            <ul>
                            <li>All</li>
                            <li>Recent</li>
                            <li>Popular</li>
                            </ul> */}
                        </div>
                        <div class="search_field">
                        <input type="text" id="txt" name="txt" className="input bg-light form-control" onChange={this.handleChange} />
                        
                    </div>
                    </div>
                </div>
        </div>
        <div className="col-2" style={{marginTop:"12px"}}>
            <button className="btn btn-warning" onClick={this.handleSubmit} disabled={disable}>Submit</button>
        </div> 
    </div>
</div>
<br/>
                {/* <div className="row">
                    <div className="col-2">
                        <select className="bg-light form-control" id="request" name="request" onChange={this.handleChange}>
                            {options.map(opt=><option value={opt.value}>{opt.name}</option>)}<i className="fa-arrow-down"></i>
                        </select>
                    </div>
                    <div className="col-8">
                        <input type="text" id="txt" name="txt" className="bg-light form-control" onChange={this.handleChange} />
                    </div>
                    <div className="col-2">
                        <button className="btn btn-warning" onClick={this.handleSubmit} disabled={disable}>Submit</button>
                    </div>                    
                </div> */}

                <div className="row" style={{borderBottom:"1px solid black"}}>
                    <div className="col-1">
                        <label className={Params ? "text-warning" : ""} onClick={()=>this.show("Params")}>Params</label>
                    </div>
                    <div className="col-1">
                        <label className={Headers ? "text-warning" : ""} onClick={()=>this.show("Headers")}>Headers</label>
                    </div>
                    <div className="col-1">
                        <label className={Body ? "text-warning" : ""} onClick={()=>this.show("Body")}>Body</label>
                    </div>
                </div>
                {bodySelector==="raw"
                ?<div className="container">
                    <label className="text-danger" style={{fontWeight: "lighter"}}>Sample JSON {sampleJSOn}</label>
                    <label className="text-dark" style={{fontWeight: "lighter", fontSize: "10px"}}>Please type in this manner</label>
                </div>
                :""}

{/* /////////////////this is for Params Tag\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */}

{Params
                ?
                <div className="container">
                <div className="row">
                    <div className="col-4 border">
                        <label style ={{fontWeight: "bold"}}>Key</label>
                    </div>
                    <div className="col-4 border">
                        <label style ={{fontWeight: "bold"}}>Value</label>
                    </div>
                    <div className="col-4 border">
                        <label style ={{fontWeight: "bold"}}>Description</label>
                    </div>
                </div>
                {ParamsArr.length===0?
                <div className="row">
                <div className="col-4 border">
                    <input type="text" style={{border: "none"}} className="form-control" placeholder="Key" name="ParamsKey" onChange={this.handleChange}/>
                </div>
                <div className="col-4 border">
                    <input type="text" style={{border: "none"}} className="form-control" placeholder="Value" name="ParamsValue" onChange={this.handleChange}/>
                </div>
                <div className="col-4 border">
                    <input type="text" style={{border: "none"}} className="form-control" placeholder="Description" name="ParamsDescription" onChange={this.handleChange}/>
                </div>
            </div>
                :ParamsArr.map(pr=>
                <React.Fragment>
                    <div className="row">
                        <div className="col-4 border">
                            <input type="text" style={{border: "none"}} className="form-control" placeholder="Key" name="ParamsKey" value={pr.key} onChange={this.handleChange}/>
                        </div>
                        <div className="col-4 border">
                            <input type="text" style={{border: "none"}} className="form-control" placeholder="Value" name="ParamsValue" value={pr.value} onChange={this.handleChange}/>
                        </div>
                        <div className="col-4 border">
                            <input type="text" style={{border: "none"}} className="form-control" placeholder="Description" name="ParamsDescription" onChange={this.handleChange}/>
                        </div>
                    </div>
                    </React.Fragment>
                )}
                </div>
                : <div className="container"></div>
    }

{/* /////////////////this is for Headers Tag\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */}
                {Headers
                ?
                <div className="container">
                <div className="row">
                    <div className="col-4 border">
                        <label style ={{fontWeight: "bold"}}>Key</label>
                    </div>
                    <div className="col-4 border">
                        <label style ={{fontWeight: "bold"}}>Value</label>
                    </div>
                    <div className="col-4 border">
                        <label style ={{fontWeight: "bold"}}>Description</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-4 border">
                        <input type="text" style={{border: "none"}} className="form-control" placeholder="Key" name="headersKey" onChange={this.handleChange}/>
                    </div>
                    <div className="col-4 border">
                        <input type="text" style={{border: "none"}} className="form-control" placeholder="Value" name="headersValue" onChange={this.handleChange}/>
                    </div>
                    <div className="col-4 border">
                        <input type="text" style={{border: "none"}} className="form-control" placeholder="Description" name="headersDescription" onChange={this.handleChange}/>
                    </div>
                </div>
                </div>
                : <div className="container"></div>
    }



{/* /////////////////this is for Body Tag\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */}
                {Body
                ?
                <div className="container">
                <div className="row">
                    {bodyOptions.map(opt=>
                        <div className="col-1">
                            <React.Fragment>
                            <input type="radio" style={{marginRight: "10px"}} value={opt} name="bodySelector" checked={opt===bodySelector} onChange={this.handleChange}/>
                            <label style={{marginBottom: "-10px"}}>{opt}</label>
                            </React.Fragment>
                        </div>
                        )}
                        <div className="col-1">
                            {bodySelector==="raw" ? <select><option>JSON</option></select> : ""}
                        </div>
                        {bodySelector==="raw" ? 
                            <textarea row="5" style={{marginTop: "2px",marginBottom: "2px",height: "136px"}} name="bodyInput" onChange={this.handleChange}/>
                        :""}
                </div>
                
                </div>
                : <div className="container"></div>
    }

        <div className="row" style={{borderTop:"1px solid black"}}>
            <p>{data}</p>
        </div>

            </div>
        )
    }
}
export default MainComponent;