import React, {Component} from "react";
import {Modal,Button} from "react-bootstrap"
class ModalPOpup extends Component{
  state={
    txt : this.props.txt
  }
  handleChange=(e)=>{
    let {currentTarget:input} = e;
        let s1 = {...this.state}
        console.log(s1.txt,7)
        s1[input.name] = input.value;
        console.log(s1)
        this.setState(s1)
  }
  save=()=>{
    let send = {request: this.props.request, url : this.props.txt}
    this.props.save(send)
  }
  
  render(){
    let {txt} = this.state;
    console.log(txt,"state")
    console.log(this.props.txt)
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <h6 id="contained-modal-title-vcenter" className="text-secondary">
            Save Request
          </h6>
        </Modal.Header>
        <Modal.Body>
          <h6 className="text-secondary">Request Name</h6>
          <input type="text" id="txt" name="txt" value={this.props.txt} className="bg-light form-control" onChange={this.handleChange} /> 
          <p className="text-secondary">
            Your Security is Our Priority
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onHide}>Close</Button>
          <Button variant="warning" onClick={()=>this.save()}>Save</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
    
export default ModalPOpup;
