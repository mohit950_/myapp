const { default: axios } = require("axios");
var express = require("express");
let app = express();
app.use(express.json());
app.use(function (req, res, next){
    res.header("Access-Control-Allow-Origin","*");
    res.header(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTION, PUT, DELETE, PATCH, HEAD"
    );
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});
var port = process.env.PORT || 2410;
app.listen(port, () => console.log(`Node app listening on port ${port}!`))
// let baseURL = "https://quiet-springs-47127.herokuapp.com/messageServer";
// let axios = require("axios")

app.post("/request", function(req,res){
    let body = req.body
    console.log(body)
    if(body.request==="get"){
        let value = body.headers.value
        let key = body.headers.key
        axios[body.request](body.url,{headers : {key : value}})
        .then((response)=>{
            console.log(response.data)
            // console.log(req.headers())
            res.send(response.data)
        })
        .catch((error)=>{
            if(error.response){
                // console.log(error)
                let {status,statusText} = error.response
                // let {IncomingMessage} = error.res
                console.log(status,statusText)
                let send = {statusCode : status, statusMessage:statusText}
                // let data1 = JSON.parse(data)
                res.send(send)
            }else{
                console.log("bad Request")
                res.send("Bad Request")
                // res.status(400).send("bad Request")
            }

        })
    }
    else if(body.request==="post"){
        let value = body.headers.value
        let key = body.headers.key
        console.log(value,key)
        axios[body.request](body.url,body.body, {headers : {key : value} })
        .then((response)=>{
            console.log(response.data)
            res.send(response.data)
        })
        .catch((error)=>{
            if(error.response){
                let {status,statusText,data} = error.response
                console.log(status,statusText,data,"post")
                res.send(statusText)

            }else{
                console.log("bad Request")
                res.send("Bad Request")
                // res.status(404).send("bad Request")
            }

        })
    }

    else if(body.request==="put"){
        let value = body.headers.value
        let key = body.headers.key
        console.log(value,key)
        axios[body.request](body.url,body.body, {headers : {key : value} })
        .then((response)=>{
            console.log(response.data)
            res.send(response.data)

        })
        .catch((error)=>{
            if(error.response){
                let {status,statusText} = error.response
                console.log(status,statusText)
                res.send(statusText)

            }else{
                console.log("bad Request")
                res.send("Bad Request")
                // res.status(404).send("Not found")
                // res.status(404).send("bad Request")
            }

        })
    }

    else if(body.request==="delete"){
        let value = body.headers.value
        let key = body.headers.key
        console.log(value,key)
        axios[body.request](body.url, {headers : {key : value} })
        .then((response)=>{
            console.log(response.data)
            res.send(response.data)
        })
        .catch((error)=>{
            if(error.response){
                let {status,statusText} = error.response
                console.log(status,statusText)
                res.send(statusText)
            }else{
                console.log("bad Request")
                res.send("Bad Request")
                // res.status(404).send("Not found")
                // res.status(404).send("bad Request")
            }

        })
    }
})