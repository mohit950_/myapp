import React, {Component} from "react";
class Receive extends Component{
    state={
        receive: this.props.receive,
        product: this.props.product,
        DDyear:[],
        DDmonth:["January","February","March","April","May","June","July","August","September","October","November","December"],
        DDday:[],
        mth:""

    }
    render(){
        let {code,quantity,year,month,day} = this.state.receive
        let {DDyear,DDday,DDmonth,mth} = this.state
        DDyear=[]
        DDday=[]
        console.log(mth)
        let {catArr,Food,Apparel,PersonalCare,handleView,editIndex} = this.props
        let prdArr= this.state.product.reduce((acc,curr) => acc.find(fi=>fi===curr.code) ? acc : [...acc,curr.code] ,[])
        for(let i=1900; i<=2025; i++){
            DDyear.push(i)
        }
        if(year && ((+year)%4===0 && month==="February")){
            console.log(year)
            for(let i=1; i<=29; i++){
                DDday.push(i)
            }
        }else if(year && ((+year)%4>0 && month==="February")){
            for(let i=1; i<=28; i++){
                DDday.push(i)
            }
        }else if(year && (year && (month=="August" || month=="July" || DDmonth.findIndex(mon=> mon===month)%2==0))){
            for(let i=1; i<=31; i++){
                DDday.push(i)
            }
        }else if(year && (DDmonth.findIndex(mon=> mon===month)%2==1)){
            for(let i=1; i<=30; i++){
                DDday.push(i)
            }
        }
        return(
            <React.Fragment>
                <h6>Select the Product Whose Stock has been received</h6>
            {this.makeDD(prdArr,"code","Select the Product Code",code)}
            <label>Stock Received</label>
            {this.textField("Enter Product quantity","quantity",quantity)}
            <br/>
            <div className="row">
                <div className="col-3">
                    {this.makeDD(DDyear,"year","Select Year",year)}
                </div>
                <div className="col-3">
                    {this.makeDD(DDmonth,"month","Select Month",month)}
                </div>
                <div className="col-3">
                    {this.dayDD(DDday,"day","Select Day",day,month,mth)}
                </div>
                <div className="col-3">
                </div>
            </div>

                <button className="btn btn-primary m-2" onClick={this.handleSubmit}>Submit</button><br/>
                <button className="btn btn-primary m-2" onClick={() => handleView(0)}>Go to HomePage</button>
            </React.Fragment>

        )
    }
    handleSubmit = (e) => {
        console.log("mohit")
        e.preventDefault();
        this.props.onReceive(this.state.receive)
    }
    textField = (label,name,value) => {
        return(
        <React.Fragment>
                <input
                type="text"
                className="form-control"
                name={name}
                id={name}
                value={value}
                placeholder={label}
                onChange={this.handleChange}
                />

                </React.Fragment>
        )
    }
    makeDD = (arr,name,label,value,month,year) => {
        let disable = year && month ? true : false
        return(
            <select id={name} name={name} value={value} onChange={this.handleChange} className="form-control" disabled={disable}>
                <option value="" disabled>{label}</option>
                {arr.map(ar=>
                    <option>{ar}</option>
                    )}
            </select>
        )
    }
    dayDD = (arr,name,label,value,month,mth) => {
        return(
            <select id={name} name={name} value={value} onChange={this.handleChange} className="form-control">
                <option value="" disabled>{label}</option>
                {arr.map(ar=>
                    <option>{ar}</option>
                    )}
            </select>
        )
    }
    handleChange = (e) => {
        let {currentTarget:input} =e;
        let s1 = {...this.state};
        if(s1.mth===s1.receive.month){}
        else{
            s1.receive.day=""
        }
        s1.receive[input.name] = input.value
        console.log(s1.receive)
        s1.mnth = s1.receive.month 
        this.setState(s1)
    }
}
export default Receive;
