import React, {Component} from "react";
import NavBar from "./navbar"
import ShowProduct from "./showProduct"
import AddProduct from "./addProduct"
import Receive from "./receiveProduct"
class MainComponent extends Component{
    state={
        Food:["Nestle", "Haldiram", "Pepsi", "Coca Cola", "Britannia", "Cadburys"],
        Apparel:["P&G", "Colgate", "Parachute","Gillete", "Dove"],
        PersonalCare: ["Levis", "Van Heusen", "Manyavaar", "Zara"],
        Brands:["Nestle", "Haldiram", "Pepsi", "Coca Cola", "Britannia", "Cadburys",
        "P&G", "Colgate", "Parachute","Gillete", "Dove",
        "Levis", "Van Heusen", "Manyavaar", "Zara"],
        Products:[
            {
            code: "PEP1253", price: 20, brand: "Pepsi", category: "Food",
            specialOffer: false, limitedStock: false, quantity: 25
            },
            {
            code: "MAGG021", price: 25, brand: "Nestle", category: "Food",
            specialOffer: true, limitedStock: true, quantity: 10
            },
            {
            code: "LEV501", price: 1000, brand: "Levis", category: "Apparel",
            specialOffer: true, limitedStock: true, quantity: 3
            },
            {
            code: "CLG281", price: 60, brand: "Colgate", category: "Personal Care",
            specialOffer: true, limitedStock: true, quantity: 5
            },
            {
            code: "MAGG451", price: 25, brand: "Nestle", category: "Food",
            specialOffer: true, limitedStock: true, quantity: 0
            },
            {
            code: "PAR250", price: 40, brand: "Parachute", category: "Personal Care",
            specialOffer: true, limitedStock: true, quantity: 5
            }
        ],
        view: 0,
        editIndex:-1,
    };
    render() {
        let {view,Products,Brands,editIndex,Food,Apparel,PersonalCare} = this.state;
       let product = {code:"",price:"", category:"",brand:"",limitedStock:"",specialOffer:"",quantity:0}
        let receive = {code:"",quantity:"",year:"",month:"",day:""}
        let catArr = Products.reduce((acc,curr)=>acc.find(ac=> ac===curr.category) ? acc : [...acc,curr.category] ,[])
        return (
            <div className="container">
            <React.Fragment>
            <NavBar Products={Products} />
            {view===0 
            ? <ShowProduct product={Products} handleView={this.handleView} edit={this.handleEdit} /> 
            : view===1
            ? <AddProduct product={editIndex>=0 ? Products[editIndex] : product} onSubmit={this.handleSubmit} handleView={this.handleView} catArr={catArr} Food={Food} Apparel={Apparel} PersonalCare={PersonalCare} editIndex={editIndex}/>
            : view==2
            ?<Receive Brands={Brands} product={Products} receive={receive} onReceive={this.handleRecieve} handleView={this.handleView} />
            :""
            }
            </React.Fragment>
            </div>
        )
    }
    handleRecieve = (receive) => {
        let s1 = {...this.state};
        let index = s1.Products.findIndex(fi=> fi.code===receive.code)
        if(index>=0){
            s1.Products[index].quantity = s1.Products[index].quantity+(+receive.quantity)
        }
        s1.view=0;
        this.setState(s1)
    }
    handleEdit = (index) => {
        let s1 = {...this.state};
        s1.editIndex=index
        s1.view=1;
        this.setState(s1)
    }
    handleSubmit=(product) => {
        let s1 = {...this.state};
        s1.editIndex>=0 ? s1.Products[s1.editIndex]=product : s1.Products.push(product)
        s1.view=0;
        console.log("mohit")
        this.setState(s1)
    }
    
    handleView = (num) =>{
        let s1 = {...this.state};
        s1.editIndex=-1
        s1.view=num;
        this.setState(s1)
    }
}
export default MainComponent;