import React, {Component} from "react"
import { Switch,Route,Redirect } from "react-router";
import NavBar from "./navbar"
import Home from "./home"
import Cart from "./cart"
import Login from "./login"
import Logout from "./logout";
import AddProduct from "./addProduct";
import AllProducts from "./allproducts";
import Delete from "./delete";
import CheckOut from "./checkout";
import authservice from "./services/authservice";
import ThankYou from "./thankyou";
import MyOrder from "./myorder";
import EditProduct from "./editProduct";
class MainComponent extends Component{
    state={
        cart: [],
    }
    handleAddCart=(product)=>{
        console.log(product,"main.jsx 16")
        let {cart} = this.state
        console.log(cart)
        let cart1 = [...cart]
        console.log(cart1,19)
        let index = cart1.findIndex(fi=> fi.id===product.id)
        if(index>=0){
            cart1.splice(index,1)
            console.log(cart1,24)
            
        }else{
            let prod = {...product,quantity: 1}
            cart1.push(prod)
            console.log(cart1,28)
        }
        console.log(cart1,29)

        this.setState( { cart : cart1 } )
        console.log(cart1,33)
        console.log(this.state,34)
    }
    handleQuantity=(id,incr)=>{
        let {cart} = this.state
        console.log(cart)
        let cart1 = [...cart]
        let index = cart1.findIndex(fi=> fi.id===id)
        if(index>=0){
            if(cart1[index].quantity===1){
                cart1.splice(index,1)
            }else{
                cart1[index].quantity = cart1[index].quantity+incr
            }
        }
        this.setState({cart:cart1})
    }
    
    render(){
        let {cart} = this.state;
        console.log(cart,"main.jsx 37")
        let user = authservice.getUser()
        return(
            <React.Fragment>
            <NavBar cart={cart}/>
            <Switch>
                <Route path="/addProduct"
                render={(props)=> <AddProduct {...props} /> } />
                
                <Route path="/delete/:id"
                render={(props)=> <Delete {...props} /> } />
                
                <Route path="/allProducts"
                render={(props)=> <AllProducts {...props} /> } />
                
                <Route path="/editProduct/:id"
                render={(props)=> <EditProduct {...props} /> } />
                
                <Route path="/home/:category"
                render={(props)=> <Home {...props} cart={cart} onADD={this.handleAddCart} /> } />

                <Route path="/thankyou"
                render={(props)=> <ThankYou {...props} cart={cart} /> } />

                <Route path="/home"
                render={(props)=> <Home {...props} cart={cart} onADD={this.handleAddCart} /> } />
                
                <Route path="/cart"
                render={(props)=> <Cart {...props} cart={cart} handleQunatity={this.handleQuantity}/> } />

                <Route path="/myOrders"
                render={(props)=> <MyOrder {...props} /> } />

                <Route path="/login"
                render={(props)=> <Login {...props} cart={cart}/> } />
                
                <Route path="/logout"
                render={(props)=> <Logout {...props} cart={cart}/> } />

                <Route path="/checkout"
                render={(props)=> <CheckOut {...props} cart={cart} /> } />
                
            </Switch>
            </React.Fragment>
        )
    }
}
export default MainComponent;