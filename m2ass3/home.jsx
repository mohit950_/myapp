import React, {Component} from "react";
import { Card,Button } from "react-bootstrap"
import httpServices from "./services/httpServices"
import pic from "./MyStore-sale.jpg"
class Home extends Component{
    state={
        products:[],
        categories:["All","Sunglasses","Watches","Belts","Handbags","Wallet","Formal Shoes","Sports Shoes","Floaters","Sandals"],
    }
    async componentDidMount() {
        let category = this.props.match.params.category
        let response;
        if(category) response = await httpServices.get(`/products/${category}`)
        else{
            response = await httpServices.get("/products")
        }
        let {data} = response
        this.setState({products:data})
    }
    async componentDidUpdate(prevProps,prevStates){
        if(this.props!==prevProps) this.fetchData()
    }
    async fetchData(){
        let category = this.props.match.params.category
        let response;
        if(category) response = await httpServices.get(`/products/${category}`)
        else{
            response = await httpServices.get("/products")
        }
        let {data} = response
        this.setState({products:data})
    }
    // for manage addCart and RemoveCart send product details to main.jsx
    handleCart = (product) => {
        this.props.onADD(product)
    }
    
    setURL = (cat)=>{
        if(cat==="All"){
            window.location.href = "/home"    
        }else{
            window.location.href = `/home/${cat}`
        }
    }
    
    getBG = (val) => 
        this.props.match.params.category===val ? "bg-secondary" : ""

    render(){
        let {products,categories} = this.state;
        console.log(this.props.cart,51)
        return(
            <div className="container">
                <img src={pic} width="1100px" height="200px" />
                <div className="row">
                    <div className="col-3">
                        <div className="row">
                        {categories.map(ct=>
                            <div className={"row border "+this.getBG(ct)} onClick={()=>this.setURL(ct)}><label className="m-2">{ct}</label></div>
                            )}
                        </div>
                    </div>

                    <div className="col-9">
                        <div className="row mb-2">
                            {products.map(pr=>
                                <div className="col-4">
                                    <Card style={{ width: '18rem' }}>
                                        <img  style={{ maxWidth : "300px",minWidth:"300px"}} src={pr.imagelink} alt="No Images Available"/>
                                    <Card.Body>
                                        <Card.Title>{pr.name}</Card.Title>
                                        <Card.Text>
                                        {pr.price}
                                        </Card.Text>
                                        <Card.Text>
                                           {pr.description.length>30 ? pr.description.substring(0,29)+"...":pr.description}
                                        </Card.Text>
                                    <div className="row">
                                        {this.props.cart.findIndex(fi=> fi.id===pr.id)>=0
                                        ?<Button variant="warning" onClick={()=>this.handleCart(pr)}>Remove from  Cart</Button>
                                        :<Button variant="success" onClick={()=>this.handleCart(pr)}>Add to Cart</Button>
                                        }
                                    </div>
                                    </Card.Body>
                                    </Card>
                                    
                                </div>
                                )}
                        </div>
                    </div>

                </div>
            </div>

        )
    }
}
export default Home;