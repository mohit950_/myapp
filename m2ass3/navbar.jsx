import React, { Component } from "react";
import { Navbar,Container,Nav,NavDropdown,Badge } from 'react-bootstrap'
import { Link } from "react-router-dom"
import authservice from "./services/authservice"
class NavBar extends Component {
  
  render() {
     let user = authservice.getUser()
     console.log(this.props.cart,"navbar")
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
        <Navbar.Brand href="/home">MyStore</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
          <Link className="nav-link" to="/home/Watches">Watches</Link>
          <Link className="nav-link" to="/home/Sunglasses">Sunglasses</Link>
          <Link className="nav-link" to="/home/Belts">Belts</Link>
          <Link className="nav-link" to="/home/Handbags">Handbags</Link>
            <NavDropdown title="Footwear" id="collasible-nav-dropdown">
            <Link className="nav-link text-dark" to="/home/Formal Shoes">Formal Shoes</Link>
            <Link className="nav-link text-dark" to="/home/Sports Shoes">Sports Shoes</Link>
            <Link className="nav-link text-dark" to="/home/Floaters">Floaters</Link>
            <Link className="nav-link text-dark" to="/home/Sandals">Sandals</Link>
            </NavDropdown>
          </Nav>
          <Nav>
            {!user
            ?<Nav.Link href="/login">Login</Nav.Link>
            // ?<Link className="nav-link text-light" to="/login">Login</Link>
            :(
              <React.Fragment>
                <NavDropdown title={user[0].email} id="collasible-nav-dropdown">
                <Link className="nav-link text-dark" to="/myOrders">My Order</Link>
              <Link className="nav-link text-dark" to="/allProducts">All Products</Link>
              <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
              </NavDropdown>
              </React.Fragment>
              )
            }
            <Link className="nav-link" to="/cart">Cart<Badge bg="success"> {this.props.cart.length} </Badge></Link>
          </Nav>
        </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
}
export default NavBar;
