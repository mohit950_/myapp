let mysql = require("mysql");
let connectData = {
    host: "localhost",
    user: "root",
    password: "",
    database: "testdb",
}

function getConnection() {
    return mysql.createConnection(connectData)
}

module.exports.getConnection = getConnection;