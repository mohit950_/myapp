import React,{Component} from "react";
import {Link} from "react-router-dom";
import authservice from "./services/authservice";
class Cart extends Component{
    
    handlequantity=(id,incr)=>{
        this.props.handleQunatity(id,incr)
    }    
    handleCheckout=(cart)=>{
        let user = authservice.getUser()
        user
        ?this.props.history.push("/checkout")
        :this.props.history.push("/login")
    }
    render(){
        console.log(this.props.cart)
        return(
            <div className="container">
                <h5 className="text-center m-2">You have {this.props.cart.length} items in Your Cart</h5><br />
                <div className="row">
                    <div className="col-2">
                    <h6>Cart Value: Rs {this.props.cart.reduce((acc,curr)=>acc+(curr.quantity*curr.price) ,0)}</h6>
                    </div>
                    <div className="col-8"></div>
                    <div className="col-2">
                        <button className="btn btn-primary" onClick={()=>this.handleCheckout(this.props.cart)}>CheckOut</button>
                    </div>

                </div>
                    <div className="row bg-dark text-light m-2">
                        <div className="col-8">
                            Product Details
                        </div>
                        <div className="col-2">
                            Quantity
                        </div>
                        <div className="col-1">
                            
                        </div>
                        <div className="col-1">
                        Price
                        </div>
                    </div>
                    {this.props.cart.map(cr=>
                    <div className="row m-2">
                    <div className="col-8">
                        <div className="row">
                            <div className="col-4">
                            <img src={cr.imagelink} style={{borderRadius : "10px"}} width="30%" height="60%"/>
                            </div>
                            <div className="col-8">
                                {cr.name}<br/>
                                {cr.category}<br/>
                                {cr.description}<br/>
                            </div>
                        </div>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-success" onClick={()=>this.handlequantity(cr.id,1)}><i className="fas fa-plus"></i></button> {cr.quantity} <button className="btn btn-warning" onClick={()=>this.handlequantity(cr.id,-1)}><i className="fas fa-minus bg-warning"></i></button></div>
                    <div className="col-1">
                    </div>
                    <div className="col-1">
                    Rs.{cr.price*cr.quantity}
                    </div>


                </div>
                    )}
            </div>
        )
    }
}
export default Cart;