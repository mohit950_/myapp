import React, {Component} from "react";
import authservice from "./services/authservice";
import httpServices from "./services/httpServices"
class Login extends Component{
    state={
        form: {email:"",password: ""}
    }
    handleChange=(e)=>{
        let {currentTarget:input} = e;
        let s1 = {...this.state}
        s1.form[input.name] = input.value
        this.setState(s1)
    }
    handleLogin=(e)=>{
        e.preventDefault();
        this.postLogin("/login",this.state.form)
    }
    async postLogin(url,obj){
        console.log("Login")
        try{
            let response = await httpServices.post(url,obj)
            let {data} = response
            console.log(data)
            if(data.length===0){
                alert("Invalid Credentials")
            }else{
                authservice.login(data)
                console.log(data)
                this.props.history.push("/checkout")
                // window.location.href = "/checkout"
            } 
        }
        catch(ex){
           console.log("data")
            alert("Login Failed")
        }
    }
   
    render() {
        return(
            <div className="container">
                <h4 className="text-center m-4">Login</h4>
                {this.textField("email","Email")}
                {this.textField("password","Password")}
                <div className="row m-2">
                    <div className="col-2"></div>
                    <div className="col-2">
                    <buton className="btn btn-primary m-2" onClick={this.handleLogin}>Login</buton>
                    </div>
                </div>
                
            </div>
        )
    }
    textField=(name,label,value)=>{
        return(
            <React.Fragment>
                <div className="row m-2">
                    <div className="col-2"></div>
                    <div className="col-2">{label}</div>
                    <div className="col-6">
                        <input
                        type="text"
                        name={name}
                        className="form-control"
                        onChange={this.handleChange}
                        />
                    </div>
                    <div className="col-2"></div>
                </div>
        </React.Fragment>
        )
    }
}
export default Login;