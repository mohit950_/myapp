import React, {Component} from "react";
import { Link } from "react-router-dom";
import httpServices from "./services/httpServices";
class AllProducts extends Component{
    state={
        products:[]
    }
    async componentDidMount() {
        let response = await httpServices.get(`/products`)
        let {data} = response
        this.setState({products:data})
    }
    handleChange=(e)=>{
        let {currentTarget:input} = e;
        let s1 = {...this.state}
        s1.search = input.value
        this.setState(s1)
    }
    handleAddProduct=()=>{
        window.location = "/addProduct"
    }
    render() {
        let {products,search} = this.state
        let product = search ? products.filter(fi=> (fi.name.indexOf(search,0)>=0)) : products
        console.log(this.props)
        return(
            <div className="container">
                <button className="btn btn-success m-2" onClick={()=>this.handleAddProduct()}>Add Product</button><br/>
                <input type="text" onChange={this.handleChange} className="form-control m-2" placeholder="Search" name={search} />
                <div className="row bg-dark text-light text-center mt-3">
                    <div className="col-2"></div>
                    <div className="col-1">Id</div>
                    <div className="col-2">Category</div>
                    <div className="col-4">Description</div>
                    <div className="col-1">Name</div>
                    <div className="col-1">Price</div>
                    <div className="col-1"></div>
                </div>
                {product.map(pr=>
                    <div className="row text-center">
                    <div className="col-2"><img src={pr.imagelink} width="40%"/></div>
                    <div className="col-1">{pr.id}</div>
                    <div className="col-2">{pr.category}</div>
                    <div className="col-4">{pr.description}</div>
                    <div className="col-1">{pr.name}</div>
                    <div className="col-1">{pr.price}</div>
                    <div className="col-1">
                        <Link to={`/editProduct/${pr.id}`} className="mx-2">Edit</Link>
                        <Link to={`/delete/${pr.id}`}>Delete</Link>
                    </div>
                </div>
                    )}
            </div>
        )
    }
    
}
export default AllProducts;