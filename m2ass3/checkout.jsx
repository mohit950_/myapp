import { render } from "@testing-library/react";
import React, {Component} from "react";
import authservice from "./services/authservice";
import httpServices from "./services/httpServices";
class CheckOut extends Component{
    state={
        address: {name: "", line1:"",line2: "",city:""}
    }
    handleChange=(e)=>{
        let {currentTarget:input} = e;
        let s1 = {...this.state}
        s1.address[input.name] = input.value
        this.setState(s1)
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let user = authservice.getUser()
        console.log(user[0].email,18)
        let {address} = this.state
        let items = this.props.cart.reduce((acc,curr)=>acc+curr.quantity,0)
        let amount = this.props.cart.reduce((acc,curr)=>acc+(curr.quantity*curr.price) ,0)
        console.log(amount)
        let orders = {email: user[0].email,name:address.name,city:address.city,line1:address.line1,line2:address.line2,amount:amount,items: items}
        console.log(orders)
        this.postData("/orders",orders)
    }
    async postData(url,obj){
        try{
            let response = await httpServices.post(url,obj)
            window.location = "/thankyou"
        }
        catch(ex){
            alert(ex)
        }
    }
    render(){
        let {cart} = this.props
        let {name, line1,line2,city} = cart
        let quantity = cart.reduce((acc,curr)=>acc+curr.quantity,0)
        return(
            <div className="container">
                <div className="row text-center">
                    <h5>Summarty of Your Order</h5>
                    <br/>
                    <h6>Your order has {quantity} items</h6>
                    <div className="row bg-secondary">
                        <div className="col-4">Name</div>
                        <div className="col-4">Quantity</div>
                        <div className="col-4">Value</div>
                    </div>
                    {cart.map(cr=>
                        <div className="row border">
                            <div className="col-4">{cr.name}</div>
                            <div className="col-4">{cr.quantity}</div>
                            <div className="col-4">{cr.quantity*cr.price}</div>
                        </div>
                        )}
                </div>
                <h5 className="text-center m-3">Delivery Details</h5>
                        {this.textField("name","Name","Enter Name")}
                        {this.textField("line1","Address","Line1")}
                        {this.textField("line2","","Line2")}
                        {this.textField("city","City","City")}
                        <button className="btn btn-success m-1" onClick={this.handleSubmit}>Submit</button>
            </div>


        )
    }
    textField=(name,label,placeholder)=>{
        return(
            <React.Fragment>
                    {label}
                        <input
                        type="text"
                        name={name}
                        placeholder={placeholder}
                        className="form-control"
                        onChange={this.handleChange}
                        />
        </React.Fragment>
        )
    }
}
export default CheckOut