import React, {Component} from "react";
import httpServices from "./services/httpServices";
class EditProduct extends Component{
    state={
        product:{
            id:"",
            description:"",
            imagelink:"",
            category: "",
            name:"",
            price:""
        },
        categories:["Sunglasses","Watches","Belts","HandBags","Wallet","Formal Shoes","Sports Shoes","Floaters","Sandals"],

    }
    async componentDidMount() {
        let id = +this.props.match.params.id
        console.log(id)
        if(id){
            let response = await httpServices.get(`/product/${id}`)
            let {data} = response
            console.log(data)
            this.setState( { product : data[0] } )
        console.log(this.state.product)

        }else{
            this.setState({product:{id:"",description:"",imagelink:"",category:"",name:"",price:""}})
        }
    }
    handleChange=(e)=>{
        let {currentTarget:input} = e;
        let s1 = {...this.state}
        s1.product[input.name] = input.value
        this.setState(s1)
        console.log(this.state.product)
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        console.log(this.state.product,33)
        this.putData(`/products/${this.state.product.id}`,this.state.product)
    }
    async putData(url,obj){
        try{
            let response = await httpServices.put(url,obj)
            window.location = "/home"
        }catch(ex){
            alert(ex)
        }
    }
    render() {
        let {id,description,imagelink,category,name,price} = this.state.product;
        console.log(this.state.product)
        return(
            <div className="container">
                <div className="row">
                <div className="col-3 mt-5 bg-danger">
                    <img src={imagelink} width="100%" />
                    <h6>Name: {name}</h6>
                    <h6>Price: {price}</h6>
                    <h6>Description: {description}</h6>
                </div>
                <div className="col-9">
                {this.textField("Id","id",id)}
                {this.textField("Description","description",description)}
                {this.textField("imageLink","imagelink",imagelink)}
                {this.textField("Name","name",name)}
                {this.textField("Price","price",price)}
                {this.DD(this.state.categories,"category","Category",category)}
                <button className="btn btn-primary mx-5" onClick={this.handleSubmit}>Submit</button>
                </div>
                </div>
            </div>
        )
    }
    textField=(label,name,value)=>{
        return(
            <React.Fragment>
                <div className="row m-2">
                    <div className="col-2"></div>
                    <div className="col-2">{label}</div>
                    <div className="col-6">
                        <input
                        type="text"
                        name={name}
                        value={value}
                        className="form-control"
                        onChange={this.handleChange}
                        />
                    </div>
                    <div className="col-2"></div>
                </div>
        </React.Fragment>
        )
    }
    DD=(arr,name,label,value)=>{
        return(
            <React.Fragment>
                <div className="row m-2">
                    <div className="col-2"></div>
                    <div className="col-2">{label}</div>
                    <div className="col-6">
                        <select
                        type="text"
                        name={name}
                        value={value}
                        className="form-control"
                        onChange={this.handleChange}
                        >
                            <option value="" disabled>Select category</option>
                            {arr.map(ar=><option>{ar}</option>)}
                        </select>
                    </div>
                    <div className="col-2"></div>
                </div>
        </React.Fragment>
        )
    }
}
export default EditProduct;