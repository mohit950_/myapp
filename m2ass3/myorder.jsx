import React, {Component} from "react";
import httpServices from "./services/httpServices"
import authservice from "./services/authservice";
import pic from "./MyStore-sale.jpg" 
class MyOrder extends Component{
state={
        order:[]
    }
    async componentDidMount() {
        let user = authservice.getUser()
        let response = await httpServices.get(`/orders/${user[0].email}`)
        let {data} = response
        console.log(data)
        this.setState({order:data})
    }

    render(){
        let {order} = this.state
        return(
            <div className="container">
                <img src={pic} width="1100px" height="200px" />

                <h5>List of Orders</h5>
                <div className="row bg-dark text-light border">
                    <div className="col-2">Name</div>
                    <div className="col-2">City</div>
                    <div className="col-4">Address</div>
                    <div className="col-2">Amount</div>
                    <div className="col-2">Items</div>
                </div>
                {order.map(ord=>
                <div className="row border">
                    <div className="col-2">{ord.name}</div>
                    <div className="col-2">{ord.city}</div>
                    <div className="col-4">{ord.line1},{ord.line2}</div>
                    <div className="col-2">{ord.amount}</div>
                    <div className="col-2">{ord.items}</div>
                </div>
                )}
            </div>
        )
    }
}
export default MyOrder;