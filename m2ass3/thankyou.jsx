import React, {Component} from "react"
class ThankYou extends Component{
    render() {
        
        return(
            <div className="row text-center">
                <h4>Thank You</h4>
                <h6>We received your order and will process it within next 24 hours!</h6>
            </div>
        )
    }
}
export default ThankYou;