import React, { Component } from "react";
import authservice from "./services/authservice";
import httpServices from "./services/httpServices";
class Delete extends Component{
    async componentDidMount() {
        let id = +this.props.match.params.id
        if(id){
            let response = await httpServices.Delete(`/products/${id}`)
            alert(response.data)
            window.location = "/allProducts"
        }
    }
    render() {
        return ""
    }
}
export default Delete;