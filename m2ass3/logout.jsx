import React, { Component } from "react";
import authservice from "./services/authservice";
class Logout extends Component{
    componentDidMount() {
        authservice.logout()
        window.location = "/home"
    }
    render() {
        return ""
    }
}
export default Logout;