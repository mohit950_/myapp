import React, {Component} from "react";
import httpservices from "./httpservices";
class TotalShopPurchase extends Component {
    state={
        purchase: []
    }
    async componentDidMount() {
        let id = this.props.match.params.id
        let response = await httpservices.get(`/totalPurchase/shops/${id}`)
        let {data} = response
        this.setState( { purchase : data})
    }
    render() {
        let {purchase} = this.state;
        return(
            <div className="container">
                <h4>Purchase Details of Shop {this.props.match.params.id}</h4>
                <div className="row border bg-warning text-center">
                                <div className="col-3">PurchaseId</div>
                                <div className="col-3">ShopId</div>
                                <div className="col-2">Productid</div>
                                <div className="col-2">Quantity</div>
                                <div className="col-2">Price</div>
                            </div>
                {purchase.map(pr=>
                    <div className="row text-center border">
                        <div className="col-3">{pr.purchaseId}</div>
                        <div className="col-3">{pr.shopId}</div>
                        <div className="col-2">{pr.productid}</div>
                        <div className="col-2">{pr.quantity}</div>
                        <div className="col-2">{pr.price}</div>
                    </div>
                    )}
            </div>
        )
    }
}
export default TotalShopPurchase;