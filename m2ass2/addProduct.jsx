import React, {Component} from "react";
import httpservices from "./httpservices";
class AddProduct extends Component {
    state={
        product: {productId: "",productName: "",category: "",description: ""},
        edit: false,
    }

    async componentDidMount() {
        let id = this.props.match.params.id
        console.log(this.props.match.params)
        if(id){
            let response = await httpservices.get(`/products/${id}`)
            let {data} = response
            this.setState({ product : data , edit : true })
        }
        else{
            let product = {productId: "",productName: "",category: "",description: ""}
            this.setState( { product : product , edit: false })
        }
    }

    handleChange=(e) => {
        let {currentTarget: input} = e;
        let s1 = {...this.state}
        s1.product[input.name]=input.value
        this.setState(s1)
    }
    handleSubmit = (e) => {
        e.preventDefault();
        if(this.state.edit){
            this.putData(`/products/${this.props.match.params.id}`,this.state.product)
        }else{
            this.posttData("/products",this.state.product)
        }
    }
    async posttData(url,obj) {
        let response = await httpservices.post(url,obj)
        let {data} = response
        alert("Inserted Successfully")
        window.location = "/viewProduct"
    }
    async putData(url,obj) {
        let response = await httpservices.put(url,obj)
        let {data} = response
        alert("Updated Succes Fully")
        window.location = "/viewProduct"
    }
    render() {
        let {productId,productName,category,description} = this.state.product
        return(
            <div className="container">
                <h4>Add products</h4>
                {this.textField("Product Id","productId",productId,"Enter product Id")}
                {this.textField("Product Name","productName",productName,"Enter product Name")}
                {this.textField("Product Rent","category",category,"Enter product category")}
                {this.textField("Product Description","description",description,"Enter product description")}
                <button className="btn btn-primary btn-sm m-1" onClick={this.handleSubmit}>{this.state.edit ? "Update" :"Submit"}</button>
            </div>
        )
    }
    textField = (label,name,value,placeholder)=>{
        return(
            <React.Fragment>
            <label>{label}</label>
            <input type="text" name={name} placeholder={placeholder} onChange={this.handleChange} className="form-control" value={value}/>
            </React.Fragment>
        )
    }
}
export default AddProduct;