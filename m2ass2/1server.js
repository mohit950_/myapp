var express = require("express");
const { AccordionCollapse } = require("react-bootstrap");
let app = express();
app.use(express.json());
app.use(function (req, res, next){
    res.header("Access-Control-Allow-Origin","*");
    res.header(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTION, PUT, DELETE, PATCH, HEAD"
    );
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});
var port = process.env.PORT || 2410;
app.listen(port, () => console.log(`Node app listening on port ${port}!`))

let {shops,products,purchases} = require("./data.js")

app.get("/shops", function(req,res){
    let arr1 = shops
    res.send(arr1)
})

app.post("/shops", function(req,res){
    let body = req.body
    let arr1 = shops
    console.log(arr1)
    let maxId = arr1.reduce((acc,curr) => acc.shoopId>curr.shoopId ? acc : curr)
    let shoId = maxId.shoopId+1
    let shop = {shoopId: shoId,...body}
    arr1.push(shop)
    res.send(arr1)
})

app.get("/products", function(req,res){
    let arr1 = products
    res.send(arr1)
})
app.get("/products/:id", function(req,res){
    let id = +req.params.id
    let arr1 = products
    arr1 = arr1.find(ar=> ar.productId===id)
    console.log(arr1)
    res.send(arr1)
})

app.post("/products", function(req,res){
    let body = req.body
    let arr1 = products
    console.log(arr1)
    let maxId = arr1.reduce((acc,curr) => acc.productId>curr.productId ? acc : curr)
    let proId = maxId.productId+1
    let product = {productId: proId,...body}
    arr1.push(product)
    res.send("Inserted SuccesFully")
})

app.put("/products/:id", function(req,res){
    let id = +req.params.id;
    console.log(id)
    let body = req.body;
    let arr1 = products
    let index = arr1.findIndex((fi) => fi.productId==+id)
    console.log(index)
    if(index>=0){
        let arr = {productId: id,...body}
        arr1[index] = arr
        res.send("Updated Succesfully")
    }else{
        res.send("Error in Updation")
    }
})

app.get("/purchases", function(req,res){
    let shop = req.query.shop
    console.log(shop)
    let product = req.query.product
    console.log(product)
    let sort = req.query.sort
    console.log(sort)
    let arr1 = purchases
    if(shop){
        arr1 = arr1.filter((fi) => fi.shopId==+shop)
    }
    if(product){
        let productArr = product.split(",")
        arr1 = arr1.filter((fi) => productArr.find(f=> +f===fi.productid))
        console.log(productArr)
    }
    if(sort==="QtyAsc"){
        arr1 = arr1.sort((ar1,ar2)=> ar1.quantity-ar2.quantity)
    }
    else if(sort==="QtyDesc"){
        arr1 = arr1.sort((ar1,ar2)=> ar2.quantity-ar1.quantity)
    }
    else if(sort==="ValueDesc"){
        arr1 = arr1.sort((ar1,ar2)=> (ar2.quantity*ar2.price)-(ar1.quantity*ar1.price))
    }
    else if(sort==="ValueAsc"){
        arr1 = arr1.sort((ar1,ar2)=> (ar1.quantity*ar1.price)-(ar2.quantity*ar2.price))
    }
    res.send(arr1)
})

app.get("/purchases/shops/:id", function(req,res){
    let id = req.params.id;
    let arr1 = purchases
    let find = arr1.filter((fi) => fi.shopId==+id)
    res.send(find)
})
app.get("/purchases/products/:id", function(req,res){
    let id = req.params.id;
    let arr1 = purchases
    let find = arr1.filter((fi) => fi.productid==+id)
    res.send(find)
})


app.get("/totalPurchase/shops/:id", function(req,res){
    let id = req.params.id;
    let purchase = purchases
    let shop = shops
    let product = products
    let arr = []
    purchase = purchase.filter((fi) => fi.shopId===+id) //here we got productid
    let proArr = purchase.reduce((acc,curr) => acc.find(fi=> fi===curr.productid) ? acc : [...acc,curr.productid] ,[])
    purchase = purchase.reduce((acc,curr)=>{
        if(proArr.find(f=> f===curr.productid)){
            if(arr.length===0){
                console.log("firstr")
                let quantity = curr.quantity+acc
                console.log(quantity)
                let pr = {shopId: +id, productid: curr.productid,quantity: +quantity,purchaseId: curr.purchaseId,price: curr.price}
                arr.push(pr)
            }else{
                let index = arr.findIndex(ar=> ar.productid===curr.productid)
                console.log(index)                
                if(index>=0){
                console.log("second")

                let quantity = arr[index].quantity+curr.quantity
                console.log(quantity)
                let pr = {shopId: +id, productid: curr.productid,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr[index] = pr;
                }else{
                    console.log("Third")
                    let quantity = curr.quantity+0
                    console.log(curr.quantity)
                    console.log(quantity)
                    let pr = {shopId: +id, productid: curr.productid,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr.push(pr)
                }
            }
        }
    },0)
    console.log(arr)
    res.send(arr)
})

app.get("/totalPurchase/product/:id", function(req,res){
    let id = req.params.id;
    let purchase = purchases
    let shop = shops
    let product = products
    let arr=[]
    purchase = purchase.filter((fi) => fi.productid==+id) //here we got shopId
    let shopArr = purchase.reduce((acc,curr) => acc.find(fi=> fi===curr.shopId) ? acc : [...acc,curr.shopId] ,[])
    purchase = purchase.reduce((acc,curr)=>{
        if(shopArr.find(f=> f===curr.shopId)){
            if(arr.length===0){
                console.log("firstr")
                let quantity = curr.quantity+acc
                console.log(quantity)
                let pr = {shopId: curr.shopId, productid: id,quantity: +quantity,purchaseId: curr.purchaseId,price: curr.price}
                arr.push(pr)
            }else{
                let index = arr.findIndex(ar=> ar.shopId===curr.shopId)
                console.log(index)                
                if(index>=0){
                console.log("second")

                let quantity = arr[index].quantity+curr.quantity
                console.log(quantity)
                let pr = {shopId: curr.shopId, productid: +id,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr[index] = pr;
                }else{
                    console.log("Third")
                    let quantity = curr.quantity+0
                    console.log(curr.quantity)
                    console.log(quantity)
                    let pr = {shopId: curr.shopId, productid: +id,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr.push(pr)
                }
            }
        }
    },0)
    console.log(arr)
    res.send(arr)

})

app.post("/purchases", function(req,res){
    let arr1 = purchases
    let body = req.body
    let maxId = arr1.reduce((acc,curr) => acc.purchaseId>curr.purchaseId ? acc : curr)
    let proId = maxId.purchaseId+1
    let purchase = {purchaseId: proId,...body}
    arr1.push(purchase)
    res.send("Inserted SuccesFully")
})