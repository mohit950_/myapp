var express = require("express");
let app = express();
app.use(express.json());
app.use(function (req, res, next){
    res.header("Access-Control-Allow-Origin","*");
    res.header(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTION, PUT, DELETE, PATCH, HEAD"
    );
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});
const port = 2410;
app.listen(port, () => console.log(`Node app listening on port ${port}!`))

const { Client} = require("pg");
const client = new Client({
        user: "aojilngfkuwcsh",
        password: "4be53602fb71f6769e19df9a87362b705b536edfa9a77facd390176c3052c51c",
        database: "ddi69mtbdueas3",
        port: 5432,
        host: "ec2-44-194-112-166.compute-1.amazonaws.com",
        ssl: { rejectUnauthorized: false },
    });
    client.connect(function (res, error) {
        if(error) console.log(error,`!!!`);
        else console.log(`Connected!!!`);
    });


app.get("/shops", function(req,res){
    let connection = getConnection()
    let sql = `SELECT * FROM shops`
    client.query(sql,function(err,result){
        if(err){
            console.log(err)
            res.send(404).send("Not found any Data")
        }
        else{
            console.log(result)
            res.send(result)
        }
    })
})

app.post("/shops", function(req,res){
    let body = req.body
    let connection = getConnection()
    let sql1 = "SELECT * FROM shops WHERE id=?"
    client.query(sql1,body.id,function(err,result){
        if(err){
            console.log(err)
            res.status(404).send("Not found Data")
        }
        else if(result.length>0){
            console.log(err)
            res.status(404).send("Id : Already esxists")
        }
        else{
            let sql = "INSERT INTO shops(shopId,shopName,rent) VALUES (?,?,?)"
            client.query(sql,[body.shopId,body.shopName,body.rent],function(err,result){
                if(err){
                    console.log(err)
                    res.status(404).send("Not found Data")
                }
                else{
                    console.log(result)
                    res.send("INSERTED")
                }
            })
        }
    })
})

app.get("/products", function(req,res){
    let connection = getConnection()
    let sql = `SELECT * FROM shops`
    client.query(sql,function(err,result){
        if(err){
            console.log(err)
            res.send(404).send("Not found any Data")
        }
        else{
            console.log(result)
            res.send(result)
        }
    })
})
app.get("/products/:id", function(req,res){
    let id = +req.params.id
    let connection = getConnection()
    let sql = `SELECT * FROM shops id=?`
    client.query(sql,id,function(err,result){
        if(err){
            console.log(err)
            res.send(404).send("Not found any Data")
        }
        else{
            console.log(result)
            res.send(result)
        }
    })
})

app.post("/products", function(req,res){
    let body = req.body
    let connection = getConnection()
    let sql1 = "SELECT * FROM products WHERE productId=?"
    client.query(sql1,body.productId,function(err,result){
        if(err){
            console.log(err)
            res.status(404).send("Not found Data")
        }
        else if(result.length>0){
            console.log(err)
            res.status(404).send("Product Id : Already esxists")
        }
        else{
            let sql = "INSERT INTO products(productId,productName,category,description) VALUES (?,?,?,?)"
            client.query(sql,[body.productId,body.productName,body.category,body.description],function(err,result){
                if(err){
                    console.log(err)
                    res.status(404).send("Not found Data")
                }
                else{
                    console.log(result)
                    res.send("INSERTED")
                }
            })
        }
    })
})

app.put("/products/:id", function(req,res){
    let id = +req.params.id;
    let connection = getConnection()
            let sql = "UPDATE products SET productId=?, productName=?, category=?, description=? WHERE productId=?"
            client.query(sql,[id,body.productName,body.category,body.description,id],function(err,result){
                if(err){
                    console.log(err)
                    res.status(404).send("Error in Updation")
                }
                else{
                    console.log(result)
                    res.send("Succes Fully Updated")
                }
            })
})

app.get("/purchases", function(req,res){
    let shop = req.query.shop
    console.log(shop)
    let product = req.query.product
    console.log(product)
    let sort = req.query.sort
    console.log(sort)
    let sql = `SELECT * FROM purchases`
    client.query(sql,function(err,result){
        if(err){
            console.log(err)
            res.send(404).send("Not found any Data")
        }
        else{
    if(shop){
        result = result.filter((fi) => fi.shopId==+shop)
    }
    if(product){
        let productArr = product.split(",")
        result = result.filter((fi) => productArr.find(f=> +f===fi.productid))
        console.log(productArr)
    }
    if(sort==="QtyAsc"){
        result = result.sort((ar1,ar2)=> ar1.quantity-ar2.quantity)
    }
    else if(sort==="QtyDesc"){
        result = result.sort((ar1,ar2)=> ar2.quantity-ar1.quantity)
    }
    else if(sort==="ValueDesc"){
        result = result.sort((ar1,ar2)=> (ar2.quantity*ar2.price)-(ar1.quantity*ar1.price))
    }
    else if(sort==="ValueAsc"){
        result = result.sort((ar1,ar2)=> (ar1.quantity*ar1.price)-(ar2.quantity*ar2.price))
    }
    res.send(result)
}
})

})

app.get("/purchases/shops/:id", function(req,res){
    let id = +req.params.id;
    let connection = getConnection()
    let sql = `SELECT * FROM purchases shopId=?`
    client.query(sql,id,function(err,result){
        if(err){
            console.log(err)
            res.send(404).send("Not found any Data")
        }
        else{
            console.log(result)
            res.send(result)
        }
    })
})
app.get("/purchases/products/:id", function(req,res){
    let id = +req.params.id;
    let connection = getConnection()
    let sql = `SELECT * FROM purchases productid=?`
    client.query(sql,id,function(err,result){
        if(err){
            console.log(err)
            res.send(404).send("Not found any Data")
        }
        else{
            console.log(result)
            res.send(result)
        }
    })
})


app.get("/totalPurchase/shops/:id", function(req,res){
    let id = +req.params.id;
    let arr = []
    let connection = getConnection()
    let sql = `SELECT * FROM purchases shopId=?`
    client.query(sql,id,function(err,result){
        if(err){
            console.log(err)
            res.send(404).send("Not found any Data")
        }
        else{
    let proArr = result.reduce((acc,curr) => acc.find(fi=> fi===curr.productid) ? acc : [...acc,curr.productid] ,[])
     result = result.reduce((acc,curr)=>{
        if(proArr.find(f=> f===curr.productid)){
            if(arr.length===0){
                console.log("firstr")
                let quantity = curr.quantity+acc
                console.log(quantity)
                let pr = {shopId: +id, productid: curr.productid,quantity: +quantity,purchaseId: curr.purchaseId,price: curr.price}
                arr.push(pr)
            }else{
                let index = arr.findIndex(ar=> ar.productid===curr.productid)
                console.log(index)                
                if(index>=0){
                console.log("second")

                let quantity = arr[index].quantity+curr.quantity
                console.log(quantity)
                let pr = {shopId: +id, productid: curr.productid,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr[index] = pr;
                }else{
                    console.log("Third")
                    let quantity = curr.quantity+0
                    console.log(curr.quantity)
                    console.log(quantity)
                    let pr = {shopId: +id, productid: curr.productid,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr.push(pr)
                }
            }
        }
    },0)
    console.log(arr)
    res.send(arr)
}
})

})

app.get("/totalPurchase/product/:id", function(req,res){
    let id = +req.params.id;
    let arr=[]
    let connection = getConnection()
    let sql = `SELECT * FROM purchases productid=?`
    client.query(sql,id,function(err,result){
        if(err){
            console.log(err)
            res.send(404).send("Not found any Data")
        }
        else{
    let shopArr = result.reduce((acc,curr) => acc.find(fi=> fi===curr.shopId) ? acc : [...acc,curr.shopId] ,[])
    result = result.reduce((acc,curr)=>{
        if(shopArr.find(f=> f===curr.shopId)){
            if(arr.length===0){
                console.log("firstr")
                let quantity = curr.quantity+acc
                console.log(quantity)
                let pr = {shopId: curr.shopId, productid: id,quantity: +quantity,purchaseId: curr.purchaseId,price: curr.price}
                arr.push(pr)
            }else{
                let index = arr.findIndex(ar=> ar.shopId===curr.shopId)
                console.log(index)                
                if(index>=0){
                console.log("second")

                let quantity = arr[index].quantity+curr.quantity
                console.log(quantity)
                let pr = {shopId: curr.shopId, productid: +id,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr[index] = pr;
                }else{
                    console.log("Third")
                    let quantity = curr.quantity+0
                    console.log(curr.quantity)
                    console.log(quantity)
                    let pr = {shopId: curr.shopId, productid: +id,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr.push(pr)
                }
            }
        }
    },0)
    console.log(arr)
    res.send(arr)
}
})   
})

app.post("/purchases", function(req,res){
    let arr1 = purchases
    let body = req.body
    let connection = getConnection()
    let sql1 = "SELECT * FROM purchase WHERE purchaseId=?"
    client.query(sql1,body.purchaseId,function(err,result){
        if(err){
            console.log(err)
            res.status(404).send("Not found Data")
        }
        else if(result.length>0){
            console.log(err)
            res.status(404).send("Purchase Id : Already esxists")
        }
        else{
            let sql = "INSERT INTO purchases(purchaseId,shopId,productid,quantity,price) VALUES (?,?,?,?,?)"
            client.query(sql,[body.purchaseId,body.shopId,body.productid,body.quantity,body.price],function(err,result){
                if(err){
                    console.log(err)
                    res.status(404).send("Not found Data")
                }
                else{
                    console.log(result)
                    res.send("INSERTED")
                }
            })
        }
    })
})