import React, {Component} from "react";
import httpservices from "./httpservices";
import { Link } from "react-router-dom";
class ViewShop extends Component {
    state={
        shops:[]
    }
    async componentDidMount() {
        let response = await httpservices.get("/shops")
        let {data} =  response
        this.setState( { shops: data } )
    }
    render() {
        let {shops} = this.state;
        return(
            <div className="container">
                <div className="row border bg-warning text-primary text-center">
                    <div className="col-3">Shop Id</div>
                    <div className="col-3">Shop Name</div>
                    <div className="col-3">Shop Rent</div>
                    <div className="col-3"></div>
                </div>
                    {shops.map(sp=>
                    <div className="row border text-center">
                    <React.Fragment>
                    <div className="col-3">{sp.shopId}</div>
                    <div className="col-3">{sp.shopName}</div>
                    <div className="col-3">{sp.rent}</div>
                    <div className="col-3">
                    <Link className="m-2" to={`/purchases/shop/${sp.shopId}`} role="button">Purchase</Link>
                    <Link className="m-2" to={`/totalPurchases/shop/${sp.shopId}`}>Total Purchase</Link>
                    </div>
                    </React.Fragment>
                    </div>
                    )}
            </div>

        )
    }
}
export default ViewShop;