import React, { Component } from "react";
import { NavDropdown } from "react-bootstrap";
import {Link} from "react-router-dom";
class NavBar extends Component {
  render() {
    let { user } = this.props;
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link className="navbar-brand" to="/">
          Products
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
        <NavDropdown title="Shops" id="basic-nav-dropdown">
          <NavDropdown.Item href="/viewShop">View Shops</NavDropdown.Item>
          <NavDropdown.Item href="/addShop ">Add Shops</NavDropdown.Item>
        </NavDropdown>
        <NavDropdown title="Products" id="basic-nav-dropdown">
          <NavDropdown.Item href="/viewProduct">View Product</NavDropdown.Item>
          <NavDropdown.Item href={`/addProduct`}>Add Product</NavDropdown.Item>
        </NavDropdown>
        <li className="nav-item">
            <Link className="nav-link" to="/purchases">
             Purchases
            </Link>
          </li>

          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;