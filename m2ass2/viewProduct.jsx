import React, {Component} from "react";
import { Link } from "react-router-dom";
import httpservices from "./httpservices";
class ViewProduct extends Component {
    state={
        products:[]
    }
    async componentDidMount() {
        let response = await httpservices.get("/products")
        let {data} =  response
        this.setState( { products: data } )
    }
    render() {
        let {products} = this.state;
        // let shops = products.reduce((acc,curr) => acc.find(fi=> fi===curr.productName) ? acc : [...acc,curr.productName] ,[])
        // console.log(shops)
        return(
            <div className="container">
                <div className="row border bg-warning text-primary text-center">
                    <div className="col-2">Product Id</div>
                    <div className="col-2">Product Name</div>
                    <div className="col-2">Product Category</div>
                    <div className="col-2">Product Description</div>
                    <div className="col-4"></div>
                </div>
                    {products.map(sp=>
                    <div className="row border text-center">
                    <React.Fragment>
                    <div className="col-2">{sp.productId}</div>
                    <div className="col-2">{sp.productName}</div>
                    <div className="col-2">{sp.category}</div>
                    <div className="col-2">{sp.description}</div>
                    <div className="col-4">
                    <Link className="m-2" to={`/addproduct/${sp.productId}`}>Edit</Link>
                    <Link className="m-2" to={`/purchases/product/${sp.productId}`}>Purchase</Link>
                    <Link className="m-2" to={`/totalPurchases/product/${sp.productId}`}>Total Purchase</Link>
                    </div>
                    </React.Fragment>
                    </div>
                    )}
            </div>

        )
    }
}
export default ViewProduct;