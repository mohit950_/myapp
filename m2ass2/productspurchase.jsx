import React, {Component} from "react";
import httpservices from "./httpservices";
class ProductPurchase extends Component {
    state={
        purchase: []
    }
    async componentDidMount() {
        let id = this.props.match.params.id
        let response = await httpservices.get(`/purchases/products/${id}`)
        let {data} = response
        this.setState( { purchase : data})
    }
    render() {
        let {purchase} = this.state;
        console.log(purchase)
        return(
            <div className="container">
                <div className="row m-2 text-center bg-dark text-light">
                        <div className="col-3">Purchase Id</div>
                        <div className="col-3">Product Id</div>
                        <div className="col-3">Quantity</div>
                        <div className="col-3">Price</div>
                    </div>
                {purchase.map(pr=>
                    <div className="row m-2 text-center">
                        <div className="col-3">{pr.purchaseId}</div>
                        <div className="col-3">{pr.productid}</div>
                        <div className="col-3">{pr.quantity}</div>
                        <div className="col-3">{pr.price}</div>
                    </div>
                    )}
            </div>
        )
    }
}
export default ProductPurchase;