import React, {Component} from "react";
import httpservices from "./httpservices";
class AddShop extends Component {
    state={
        shop: {shopId: "",shopName: "",rent: ""},
        edit: false,
    }

    async componentDidMount() {
        let id = this.props.match.params
        console.log(id)
        if(id){
            let response = await httpservices.get(`/shops/${id}`)
            let {data} = response
            this.setState({ shop : data , edit : true })
        }
        else{
            let shop = {shopId: "",shopName: "",rent: ""}
            this.setState( { shop : shop , edit: false })
        }
    }

    handleChange=(e) => {
        let {currentTarget: input} = e;
        let s1 = {...this.state}
        s1.shop[input.name]=input.value
        this.setState(s1)
    }
    handleSubmit = (e) => {
        e.preventDefault();
        if(this.state.edit){
            this.putData(`/shops/${this.props.match.params}`,this.state.shop)
        }else{
            this.posttData("/shops",this.state.shop)
        }
    }
    async posttData(url,obj) {
        let response = await httpservices.post(url,obj)
        let {data} = response
        alert("Inserted Successfully")
        window.location = "/viewShop"
    }
    async puttData(url,obj) {
        let response = await httpservices.put(url,obj)
        let {data} = response
        alert(data)
    }
    render() {
        let {shopId,shopName,rent} = this.state.shop
        return(
            <div className="container">
                <h4>Add Shops</h4>
                {this.textField("Shop Id","shopId",shopId,"Enter Shop Id")}
                {this.textField("Shop Name","shopName",shopName,"Enter Shop Name")}
                {this.textField("Shop Rent","rent",rent,"Enter Shop Rent")}
                <button className="btn btn-primary btn-sm m-1" onClick={this.handleSubmit}>{this.state.edit ? "Update" :"Submit"}</button>
            </div>
        )
    }
    textField = (label,name,value,placeholder)=>{
        let disable = name==="shopId" ? value ? true : false : false;
        return(
            <React.Fragment>
            <label>{label}</label>
            <input type="text" name={name} placeholder={placeholder} onChange={this.handleChange} className="form-control" value={value} disabled={disable}/>
            </React.Fragment>
        )
    }
}
export default AddShop;