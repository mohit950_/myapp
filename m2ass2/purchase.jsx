import React, {Component} from "react";
import httpservices from "./httpservices";
import queryString from "query-string";
class Purchase extends Component{
    state={
        filter: {shop: "",product: "",sort: ""},
        purchases: [],
        sorts: ["QtyAsc","QtyDesc","ValueAsc","ValueDesc"],
        productsName: ["Pepsi Can", "Pepsi PET", "Diet Coke", "Mazaa", "Dairy Milk", "Fruit & Nut", "Silk - Crackles", "Perk"],
        products:[1, 2, 3, 4, 5, 6, 8],
        shops:[1, 2, 3, 4],
    }
    async componentDidMount() {
        let response = await httpservices.get("/purchases")
        let {data} = response
        this.setState( { purchases : data })
    }
    async componentDidUpdate(prevProps,prevState) {
        if(this.props!==prevProps) this.fetchData()
    }
    async fetchData() {
        let queryParams = queryString.parse(this.props.location.search)
        let searchStr = this.makeSearchString(queryParams)
        let response = await httpservices.get(`/purchases?${searchStr}`)
        let {data} = response
        this.setState( { purchases : data })
    }
    handleChange=(e) => {
        let {currentTarget: input} = e;
        let s1 = {...this.state}
        input.type =="checkbox"
        ?s1.filter.product = this.updateCB(input.checked,input.value,s1.filter.product)
        :s1.filter[input.name] = input.value
        console.log(s1.filter.product)
        console.log(s1.filter)
        this.setState(s1)
        this.callUrl("/purchases", this.state.filter)
    }
    updateCB = (checked,value,name) =>{
        console.log(value)
        console.log(name)
        let inpuArr = name ? name.split(",") : []
        if(checked) inpuArr.push(value)
        else{
            let index = inpuArr.findIndex(fi=> fi===value)
            if(index>=0) inpuArr.splice(index,1)
        }
        return inpuArr.join(',')
    }
    callUrl = (url,options)=> {
        console.log(options)
        let seachstring = this.makeSearchString(options)
        console.log(seachstring)
        this.props.history.push({
            pathname: url,
            search: seachstring,
        })
    }
    makeSearchString = (options) => {
        let {shop,product,sort} = options
        let searchString=""
        searchString = this.addQueryString(searchString,"shop",shop)
        searchString = this.addQueryString(searchString,"product",product)
        searchString = this.addQueryString(searchString,"sort",sort)
        console.log(searchString)
        return searchString
    }
    addQueryString = (str,name,value) =>
        value
        ?str
        ?`${str}&${name}=${value}`
        :`${name}=${value}`
        :str;

    render() {
        let {shops,purchases,sorts,filter,products,productsName} = this.state;
        let {shop,product="",sort} = filter;
        // let shops = purchases.reduce((acc,curr) => acc.find(fi=> fi===curr.shopId) ? acc : [...acc,curr.shopId] ,[])
        return(
            <div className="container">
                <div className="row">
                    <div className="col-3">
                        <div className="row">
                            Products
                        </div>
                        {this.checkBoxes(products,"product",product.split(","))}
                        <div className="row">
                            Store
                        </div>
                        <select name="shop" value={shop}  onChange={this.handleChange} className="form-control">
                            <option value="" disabled>Select shop</option>
                            {shops.map(sp=><option>{sp}</option>)}
                        </select>
                        <div className="row">
                            Sort
                        </div>
                        <select name="sort" value={sort}  onChange={this.handleChange} className="form-control">
                            <option value="" disabled>Select Sort</option>
                            {sorts.map(sp=><option>{sp}</option>)}
                        </select>
                    </div>
                    <div className="col-9">
                    <div className="row border bg-warning text-center">
                                <div className="col-2">PurchaseId</div>
                                <div className="col-2">ShopId</div>
                                <div className="col-2">Productid</div>
                                <div className="col-2">Quantity</div>
                                <div className="col-2">Price</div>
                                <div className="col-2">Value</div>
                            </div>
                        {purchases.map(pr=>
                            <div className="row border text-center">
                                <div className="col-2">{pr.purchaseId}</div>
                                <div className="col-2">{pr.shopId}</div>
                                <div className="col-2">{pr.productid}</div>
                                <div className="col-2">{pr.quantity}</div>
                                <div className="col-2">{pr.price}</div>
                                <div className="col-2">{pr.price*pr.quantity}</div>
                            </div>
                            )}
                    </div>
                </div>
            </div>
        )
    }
    checkBoxes = (arr,name,value) => {
        return(
            arr.map(pr=>
                <React.Fragment>
                <input type="checkbox" name={name} value={pr} onChange={this.handleChange} className="check-input" />
                <label>{pr}</label><br/>
                </React.Fragment>
                )
        )
    }
}
export default Purchase;