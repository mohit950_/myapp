let mysql = require("mysql")
const passport = require("passport")

let connectData = {
    host: "localhost",
    user: "root",
    password: "",
    database: "testdb",
}

function getConnection() {
    return mysql.createConnection(connectData)
}

module.exports.getConnection = getConnection;