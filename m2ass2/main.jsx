import React, {Component} from "react";
import {Switch,Route} from "react-router-dom"
import NavBar from "./navbar.jsx";
import ViewShop from "./viewShop";
import ViewProduct from "./viewProduct";
import AddProduct from "./addProduct";
import AddShop from "./addShop";
import Purchase from "./purchase";
import ShopPurchase  from "./ShopPurchase.jsx";
import ProductPurchase from "./productspurchase.jsx";
import TotalShopPurchase from "./totalShopPurchase";
import TotalProductPurchase from "./totalProductPurchase.jsx";
class MainComponent extends Component{
    render() {
        return(
            <div className="container">
                <NavBar />
                <Switch>
                    <Route path="/viewShop" component={ViewShop} />
                    <Route path="/viewProduct" component={ViewProduct} />
                    <Route path="/addShop" component={AddShop} />
                    <Route path="/addProduct/:id" component={AddProduct} />
                    <Route path="/addProduct" component={AddProduct} />
                    <Route path="/purchases/shop/:id" component={ShopPurchase} />
                    <Route path="/purchases/product/:id" component={ProductPurchase} />
                    <Route path="/purchases" component={Purchase} />
                    <Route path="/totalPurchases/shop/:id" component={TotalShopPurchase} />
                    <Route path="/totalPurchases/product/:id" component={TotalProductPurchase} />
                </Switch>
            </div>
        )
    }
}
export default MainComponent;