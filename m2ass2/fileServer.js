var express = require("express");
const { AccordionCollapse } = require("react-bootstrap");
let app = express();
app.use(express.json());
app.use(function (req, res, next){
    res.header("Access-Control-Allow-Origin","*");
    res.header(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTION, PUT, DELETE, PATCH, HEAD"
    );
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});
var port = process.env.PORT || 2410;
app.listen(port, () => console.log(`Node app listening on port ${port}!`))

let {data} = require("./data.js")

let fs = require("fs");

let fname = "ass2data.json"

app.get("/getData" , function(req,res){
    // console.log(shops)
    let data1 = JSON.stringify(data)
    fs.writeFile(fname,data1,function(err,result){
        if(err) res.status(404).send(err)
        else res.send("DATA SETS")
    })
})

app.get("/shops", function(req,res){
    fs.readFile(fname,"utf8", function(err,data){
        if(err) res.send(404).send("Bad Request")
        else {
            let shops = JSON.parse(data)
            console.log(shops.shops)
            res.send(shops.shops)
        }
    })
})

app.post("/shops", function(req,res){
    let body = req.body
    fs.readFile(fname,"utf8", function(err,data){
        if(err) res.send(404).send("Bad Request")
        else {
            let shops = JSON.parse(data)
            let maxId = shops.shops.reduce((acc,curr) => acc.shoopId>curr.shopId ? acc : curr)
            let shoId = maxId.shopId+1
            let shop = {shopId: shoId,...body}
            shops.shops.push(shop)
            console.log(shops.shops)
            let data1 = JSON.stringify(shops)
            fs.writeFile(fname,data1,function(err,data){
                if(err) res.status(404).send("Bad Request")
                else res.send("Data Inserted")
            })
            // res.send(shops.shops)
        }
    })
})

app.get("/products", function(req,res){
    fs.readFile(fname,"utf8", function(err,data){
        if(err) res.send(404).send("Bad Request")
        else {
            let products = JSON.parse(data)
            console.log(products.products)
            res.send(products.products)
        }
    })
})
app.get("/products/:id", function(req,res){
    let id = +req.params.id
    fs.readFile(fname,"utf8", function(err,data){
        if(err) res.send(404).send("Bad Request")
        else {
            let products = JSON.parse(data)
            arr1 = products.products.find(ar=> ar.productId===id)
            res.send(arr1)
        }
    })
})

app.post("/products", function(req,res){
    let body = req.body
    fs.readFile(fname,"utf8", function(err,data){
        if(err) res.status(404).send("Bad Request")
        else {
            let products = JSON.parse(data)
            let maxId = products.products.reduce((acc,curr) => acc.productId>curr.productId ? acc : curr)
            let proId = maxId.productId+1
            let product = {productId: proId,...body}
            products.products.push(product)
            let data1 = JSON.stringify(products)
            fs.writeFile(fname,data1,function(err,data){
                if(err) res.status(404).send("Bad Request")
                else res.send("Data Inserted")
            })
        }
    })
    
})

app.put("/products/:id", function(req,res){
    let id = +req.params.id;
    let body = req.body;
    fs.readFile(fname,"utf8", function(err,data){
        if(err) res.status(404).send("Bad Request")
        else {
            let products = JSON.parse(data)
            let index = products.products.findIndex((fi) => fi.productId==+id)
            if(index>=0){
                let arr = {productId: id,...body}
                products.products[index] = arr
                let data1 = JSON.stringify(products)
                fs.writeFile(fname,data1,function(err,data){
                    if(err) res.status(404).send("Bad Request")
                    else res.send("Updated Succesfully")
                })
            }
            
        }
    })
})

app.get("/purchases", function(req,res){
    let shop = req.query.shop
    let product = req.query.product
    let sort = req.query.sort
    fs.readFile(fname,"utf8", function(err,data){
        if(err) res.send(404).send("Bad Request")
        else {
                let purchases = JSON.parse(data)
                if(shop){
                    purchases.purchases = purchases.purchases.filter((fi) => fi.shopId==+shop)
                }
                if(product){
                    let productArr = product.split(",")
                    purchases.purchases = purchases.purchases.filter((fi) => productArr.find(f=> +f===fi.productid))
                    // console.log(productArr)
                }
                if(sort==="QtyAsc"){
                    purchases.purchases = purchases.purchases.sort((ar1,ar2)=> ar1.quantity-ar2.quantity)
                }
                else if(sort==="QtyDesc"){
                    purchases.purchases = purchases.purchases.sort((ar1,ar2)=> ar2.quantity-ar1.quantity)
                }
                else if(sort==="ValueDesc"){
                    purchases.purchases = purchases.purchases.sort((ar1,ar2)=> (ar2.quantity*ar2.price)-(ar1.quantity*ar1.price))
                }
                else if(sort==="ValueAsc"){
                    purchases.purchases = purchases.purchases.sort((ar1,ar2)=> (ar1.quantity*ar1.price)-(ar2.quantity*ar2.price))
                }
            res.send(purchases.purchases)
        }
    })
})

app.get("/purchases/shops/:id", function(req,res){
    let id = req.params.id;
    fs.readFile(fname,"utf8",function(err,data){
        if(err) res.status(404).send ("Bada Request")
        else{
            let purchases = JSON.parse(data)
            let find = purchases.purchases.filter((fi) => fi.shopId==+id)
            res.send(find)
        }
    })
    
})
app.get("/purchases/products/:id", function(req,res){
    let id = req.params.id;
    fs.readFile(fname,"utf8",function(err,data){
        if(err) res.status(404).send ("Bada Request")
        else{
            let purchases = JSON.parse(data)
            let find = purchases.purchases.filter((fi) => fi.productid==+id)
            res.send(find)
        }
    })
})


app.get("/totalPurchase/shops/:id", function(req,res){
    let id = req.params.id;
    let arr = []
    fs.readFile(fname,"utf8",function(err,data){
        if(err) res.status(404).send ("Bada Request")
        else{
            let purchases = JSON.parse(data)
            let purchase = purchases.purchases.filter((fi) => fi.shopId===+id) //here we got productid
            let proArr = purchase.reduce((acc,curr) => acc.find(fi=> fi===curr.productid) ? acc : [...acc,curr.productid] ,[])
            purchase = purchase.reduce((acc,curr)=>{
                if(proArr.find(f=> f===curr.productid)){
                    if(arr.length===0){
                        console.log("firstr")
                        let quantity = curr.quantity+acc
                        console.log(quantity)
                        let pr = {shopId: +id, productid: curr.productid,quantity: +quantity,purchaseId: curr.purchaseId,price: curr.price}
                        arr.push(pr)
                    }else{
                        let index = arr.findIndex(ar=> ar.productid===curr.productid)
                console.log(index)                
                if(index>=0){
                console.log("second")

                let quantity = arr[index].quantity+curr.quantity
                console.log(quantity)
                let pr = {shopId: +id, productid: curr.productid,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr[index] = pr;
                }else{
                    console.log("Third")
                    let quantity = curr.quantity+0
                    console.log(curr.quantity)
                    console.log(quantity)
                    let pr = {shopId: +id, productid: curr.productid,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                    arr.push(pr)
                }
            }
        }
    },0)
    console.log(arr)
    res.send(arr)
        }
    })
})

app.get("/totalPurchase/product/:id", function(req,res){
    let id = req.params.id;
    let arr=[]
    fs.readFile(fname,"utf8",function(err,data){
        if(err) res.status(404).send ("Bada Request")
        else{
            let purchases = JSON.parse(data)
            let purchase = purchases.purchases.filter((fi) => fi.productid==+id) //here we got shopid
            let shopArr = purchase.reduce((acc,curr) => acc.find(fi=> fi===curr.shopId) ? acc : [...acc,curr.shopId] ,[])
            purchase = purchase.reduce((acc,curr)=>{
                if(shopArr.find(f=> f===curr.shopId)){
                    if(arr.length===0){
                        console.log("firstr")
                        let quantity = curr.quantity+acc
                        console.log(quantity)
                        let pr = {shopId: curr.shopId, productid: id,quantity: +quantity,purchaseId: curr.purchaseId,price: curr.price}
                        arr.push(pr)
                    }else{
                        let index = arr.findIndex(ar=> ar.shopId===curr.shopId)
                        console.log(index)                
                        if(index>=0){
                        console.log("second")
        
                        let quantity = arr[index].quantity+curr.quantity
                        console.log(quantity)
                        let pr = {shopId: curr.shopId, productid: +id,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                            arr[index] = pr;
                        }else{
                            console.log("Third")
                            let quantity = curr.quantity+0
                            console.log(curr.quantity)
                            console.log(quantity)
                            let pr = {shopId: curr.shopId, productid: +id,quantity: quantity,purchaseId: curr.purchaseId,price: curr.price}
                            arr.push(pr)
                        }
                    }
                }
    },0)
    console.log(arr)
    res.send(arr)
        }
    })
})

app.post("/purchases", function(req,res){
    let body = req.body
    fs.readFile(fname,"utf8",function(err,data){
        if(err) res.status(404).send("Bad Request")
        else{
            let purchases = JSON.parse(data)
            let maxId = purchases.purchases.reduce((acc,curr) => acc.purchaseId>curr.purchaseId ? acc : curr)
            let proId = maxId.purchaseId+1
            let purchase = {purchaseId: proId,...body}
            purchases.purchases.push(purchase)
            let data1 = JSON.stringify(purchases)
            fs.writeFile(fname,data1,function(err,data){
                if(err) res.status(404).send("Bad Request")
                else ("Data Inserted")
            })
        }
    })
    
})