import React, {Component} from "react";
class Points extends Component{

    sort = (num) => {
        let s1 = {...this.props.team};
        switch(num){
            case 0 :
                this.props.team.sort((a1,a2) => a1.name.localeCompare(a2.name))
                break;
            case 1 :
                this.props.team.sort((a1,a2) => a1.played-a2.played)
                break;
            case 2 :
                this.props.team.sort((a1,a2) => a1.won-a2.won)
                break;
            case 3 :
                this.props.team.sort((a1,a2) => a1.lost-a2.lost)
                break;
            case 4 :
                this.props.team.sort((a1,a2) => a1.drawn-a2.drawn)
                break;
            case 5 :
                this.props.team.sort((a1,a2) => a1.goalsFor-a2.goalsFor)
                break;
            case 6 :
                this.props.team.sort((a1,a2) => a1.goalsAgainst-a2.goalsAgainst)
                break;
            case 7 :
                this.props.team.sort((a1,a2) => a1.points-a2.points)
                    break;
            default:
                break;
        }
        this.setState(s1)
    }

    render() {
        let {team,matches} = this.props;
        
        console.log(team)
        return (
            <div className="container">
                <div className="row bg-dark text-center text-white">
                    <div className="col-3" onClick={() => this.sort(0)}>
                        Team
                    </div>
                    <div className="col-1" onClick={() => this.sort(1)}>
                        Played
                    </div>
                    <div className="col-1" onClick={() => this.sort(2)}>
                        Won
                    </div>
                    <div className="col-1" onClick={() => this.sort(3)}>
                        Lost
                    </div>
                    <div className="col-1" onClick={() => this.sort(4)}>
                        Drawn
                    </div>
                    <div className="col-2" onClick={() => this.sort(5)}>
                        Goals For
                    </div>
                    <div className="col-2" onClick={() => this.sort(6)}>
                        Goals Against
                    </div>
                    <div className="col-1" onClick={() => this.sort(7)}>
                        Points
                    </div>
                </div>
                <div className="row bg-light text-center text-dark">
                {team.map(tm=>
                <React.Fragment>
                    <div className="col-3 border-bottom">
                        {tm.name}
                    </div>
                    <div className="col-1 border-bottom">
                        {tm.played}
                    </div>
                    <div className="col-1 border-bottom">
                        {tm.won}
                    </div>
                    <div className="col-1 border-bottom">
                        {tm.lost}
                    </div>
                    <div className="col-1 border-bottom">
                        {tm.drawn}   
                    </div>
                    <div className="col-2 border-bottom">
                        {tm.goalsFor}
                    </div>
                    <div className="col-2 border-bottom">
                        {tm.goalsAgainst}
                    </div>
                    <div className="col-1 border-bottom">
                        {tm.points}
                    </div>
                </React.Fragment>
                )}
                </div>
            </div>
        )
    }
}
export default Points;