import React, {Component} from "react";
class AllMatches extends Component{
    render() {
        let {matches} = this.props;
        return (
            <div className="container">
            {matches.length===0 
            ?<h4 className="text-center">There are no matches</h4>
            :<React.Fragment>
                <div className="container text-center">
                    <h4 className="text-center">Result of the matches so far</h4>
                    <div className="row bg-dark text-white">
                    <div className="col-2 border-bottom">
                                Team1
                            </div>
                            <div className="col-2 border-bottom">
                            Team2
                            </div>
                            <div className="col-2 border-bottom">
                                Score
                            </div>
                            <div className="col-6 border-bottom">
                                Result
                            </div>
                            </div>
                    <div className="row">
                        {matches.map(mat=>
                        <React.Fragment>
                            <div className="col-2 border-bottom">
                                {mat.teamA}
                            </div>
                            <div className="col-2 border-bottom">
                                {mat.teamB}
                            </div>
                            <div className="col-2 border-bottom">
                                {mat.scoreA}-{mat.scoreB}
                            </div>
                            <div className="col-6 border-bottom">
                                {mat.result}
                            </div>
                            </React.Fragment>
                            )}
                    </div>
                </div>
            </React.Fragment>
            }
            </div>
        )
    }
}
export default AllMatches;