import React, {Component} from "react";
class Match extends Component{
    state={
        scoreA: 0,
        scoreB: 0,
    }
    Over = (scoreA,scoreB,teamA,teamB) =>{
        this.props.MatchOver(scoreA,scoreB,teamA,teamB)
    }
    score=(num)=>{
        let s1 = {...this.state};
        if(num===0){
            s1.scoreA++
        }else{
            s1.scoreB++
        }
        this.setState(s1)
    }
    render() {
        let {scoreA,scoreB} = this.state;
        let {teamA,teamB} = this.props;
        return (
            <div className="container">
            <h1 className="text-center">Welcome to an exciting match</h1><br/>
            <div className="text-center row">
                <div className="col-4">
                    <h3>{teamA}</h3>
                </div>
                <div className="col-4">
                    <h3>{scoreA}-{scoreB}</h3>
                </div>
                <div className="col-4">
                    <h3>{teamB}</h3>
                </div>
            </div>
            <div className="text-center row">
                <div className="col-4">
                    <button className="btn btn-warning" onClick={()=> this.score(0)}>Goal Score</button>
                </div>
                <div className="col-4">
                   
                </div>
                <div className="col-4">
                <button className="btn btn-warning" onClick={()=> this.score(1)}>Goal Score</button>
                </div>
            </div>
            <div className="text-center row">
                <div className="col-12">
                    <button className="btn btn-warning" onClick={() => this.Over(scoreA,scoreB,teamA,teamB)}>Match Over</button>
                </div>
            </div>
            </div>
        )
    }
}
export default Match;