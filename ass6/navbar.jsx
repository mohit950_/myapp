import React, { Component } from "react";
class NavBar extends Component {
  render() {
    let { matches } = this.props;
    return (
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">
          Football Tournament
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#">
               Number of Matches<span className="bg-primary badge badge-pill badge-primary">{matches.length}</span>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;