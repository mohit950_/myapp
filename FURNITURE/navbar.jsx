import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Navbar, Container, Nav } from "react-bootstrap";
import { reach } from "yup";
class NavBar extends Component {
  render() {
    let { user } = this.props;
    console.log(user);
    return (
      // <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      //   <Link class="navbar-brand" to="/">
      //     Furniture Store
      //   </Link>
      //   <button
      //     class="navbar-toggler"
      //     type="button"
      //     data-toggle="collapse"
      //     data-target="#navbarTogglerDemo02"
      //     aria-controls="navbarTogglerDemo02"
      //     aria-expanded="false"
      //     aria-label="Toggle navigation"
      //   >
      //     <span class="navbar-toggler-icon"></span>
      //   </button>

      //   <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      //     <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      //       <li class="nav-item active">
      //         <Link class="nav-link" to="/">
      //           Products
      //         </Link>
      //       </li>
      //       {user && user.role === "customer" ? (
      //         <li class="nav-item">
      //           <Link class="nav-link" to="/cart">
      //             Cart
      //           </Link>
      //         </li>
      //       ) : (
      //         ""
      //       )}
      //       {user && user.role === "admin" ? (
      //         <li class="nav-item">
      //           <Link class="nav-link" to="/addNewPrroduct">
      //             Add New Product
      //           </Link>
      //         </li>
      //       ) : (
      //         ""
      //       )}
      //       {!user ? (
      //         <li class="nav-item">
      //         <Link class="nav-link" to="/login">
      //           Sign In
      //         </Link>
      //       </li>
      //       ) : (
      //         <li class="nav-item">
      //         <Link class="nav-link" to="/logout">
      //           Logout
      //         </Link>
      //       </li>
      //         // </React.Fragment>
      //       )}
      //     </ul>
      //   </div>
      // </nav>

      <React.Fragment>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Container>
            <Navbar.Brand to="/">Furniture Store</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="me-auto text-dark">

                  <Link className="nav-link" to="/furniture">Products</Link>

                  {user && user.role==="customer"?<Link className="nav-link" to="/cart">CARTS</Link>
                  :""
                  }
                  {user && user.role==="admin"?<Link className="nav-link" to="/addproduct">Add new Product</Link>
                  :""
                  }
              </Nav>
              <Nav>
                {!user
                ?(<Link  className="nav-link" eventkey={2} to="/login">
                  Sign In
                </Link>)
                :(
                  // <React.Fragment>
                // {/* <h6>Welcome{user.email}</h6> */}
                  <Link  className="nav-link" eventkey={1} to="/logout">
                Logout
              </Link>
              // </React.Fragment>
              )
                }

              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </React.Fragment>
    );
  }
}
export default NavBar;
