import React, {Component} from "react"
import authservice from "./authservice";
import { Formik,Field,Form } from "formik"
import * as yup from "yup";

const loginValidationSchema = yup.object().shape({
    email: yup.string().email().typeError("PLease Enter Valid Mail Address").required("Email is mandatory"),
    password: yup.string().required("Password is mandatory"),
})

class Login extends Component{
    state={

    }
    login=(value)=>{
        let login = this.props.users.find(fi=>fi.email===value.email && fi.password===value.password)
        console.log(login)
        return login
    }
    render() {
        return(
            <div className="container">
                <Formik initialValues={{
                    email: "",
                    password: "",
                }}
                    validationSchema = {loginValidationSchema}
                    onSubmit = {(values)=>{
                      let LoginSuccess = this.login(values)
                      if(LoginSuccess){
                          authservice.login(LoginSuccess)
                          window.location = "/"  
                      }else{
                          alert ("Invalid Credentials")
                      }
                    }}
                    >
                        {({values,errors})=>(
                            <Form>
                        {console.log(errors)}
                        <React.Fragment>
                            <h4 className="text-center">Login</h4>
                            <h6>Email</h6>
                        <Field type="email" name="email" className="form-control" placeholder="Email here"/>
                            <h6>Password</h6>
                        <Field type="password" name="password" className="form-control" placeholder="password here" />
                        <button type="submit" className="btn btn-primary m-2">Submit</button>
                        </React.Fragment>
                    </Form>
                    )}
                </Formik>
            </div>
        )
    }
}
export default Login;