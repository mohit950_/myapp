import React, {Component} from "react";
import queryString from "query-string"
import authservice from "./authservice";
class Home extends Component{
    state={
        options: ["Dining","Drawing","Bedroom","Study"],
        details: "",
        option: "",
    }
    handleDetails = (da) => {
        // let s1 = {...this.state}
        // s1.details = da
        // this.setState(s1)
        this.props.history.push(`/furniture/${da.category}/${da.prodCode}`)
        // window.location.href = `/furniture/${da.category}/${da.prodCode}`
        // console.log(this.props)
    }
    handleChange=(e)=>{
        let {currentTarget: input} = e ;
        let s1 = {...this.state}
        s1.option = input.value;
        this.props.history.push(`/furniture/${input.value}`)
        this.setState(s1)

    }
    handleCart=(product)=>{
        this.props.onSubmit(product)
    }
    handleEdit=(details)=>{
        this.props.history.push(`/products/${details.prodCode}/edit`)
    }
    handleRemove=(details)=>{
        this.props.history.push(`/delete/${details.prodCode}`)
    }
    render() {
        let user = authservice.getUser()
        let {options,details,option} = this.state
        let cat = this.props.match.params.category
        let prodCode = this.props.match.params.prodCode
        option = cat ? cat : option;
        let data1 = option ? this.props.data.filter(fi=> fi.category===option) : this.props.data;
        console.log(data1)
        details = this.props.data.find(fi=> fi.prodCode === prodCode)
        return(
            <div className="container">
                <div className="row">
                    <div className="col-3">
                        <h6>Options</h6>
                            {this.makeRadio(options,"option",option)}
                    </div>
                    <div className="col-6">
                        <div className="row">
                        {data1.map(da=>
                            <React.Fragment>
                                <div className="col-5">
                                   <img className="mt-2 mb-2" src={da.img} width="70%" onClick={()=>this.handleDetails(da)}/>
                                
                                </div>
                        </React.Fragment>
                                
                                )}
                        </div>
                    </div>

                    <div className="col-3">
                        {!details ? <h6>Choose Product</h6> : user && user.role==="customer" ? (
                            <React.Fragment>
                            <img className="mt-2 mb-2" src={details.img} width="70%" /><br/>
                            <label style={{fontWeight:"bold"}}>{details.desc[0]}</label>
                            <label>{details.desc}</label>
                            <label>{details.desc[2]}</label>
                            <h6>Items in PRoduct</h6>
                            <div className="row">
                            {details.ingredients.map(dt=>
                            <div className="col-12">
                                <label>- {dt.ingName} : {dt.qty}</label>
                                </div>
                            )}
                                </div>
                            <button className="btn btn-success m-1" onClick={()=> this.handleCart(details)}>Add to Cart</button>

                            </React.Fragment>
                        ): user && user.role==="admin"
                        ?(
                            <React.Fragment>
                                <div className="row">
                                    <div className="col-3 btn btn-secondary btn-sm mx-1" onClick={()=>this.handleEdit(details)}>Edit Product</div>
                                    <div className="col-3 btn btn-secondary btn-sm" onClick={()=>this.handleRemove(details)}>Delete Product</div>
                                </div>
                            <img className="mt-2 mb-2" src={details.img} width="70%" /><br/>
                            <label style={{fontWeight:"bold"}}>{details.desc[0]}</label>
                            <label>{details.desc}</label>
                            <label>{details.desc[2]}</label>
                            <h6>Items in PRoduct</h6>
                            <div className="row">
                            {details.ingredients.map(dt=>
                            <div className="col-12">
                                <label>- {dt.ingName} : {dt.qty}</label>
                                </div>
                            )}
                                </div>

                            </React.Fragment>
                        )
                        :(
                            <React.Fragment>
                            <img className="mt-2 mb-2" src={details.img} width="70%" /><br/>
                            <label style={{fontWeight:"bold"}}>{details.desc[0]}</label>
                            <label>{details.desc}</label>
                            <label>{details.desc[2]}</label>
                            <h6>Items in PRoduct</h6>
                            <div className="row">
                            {details.ingredients.map(dt=>
                            <div className="col-12">
                                <label>- {dt.ingName} : {dt.qty}</label>
                                </div>
                            )}
                                </div>

                            </React.Fragment>
                        )
                        }
                    </div>

                </div>
            </div>
        )
    }
    makeRadio = (arr,name,cat="") => {
        console.log(cat,66)
        return(
            <div className="row border">

                {arr.map(ar=>
                <React.Fragment>
                <div className="col-12 m-3">
                    <input type="radio" value={ar} checked={ar===cat} name={name} className="form-check-input" onChange={this.handleChange} />
                    <label className="form-check-label">{ar}</label>
                </div>
                </React.Fragment>
                )}
            </div>
        )
    }

}
export default Home;
{/* <label className="form-check-label">{opt}</label>
</div> */}