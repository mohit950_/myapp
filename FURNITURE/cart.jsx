import React, { Component } from "react";
import { Accordion } from "react-bootstrap";
import authservice from "./authservice";
class Cart extends Component{
    
    logout=()=>{
        authservice.logout()
        window.location = "/furniture"
    }
    handleAdd = (product,incr) => {
        this.props.onAdd(product,incr)
    }        
    render() {
        console.log(this.props.cart)
        return (
            <div className="container">
            <h4 className="text-center">Products in Shopping Cart</h4>
            {this.props.cart.length>0
            ?
            <React.Fragment>
            <div className="row border">
                {this.props.cart.map(cr=>
                    <React.Fragment>
                        <div className="col-3">
                            <img src={cr.img} width="40%" />
                        </div>
                        <div className="col-6">
                            {cr.desc[0]}
                        </div>
                        <div className="col-3">
                            <i className="fas fa-plus btn btn-success" onClick={()=>this.handleAdd(cr,"plus")}></i><button className=" btn btn-secondary">{cr.quantity}</button><i className="fas fa-minus btn btn-danger" onClick={()=>this.handleAdd(cr,"minus")}></i>
                        </div>
                    </React.Fragment>
                    )}
            </div>
            <h4 className="text-center">List of Items in Cart</h4>
            <div className="row">
            <div className="col-2"></div>
                <div className="col-4 text-center bg-dark text-light">
                    Item Name
                </div>
                <div className="col-4 text-center bg-dark text-light">
                    Count
                </div>
                <div className="col-2"></div>
            </div>


            {this.props.cart.map(cr=>
            cr.ingredients.map(c=>
            
                <div className="row border">
                    {console.log(c.ingName,51)}
                <div className="col-2"></div>
                    <div className="col-4 text-center">
                        {c.ingName}
                    </div>
                    <div className="col-4 text-center">
                    {c.qty*cr.quantity}
                    </div>
                    <div className="col-2"></div>
                
                </div>
                
                )
                )}
            </React.Fragment>

            
            :""}
            </div>
        )
    }
}
export default Cart;