import React, {Component} from "react"
import { Route,Switch,Redirect } from "react-router";
import authservice from "./authservice";
import Home from "./home"
import Login from "./login";
import NavBar from "./navbar"
import Logout from "./logout";
import Cart from "./cart"
import EditPRoduct from "./editProduct";
import AddProduct from "./addProduct";
import Delete from "./delete";
class MainComponent extends Component{
    state={
        cart: [],
        data: [
            
		{
            prodCode:	"DS2S245",
            category:	"Dining",
            desc:	[
                    "Two	seater	Dining	Set",
                    "Built	from	High	quality	wood",
                    "1	year	warranty"
            ],
            img:
                    "https://www.hometown.in/media/product/61/9353/47156/1-catalog_255.jpg",
            ingredients:	[
                    {	ingName:	"Dining	Table",	qty:	1	},
                    {	ingName:	"Chair",	qty:	2	}
            ],
            title:	"Two	seater	Dining	Set"
    },
    {
            prodCode:	"DS6S761",
            category:	"Dining",
            desc:	[
                    "Six	Seater	Dining	Set	in	Antique	Cherry	Colour",
                    "Assembly	by	Skilled	Carpenters",
                    "Made	from	Teak	wood"
            ],
            img:
                    "https://www.hometown.in/media/product/03/9453/94498/1-catalog_255.jpg",
            ingredients:	[
                    {	ingName:	"Dining	Table",	qty:	1	},
                    {	ingName:	"Chair",	qty:	4	},
                    {	ingName:	"Bench",	qty:	1	}
            ],
            title:	"Six	Seater	Dining	Set"
    },
    {
            prodCode:	"DS4S177",
            category:	"Dining",
            desc:	[
                    "Mild	Steel	Four	Seater	Dining	Set	in	Black	Colour",
                    "Knock-down	construction	for	easy	transportation"
            ],
            img:
                    "https://www.hometown.in/media/product/99/3753/86779/1-catalog_255.jpg",
            ingredients:	[
                    {	ingName:	"Dining	Table",	qty:	1	},
                    {	ingName:	"Chair",	qty:	4	}
            ],
            title:	"Mild	Steel	Dining	Set"
    },
    {
            prodCode:	"DC2S705",
            category:	"Dining",
            desc:	[
                    "Solid	Wood	Dining	Chair	Set	of	Two	in	Dark	Walnut	Colour",
                    "Beautiful	design	carved	on	dining	chair",
                    "Dining	chair	seat	upholstered	in	dark	brown	Fabric"
            ],
            img:
                    "https://www.hometown.in/media/product/87/9453/34067/1-catalog_255.jpg",
            ingredients:	[{	ingName:	"Chair",	qty:	2	}],
            title:	"Dining	Chair	Set"
    },
    {
            prodCode:	"BN1S388",
            category:	"Dining",
            desc:	[
                    "Solid	Wood	Dining	Bench	in	Dark	Walnut	Colour",
                    "Comfortable	bench	for	a	relaxed	dinner"
            ],
            img:
                    "https://www.hometown.in/media/product/94/0553/11804/1-catalog_255.jpg",
            ingredients:	[{	ingName:	"Bench",	qty:	1	}],
            title:	"Dining	Bench"
    },
    {
            prodCode:	"SF2S532",
            category:	"Drawing",
            desc:	[
                    "Characteristic	Rising	Track	Arm	Rest	Design",
                    "Premium	High	Gloss	Leatherette	Upholstery",
                    "Independent	Headrest	And	Lumber	Support"
            ],
            img:
                    "https://www.hometown.in/media/product/48/2753/29530/1-catalog_255.jpg",
            ingredients:	[{	ingName:	"Sofa",	qty:	1	}],
            title:	"Two	Seater	Sofa"
    },
    {
            prodCode:	"SF2S206",
            category:	"Drawing",
            desc:	["Two	Seater	Sofa	in	Blue	Colour",	"Assembly	by	Skilled	Carpenters"],
            img:
                    "https://www.hometown.in/media/product/20/3453/62912/1-catalog_255.jpg",
            ingredients:	[{	ingName:	"Sofa",	qty:	1	}],
            title:	"Two	Seater	Sofa"
    },
    {
            prodCode:	"SFBD311",
            category:	"Drawing",
            desc:	[
                    "Sofa	Cum	bed	in	Brown	Colour",
                    "Ply-wood	construction	with	hand	polished	finish",
                    "Removable	fabric	cover	on	best	quality	foam	mattress",
                    "Throw	cushions	and	bolsters	come	with	the	product"
            ],
            img:
                    "https://www.hometown.in/media/product/30/3253/57132/1-catalog_255.jpg",
            ingredients:	[{	ingName:	"Sofa",	qty:	1	},	{	ingName:	"Cushions",	qty:	2	}],
            title:	"Sofa	cum	Bed"
    },
    {
            prodCode:	"BDQS381",
            category:	"Bedroom",
            desc:	[
                    "Wood	Box	Storage	King	Size	Bed	in	Wenge	Colour	",
                    "Box	Storage	included	for	Maximum	space	utilization",
                    "Mattress	is	included"
            ],
            img:
                    "https://www.hometown.in/media/product/43/8153/54285/1-catalog_255.jpg",
            ingredients:	[{	ingName:	"Bed",	qty:	1	},	{	ingName:	"Mattress",	qty:	2	}],
            title:	"King	size	Bed"
    },
    {
            prodCode:	"BDQS229",
            category:	"Bedroom",
            desc:	[
                    "Wood	Hydraulic	Storage	Queen	Size	Bed",
                    "Half	hydraulic	storage",
                    "Superior	E2	grade	MDF	used	with	melamine	finish"
            ],
            img:
                    "https://www.hometown.in/media/product/74/1153/94332/1-catalog_255.jpg",
            ingredients:	[{	ingName:	"Bed",	qty:	1	}],
            title:	"Queen	size	Bed"
    },
    {
            prodCode:	"ST1T425",
            category:	"Study",
            desc:	[
                    "Wood	Study	Table	in	Walnut	Colour",
                    "Assembly	by	Skilled	Carpenters",
                    "Built	from	High	Quality	Wood"
            ],
            img:
                "https://www.hometown.in/media/product/02/9153/44662/1-catalog_255.jpg",
            ingredients:	[{	ingName:	"Study	Table",	qty:	1	}],
            title:	"Study	Table"
    },
    {
            prodCode:	"ST1T588",
            category:	"Study",
            desc:	[
                    "	Wood	Study	Table	in	Highgloss	White	&	Blue	Colour",
                    "Study	table	comes	with	bookshelf	on	top,	5	drawers	&	1	open	shelf",
                    "Superior	quality	MDF	with	stain	resistant	melamine	finish"
            ],
            img:
                    "https://www.hometown.in/media/product/07/9553/45053/1-catalog_255.jpg",
            ingredients:	[{	ingName:	"Study	Table",	qty:	1	}],
            title:	"Study	Table"
    }
],
        users:[{email: "mohit@test.com",password: "mohit", role: "admin"},{email: "tony@stark.com",password: "stark", role: "customer"}] 
    }
    handleCart =(products)=> {
            let {cart} = this.state;
            let cart1 = [...cart]
            let index = cart1.find(fi=> fi.prodCode === products.prodCode)
            if(index>=0){
                    cart1.splice(index,1)
            }else{
                    let pr = {...products,quantity: 1}
                    cart1.push(pr)
            }
            this.setState({ cart : cart1 })
    }
    handleAddQuantity=(products,incr) =>{
            console.log(products,191)
            console.log(incr,192)
        let {cart} = this.state;
            let cart1 = [...cart]
            let index = cart1.findIndex(fi=> fi.prodCode === products.prodCode)
            console.log(index,196)
            if(index>=0){
                    if(cart1[index].quantity===1 && incr==="minus") cart1.splice(index,1)
                    else{
                            if(incr==="minus") incr = -1
                            else incr =1 
                            cart1[index].quantity = cart1[index].quantity + incr
                    }
            }
            console.log(cart1)
            this.setState({ cart : cart1 })
    }
    handleAddProduct = (product) => {
            let {data} = this.state;
            let data1= [...data]
            data1.push(product)
            this.setState({data:data1})
    }
    handleEditProduct = (product) => {
                let {data} = this.state;
                let data1= [...data]
                let index = data1.findIndex(fi=> fi.prodCode === product.prodCode)
                if(index>=0){
                        data1[index] = product
                        this.setState({data:data1})
                        }
        }
        handleRemove = (prodCode) => {
                let {data} = this.state;
                let data1= [...data]
                let index = data1.findIndex(fi=> fi.prodCode === prodCode)
                if(index>=0){
                        data1.splice(index,1)
                        this.setState({data:data1})
                }
        }
    render() {
            let user = authservice.getUser()
            console.log(this.state.cart)
        return(
            <div className="fluid-container">
                <NavBar user={user} />
                <Switch>
                    <Route path="/furniture/:category/:prodCode" render={(props)=> <Home {...props} onSubmit={this.handleCart} data={this.state.data} /> }/>
                    <Route path="/furniture/:category" render={(props)=> <Home {...props} cart={this.state.cart} data={this.state.data} /> }/>
                    <Route path="/furniture" render={(props)=> <Home {...props} cart={this.state.cart} data={this.state.data} /> }/>
                    <Route path="/cart" render={(props)=> <Cart {...props} cart={this.state.cart} onAdd={this.handleAddQuantity}/> }/>
                    <Route path="/products/:prodCode/edit" render={(props)=> <EditPRoduct {...props} data={this.state.data} onAdd={this.handleAdd}/> }/>
                    <Route path="/addproduct" render={(props)=> <AddProduct {...props} data={this.state.data} onSubmit={this.handleAddProduct}/> }/>
                    <Route path="/login" render={(props)=> <Login {...props} users={this.state.users}  onSubmit={this.handleEditProduct}/> }/>
                    <Route path="/logout" render={(props)=> <Logout {...props} /> }/>
                    <Route path="/delete/:prodCode" render={(props)=> <Delete {...props} onSubmit={this.handleRemove} /> }/>
                    <Redirect from="/" to="/furniture" />
                </Switch>

            </div>
        )
    }
}
export default MainComponent;