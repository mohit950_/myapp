import { Formik,Form,Field, FieldArray,ErrorMessage } from "formik";
import React, { Component } from "react";
import * as yup from "yup";

const AddValidations = yup.object().shape({
    prodCode:yup.string().required("Required"),
    category:yup.string().required("Required"),
    desc:yup.array().required("Required").min(1,"At least 1 item"),
    img: yup.string().required("Required"),
    ingredients:  yup.array().of(
        yup.object().shape({
            ingName: yup.string().required("Required").min(3,"Max 3 Characters"),
            qty: yup.number().typeError("must be a number").required("Required").min(0,"Qty is greater than 0")
        })
    ).required("Required").min(1,"At least 1 item"),
    title: yup.string().required("Required"),  
})

class AddProduct extends Component {
  state = {
    options: ["Choose Category", "Dining", "Drawing", "Bedroom", "Study"],
  };
  render() {
    let prodCode = this.props.match.params.prodCode;
    // let product = this.props.data.find((fi) => fi.prodCode === prodCode);

    return (
        <div className="container mt-3">
        <Formik
          initialValues={{
            prodCode:"",
            category:"",
            desc:[],
            img: "",
            ingredients:  [],
            title: "",
          }}
          validationSchema = {AddValidations}
          onSubmit={(values) => {
              console.log(values)
              this.props.onSubmit(values)
              this.props.history.push("/furniture")
            //   alert("MOHIT")
            // this.submit(values);14
          }}
        >
          {({ values, errors }) => (
            <React.Fragment>
              <Form>
                <label>ProdCode</label>
                <Field type="text" className="form-control" name="prodCode" />
                <div className="form-group text-danger">
                    <ErrorMessage name="prodCode" />
                  </div>
                <label>Name</label>
                <Field type="text" className="form-control" name="title" />
                <div className="form-group text-danger">
                    <ErrorMessage name="title" />
                  </div>
                <label>imageURl</label>
                <Field type="text" className="form-control" name="img" />
                <div className="form-group text-danger">
                    <ErrorMessage name="img" />
                  </div>
                <label>imageURl</label>
                <Field as="select" className="form-control" name="category">
                {this.state.options.map((opt) => (
                  <option>{opt}</option>
                ))}
                </Field>
                <div className="form-group text-danger">
                    <ErrorMessage name="category" />
                  </div>
                <FieldArray
              name="desc"
              render={(arrayHelpers) => (
                <div>
                {/* {console.log(values.descriptions)} */}
                  {values.desc.map((p1, index) => (
                    <div className="row mb-2" key={index}>
                      <div className="col-4">
                        <Field
                          name={`desc[${index}]`}
                          className="form-control"
                          type="text"
                          placeholder="Description"
                        />
                      </div>

                      <div className="col-2 align-middle">
                        <button
                          className="btn btn-danger mr-2"
                          type="button"
                          onClick={() => arrayHelpers.remove(index)}
                        ><i className="fas fa-times"></i>
                        </button>

                      </div>
                      
                    </div>
                  ))}
                  <button
                    className="btn btn-secondary mr-2"
                    type="button"
                    onClick={() => arrayHelpers.push("")}
                  >
                    Add Description
                  </button>
                  <div className="form-group text-danger">
                    <ErrorMessage name="desc" />
                  </div>
                </div>
              )}
            />
              
                <FieldArray
                  name="ingredients"
                  render={(arrayHelpers) => (
                    <div>
                      
                      {values.ingredients.map((des, index) => (
                        <div className="row">
                          <div className="col-4">
                            <Field
                              name={`ingredients[${index}].ingName`}
                              className="form-control"
                              type="text"
                              placeholder="Ing Name"
                            />
                          </div>
                          <div className="col-4">
                            <Field
                              name={`ingredients[${index}].qty`}
                              className="form-control"
                              type="text"
                              placeholder="Ing Quantity"
                            />
                          </div>
                          <div className="col-2 align-middle">
                            <button
                              className="btn btn-danger mr-2"
                              type="button"
                              onClick={() => arrayHelpers.remove(index)}
                            >
                              <i className="fas fa-times"></i>
                            </button>
                          </div>
                        </div>
                      ))}
                      <button
                    className="btn btn-secondary mr-2"
                    type="button"
                    onClick={() => arrayHelpers.push("")}
                  >
                    Add Ingredients
                  </button>
                  <div className="form-group text-danger">
                    {typeof errors.ingredients === "string"
                      ? errors.ingredients
                      : errors.ingredients
                      ? errors.ingredients.reduce(
                          (acc, curr) =>
                            acc
                              ? acc
                              : curr
                              ? curr.ingName || curr.qty
                              : acc,
                          ""
                        )
                      : ""}
                  </div>

                    </div>
                  )}
                  
                />
                <div className="form-group">
                  <button type="submit" className="btn btn-primary m-2">
                    {prodCode ? "Update" : "Add"}
                  </button>
                </div>
              </Form>
            </React.Fragment>
          )}
        </Formik>
        </div>
    );
  }
}
export default AddProduct