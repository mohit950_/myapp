import React, {Component} from "react";
class NavBar extends Component{
    render(){
        let { handleView,active } = this.props;
        let ss={
            textDecoration: "none",
            color: "white"
        }
        return(
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand">
                Billing System
            </a>
            <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class={"nav-item "+active}>
                    <a class="nav-a" style={ss} onClick={() => handleView(0)}>
                    New Bill
                    </a>
                </li>
                </ul>
            </div>
            </nav>
            )
        }
    }
    export default NavBar;