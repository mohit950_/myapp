import React, {Component} from "react";
class DropDown extends Component{
    render(){
        let {dd,sizes,crusts} = this.props
        let {size,crust} = dd;
        return(
            <React.Fragment>
            <div className="col-6">
            {this.makeDD("Select Sizes","size",size,sizes)}
            </div>
            <div className="col-6">
            {this.makeDD("Select Crusts","crust",crust,crusts)}
            </div>
            </React.Fragment>
        )
    }
    makeDD = (label,name,value,arr,bool="") =>{
        return(
        <select id={name} name={name} onChange={this.handleChange} value={value} className="form-control" disabled={bool}>
            <option value="" disabled>{label}</option>
            {arr.map(ar=><option>{ar}</option>)}
        </select>
        )
    }
}
export  default DropDown;