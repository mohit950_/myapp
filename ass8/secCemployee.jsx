import React, {Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string"
import LeftPanel from "./leftpanel";
import Button from "./button";
class Employee extends Component{
    state={
        change: 0
    }
    filterParams = (arr,queryParams) => {
        let{designation,department} = queryParams 
        arr = this.CallfilterParams(arr,"department",department);    
        arr = this.CallfilterParams(arr,"designation",designation);    
        return arr;
      }
    
      CallfilterParams = (arr,names,value) =>{
        if(!value) return arr;
        console.log(value,"line 19")
        let valueArr = value.split(",")
        let arr1 = arr.filter(a1=> valueArr.find(val=>val===a1[names]))
        console.log(valueArr,"line 21")
        return arr1;
      }
    render() {
        let {employee,Department,Designation} = this.props;
        let {city} = this.props.match.params
        let queryParams = queryString.parse(this.props.location.search)
        console.log(queryParams)
        let {page=1,department="",designation=""} = queryParams
        let pageNum=0
            pageNum=page
        console.log(page)
        let employee1 = !city ? employee : city==="NewDelhi" ? employee.filter(fi=>fi.location==="New Delhi") : city==="Noida" ? employee.filter(fi=>fi.location==="Noida") : "";
        console.log(employee1)
        employee1 = this.filterParams(employee1,queryParams)

        let size = 1
        
        let startIndex = (pageNum-1)*size
        console.log(page)

        let endIndex = employee1.length > startIndex + size - 1 ? startIndex + size +1 : employee1.length-1
        let employee2 =
        employee1.length > 2
            ? employee1.filter((emp, index) => index >= startIndex && index < endIndex)
            : employee1;

            return(
                <React.Fragment>
                <div className="row">
                    <div className="col-3">
                        <LeftPanel Department={Department} Designation={Designation} queryParams={queryParams} onOptionChange={this.handleCheckBox} city={city}/>
                    </div>
                    <div className="col-9">
                        <div className="row">
                            <div className="col-12">
                                <h4 className="text-center">Welcome to employee portal</h4>
                                <h6>You have choosen</h6>
                                <label>Location: {!city ? 'All' : city}</label><br/>
                                <label>Department: {!department ? "All" : queryParams.department}</label><br/>
                                <label>Designation: {!designation ? "All" : queryParams.designation}</label><br/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <label>The number of employees matching the option :{employee1.length}</label>
                            </div>
                        </div>
                        <div className="row">
                                {employee2.map(emp=>
                                <div className="col-6 border bg-light">
                                    <label style={{fontWeight:"bold"}}>{emp.name}</label><br/>
                                    <label>{emp.email}</label><br/>
                                    <label>{emp.mobile}</label><br/>
                                    <label>{emp.location}</label><br/>
                                    <label>{emp.department}</label><br/>
                                    <label>{emp.designation}</label><br/>
                                    <label>{emp.salary}</label><br/>
                                </div>
                                )}
                        </div>
                        <div className="row">
                            <div className="col-2">
                                {startIndex > 0
                                ?this.state.change===1 
                                ?<button className="btn btn-primary" onClick={()=> {this.prevPage(city,queryParams)}}>Prev</button>
                                :<button className="btn btn-primary" onClick={()=> {this.prevPage(city,queryParams)}}>Prev</button>
                                :""}
                            </div>
                            <div className="col-8"></div>
                            <div className="col-2">
                                { (endIndex < employee1.length-1)             
                                    ?this.state.change===1 
                                    ?<button className="btn btn-primary" onClick={()=> {this.handlePage2(city,queryParams)}}>fext</button>
                                    :<button className="btn btn-primary" onClick={()=> {this.handlePage2(city,queryParams)}}>Next</button>
                                    :""}
                            </div>

                        </div>
                    </div>
                </div>
                </React.Fragment>
                );
        }
        prevPage = (city,queryParams) => {
            if(queryParams.page>=1){
                queryParams.page=(+queryParams.page)-1
            }
            this.handleButton(city,queryParams);
        }
        handlePage2 =(value,queryParams)=>{
            let {page} = queryParams
            if(!queryParams.page){
                queryParams.page=1
            }
            else if(queryParams.page>=1){
                queryParams.page=(+queryParams.page)+1
            }
                this.handleButton(value,queryParams);
        }
        // handlePage = (queryParams) => {
        //     console.log(queryParams)
        //     let s1 =  {...this.state};
        //     let {page} = queryParams
        //         if(queryParams){
        //             queryParams.page=1
        //         }
        //     s1.change=1
        //         this.handleButton(queryParams);
        //     this.setState(s1)
        // }
        handleButton = (value,queryParams) => {
            console.log(value)
            if(!value) this.callUrl("/emps" , queryParams)
            else{
                this.callUrl(`/emp/${value}` , queryParams)
            }

        }
        handleCheckBox = (value,queryParams) => {
            if(!value) this.callUrl("/emps" , queryParams)
            else{
                this.callUrl(`/emp/${value}` , queryParams)
            }
        }
        callUrl = (url,options) => {
            let searchString = this.makeSearchstring(options)
            console.log("ulr" ,url)
            console.log("path" ,options)
            this.props.history.push({
              pathname: url,
              search: searchString
            })
        }
        makeSearchstring = (queryParams) => {
            let{department,designation,page} = queryParams 
            let searchStr = "";
            searchStr = this.addtoQueryString(searchStr,"department",department);
            searchStr = this.addtoQueryString(searchStr,"designation",designation);
            searchStr = this.addtoQueryString(searchStr,"page",page);
            return searchStr;
        }
        addtoQueryString =(str,paramName,paramValue) => 
                paramValue 
                ? str
                ? `${str}&${paramName}=${paramValue}`
                : `${paramName}=${paramValue}`
                : str;
        prev = (page) => {
            console.log(page)
            this.props.history.push(`/emps?page=${page}`)
    }
    send = (queryParams) => {
        let{department,designation,page} = queryParams 
        this.callUrl("/emps",queryParams)
        console.log(queryParams,"queryParams")
        if(queryParams){
            this.makeSearchstring(queryParams)
        }
        let str=""
        let search = this.addtoQueryString(str,"page",page)
        console.log(search)
        let s1 = {...this.state};
        s1.change=1
        this.props.history.push("/emps?page=2")
        this.setState(s1)
    }
    send2 = (page) => {
        this.props.history.push(`/emps?page=${page}`)
    }
    

    // { startIndex > 0 ? <Link to={`/emps?page=${pageNum-1}`}>Prev</Link> : "" }
// { (endIndex < employee1.length-1) && page ? <Link to={`/emps/?page=${pageNum+1}`}>Next</Link> : (endIndex < employee1.length-1) && page ? <Link to={`/emps?page=2`}>Next</Link> :"" } */}
}
export default Employee;