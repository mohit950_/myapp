import React, {Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string"
import LeftPanel from "./leftpanel";
class Employee extends Component{
    state={
        change: 0
    }
    render() {
        let {employee,Department,Designation} = this.props;
        let {city} = this.props.match.params
        let queryParams = queryString.parse(this.props.location.search)
        console.log(queryParams)
        let {page=""} = queryParams
        let pageNum=0
        if(page){
            pageNum=page
        }else{
            pageNum=1
        }
        console.log(page)
        let employee1 = city==="NewDelhi" ? employee.filter(fi=>fi.location==="New Delhi") : city==="Noida" ? employee.filter(fi=>fi.location==="New Delhi") : employee;
        console.log(employee1)

        let size = 2
        
        let startIndex = (pageNum-1)*size
        console.log(page)

        let endIndex = employee1.length > startIndex + size - 1 ? startIndex + size - 1 : employee1.length-1
        let employee2 =
        employee1.length > 2
            ? employee1.filter((emp, index) => index >= startIndex && index <= endIndex)
            : employee1;

            return(
                <div className="row">
                    <div className="col-3">
                        <LeftPanel Department={Department} Designation={Designation} />
                    </div>
                    <div className="col-9">
                        <div className="row">
                            <div className="col-12">
                                <h4 className="text-center">Welcome to employee portal</h4>
                                <h6>You have choosen</h6>
                                <label>Location: {!city ? 'All' : city}</label><br/>
                                <label>Department: </label><br/>
                                <label>Designation: </label><br/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <label>The number of employees matching the option :{employee1.length}</label>
                            </div>
                        </div>
                        <div className="row">
                                {employee2.map(emp=>
                                <div className="col-6">
                                    <label style={{fontWeight:"bold"}}>{emp.name}</label><br/>
                                    <label>{emp.email}</label><br/>
                                    <label>{emp.mobile}</label><br/>
                                    <label>{emp.location}</label><br/>
                                    <label>{emp.department}</label><br/>
                                    <label>{emp.designation}</label><br/>
                                    <label>{emp.salary}</label><br/>
                                </div>
                                )}
                        </div>
                        <div className="row">
                            <div className="col-2">
                                {startIndex > 0
                                ?this.state.change===1 
                                ?<button className="btn btn-primary" onClick={()=> {this.prev((+queryParams.page)-1)}}>Prev</button>
                                :<button className="btn btn-primary" onClick={()=> {this.prev((+queryParams.page)-1)}}>Prev</button>
                                :""}
                            </div>
                            <div className="col-8"></div>
                            <div className="col-2">
                                { (endIndex < employee1.length-1)             
                                    ?this.state.change===1 
                                    ?<button className="btn btn-primary" onClick={()=> {this.send2((+queryParams.page)+1)}}>Next</button>
                                    :<button className="btn btn-primary" onClick={()=> {this.send()}}>Next</button>
                                    :""}
                            </div>
                        </div>
                    </div>
                </div>
                );
        }
        
    }
export default Employee;