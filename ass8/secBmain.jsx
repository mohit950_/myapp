import React, {Component} from "react";
import {Route,Switch,Redirect} from "react-router-dom";
import NavBar from "./secBnavbar"
import DropDown from "./secBDD"
class MainComponent extends Component{
    state={
        sizes: ["Regular","Medium","Large"],
        crusts: ["New Hand Tossed","Wheat Thin Crust","Cheese Burst","Fresh Pan Pizza","Classic Hand Tossed"],
        items: [
            {"id":"MIR101","image":"https://i.ibb.co/SR1Jzpv/mirinda.png","type":"Beverage","name":"Mirinda","desc":"Mirinda","veg":"Yes"},
            {"id":"PEP001","image":"https://i.ibb.co/3vkKqsF/pepsiblack.png","type":"Beverage","name":"Pepsi Black Can","desc":"Pepsi Black Can","veg":"Yes"},
            {"id":"LIT281","image":"https://i.ibb.co/27PvTng/lit.png","type":"Beverage","name":"Lipton Iced Tea","desc":"Lipton Iced Tea","veg":"Yes"},
            {"id":"PEP022","image":"https://i.ibb.co/1M9xDZB/pepsi-new20190312.png","type":"Beverage","name":"Pepsi New","desc":"Pepsi New","veg":"Yes"},
            {"id":"BPCNV1","image":"https://i.ibb.co/R0VSJjq/Burger-Pizza-Non-Veg-nvg.png","type":"Burger Pizza","name":"Classic Non Veg","desc":"Oven-baked buns with cheese, peri-peri chicken, tomato & capsicum in creamy mayo","veg":"No"},
            {"id":"BPCV03","image":"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel1.png","type":"Burger Pizza","name":"Classic Veg","desc":"Oven-baked buns with cheese, tomato & capsicum in creamy mayo","veg":"Yes"},
            {"id":"BPPV04","image":"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel1.png","type":"Burger Pizza","name":"Premium Veg","desc":"Oven-baked buns with cheese, paneer,tomato, capsicum & red paprika in creamy mayo","veg":"Yes"},
            {"id":"DIP033","image":"https://i.ibb.co/0mbBzsw/new-cheesy.png","type":"Side Dish","name":"Cheesy Dip","desc":"An all-time favorite with your Garlic Breadsticks & Stuffed Garlic Bread for a Cheesy indulgence","veg":"Yes"},
            {"id":"DIP072","image":"https://i.ibb.co/fY52zBw/new-jalapeno.png","type":"Side Dish","name":"Cheesy Jalapeno Dip","desc":"A spicy, tangy flavored cheese dip is a an absolute delight with your favourite Garlic Breadsticks","veg":"Yes"},
            {"id":"GAR952","image":"https://i.ibb.co/BNVmfY9/Garlic-bread.png","type":"Side Dish","name":"Garlic Breadsticks","desc":"Baked to perfection. Your perfect pizza partner! Tastesbest with dip","veg":"Yes"},
            {"id":"PARCH1","image":"https://i.ibb.co/prBs3NJ/Parcel-Nonveg.png","type":"Side Dish","name":"Chicken Parcel","desc":"Snacky bites! Pizza rolls with chicken sausage & creamy harissa sauce","veg":"No"},
            {"id":"PARVG7","image":"https://i.ibb.co/JHhrM7d/Parcel-Veg.png","type":"Side Dish","name":"Veg Parcel","desc":"Snacky bites! Pizza rolls with paneer & creamy harissa sauce","veg":"Yes"},
            {"id":"PATNV7","image":"https://i.ibb.co/0m89Jw9/White-Pasta-Nvg.png","type":"Side Dish","name":"White Pasta Italiano Non-Veg","desc":"Creamy white pasta with pepper barbecue chicken","veg":"No"},
            {"id":"PATVG4","image":"https://i.ibb.co/mv8RFbk/White-Pasta-Veg.png","type":"Side Dish","name":"White Pasta Italiano Veg","desc":"Creamy white pasta with herb grilled mushrooms","veg":"Yes"},
            {"id":"DES044","image":"https://i.ibb.co/gvpDKPv/Butterscotch.png","type":"Dessert","name":"Butterscotch Mousse Cake","desc":"Sweet temptation! Butterscotch flavored mousse","veg":"Yes"},
            {"id":"DES028","image":"https://i.ibb.co/nm96NZW/ChocoLava.png","type":"Dessert","name":"Choco Lava Cake","desc":"Chocolate lovers delight! Indulgent,gooey molten lava inside chocolate cake","veg":"Yes"},
            {"id":"PIZVDV","image":"https://i.ibb.co/F0H0SWG/deluxeveg.png","type":"Pizza","name":"Deluxe Veggie","desc":"Veg delight - onion, capsicum, grilled mushroom, corn & paneer","veg":"Yes"},
            {"id":"PIZVFH","image":"https://i.ibb.co/4mHxB5x/farmhouse.png","type":"Pizza","name":"Farmhouse","desc":"Delightful combination of onion, capsicum, tomato & grilled mushroom","veg":"Yes"},
            {"id":"PIZVIT","image":"https://i.ibb.co/sRH7Qzf/Indian-TandooriPaneer.png","type":"Pizza","name":"Indi Tandoori Paneer","desc":"It is hot. It is spicy. It is oh-soIndian. Tandoori paneer with capsicum, red paprika & mint mayo","veg":"Yes"},
            {"id":"PIZVMG","image":"https://i.ibb.co/MGcHnDZ/mexgreen.png","type":"Pizza","name":"Mexican Green Wave","desc":"Mexican herbs sprinkled on onion, capsicum, tomato & jalapeno","veg":"Yes"},
            {"id":"PIZVPP","image":"https://i.ibb.co/cb5vLX9/peppypaneer.png","type":"Pizza","name":"Peppy Paneer","desc":"Flavorful trio of juicy paneer, crisp capsicum with spicy red paprika","veg":"Yes"},
            {"id":"PIZVVE","image":"https://i.ibb.co/gTy5DTK/vegextra.png","type":"Pizza","name":"Veg Extravaganza","desc":"Black olives, capsicum, onion, grilled mushroom, corn, tomato, jalapeno & extra cheese","veg":"Yes"},
            {"id":"PIZNCP","image":"https://i.ibb.co/b5qBJ9d/cheesepepperoni.png","type":"Pizza","name":"Chicken Pepperoni","desc":"A classic American taste! Relish the delectable flavor of Chicken Pepperoni,topped with extra cheese","veg":"No"},
            {"id":"PIZNCD","image":"https://i.ibb.co/GFtkbB1/ChickenDominator10.png","type":"Pizza","name":"Chicken Dominator","desc":"Loaded with double pepperbarbecue chicken, peri-peri chicken, chicken tikka & grilled chicken rashers","veg":"No"},
            {"id":"PIZNPB","image":"https://i.ibb.co/GxbtcLK/Pepper-Barbeque-OnionC.png","type":"Pizza","name":"Pepper Barbecue & Onion","desc":"A classic favourite with pepperbarbeque chicken & onion","veg":"No"},
            {"id":"PIZNIC","image":"https://i.ibb.co/6Z5wBqr/Indian-Tandoori-ChickenTikka.png","type":"Pizza","name":"Indi Chicken Tikka","desc":"The wholesome flavour of tandoori masala with Chicken tikka, onion, red paprika & mint mayo","veg":"No"}
        ],
        view:-1,
        cart:[],
        size:"",
        crust:"",
        errors: {},
        order: []
    }
    handleView = (num) => {
        let s1 = {...this.state}
        s1.view=num
        this.setState(s1)
    }
    handleChange =(index,e) => {
        console.log(index)
        let s1 = {...this.state};
        let {currentTarget:input} = e;
        s1.order[index][input.name] = input.value
        console.log(s1.order[index])
        this.setState(s1)
    }

    validateSize = (size) => (!size ? alert("Please Select Size") : "");
    validateCrust = (crust) => (!crust ? alert("Please Select Crust") : "");

    handleValidate = (prd) => {
        let s1 = {...this.state};
        console.log(prd.size)
        s1.errors.size = this.validateSize(prd.size)
        s1.errors.crust = this.validateCrust(prd.crust)
        this.setState(s1)
        console.log(!s1.errors.size && !s1.errors.crust)
        return !(!s1.errors.size && !s1.errors)
    }
    handleSubmit2 = (dd) => {

    }
    addCart = (pr) => {
        let s1 = {...this.state};
        let index=s1.cart.findIndex(fi=>fi.name===pr.name)
        let ordIndex = s1.order.findIndex(fi=>fi.name===pr.name)
        console.log(ordIndex)
        let prd = {image: pr.image,name: pr.name, desc: pr.desc, price: pr.price, size: s1.order[ordIndex].size, crust: s1.order[ordIndex].crust}
        if(index<0){
            s1.errors.size = this.validateSize(prd.size)
            s1.errors.crust = this.validateCrust(prd.crust)
        }
        console.log(s1.errors)
        if(index<0 && (s1.errors.size=="" && s1.errors.crust=="")){
            console.log("index")
            let quantity = 1
            s1.cart.push({image: prd.image,name: prd.name, desc: prd.desc, price: prd.price, size: prd.size, crust: prd.crust, quantity: quantity})   
        }
        else if(index>=0){
            console.log("index")
            s1.cart[index].quantity=s1.cart[index].quantity+1;
        }
        s1.order.splice(0)
        this.setState(s1)
    }
    addCart2 = (prd) => {
        let s1 = {...this.state};
        let index=s1.cart.findIndex(fi=>fi.name===prd.name)
        if(index<0){
            console.log("index")
            let quantity = 1
            s1.cart.push({image: prd.image,name: prd.name, desc: prd.desc, price: prd.price, quantity: quantity})   
        }
        else if(index>=0){
            console.log("index")
            s1.cart[index].quantity=s1.cart[index].quantity+1;
        }
        this.setState(s1)
    }
    render() {
        let {view,items,crusts,sizes,crust,size,cart,errors,order} = this.state;
        let filter = view===0 ? items.filter(it=> it.type==="Pizza" && it.veg==="Yes") : []
        filter = view===1 ? items.filter(it=> it.type==="Pizza" && it.veg==="No") : filter
        filter = view===2 ? items.filter(it=> it.type==="Dessert") : filter
        filter = view===3 ? items.filter(it=> it.type==="Side Dish") : filter
        let name1 = filter.reduce((acc,curr) => acc.find(fi=> fi.name===curr.name) ? acc : [...acc,curr.name] ,[])
        name1.map(fi => order.push({name: fi,size: "",crust: ""}))
        console.log(order)
        return(
            <div className="container">
                <NavBar handleView={this.handleView}/>
                <div className="row">
                    <div className="col-8 border">
                    {view===0 || view===1? (
                        <div className="row">
                            {filter.map((fi,index)=>
                            <div className="col-6 text-center">
                                <img src={fi.image} style={{width: "70%"}} /><br/>
                                <label style={{fontWeight: "bold"}}>{fi.name}</label><br/>
                                <label className="mt-0">{fi.desc}</label><br/>
                                <div className="row">
                                    {/* <DropDown dd={dd} onSubmit2={this.handleSubmit2} sizes={sizes} crust={crust} /> */}
                                    {cart.length===0 || cart.findIndex(car=>car.name===fi.name)<0
                                        ?(
                                            <React.Fragment>
                                            <div className="col-6">
                                            {this.makeDD("Select Sizes","size",order[index].size,sizes,index)}
                                            </div>
                                            <div className="col-6">
                                            {this.makeDD("Select Crusts","crust",order[index].crust,crusts,index)}
                                            </div>
                                            </React.Fragment>
                                        ):(
                                            <React.Fragment>
                                            <div className="col-6">
                                            {this.makeDD("Select Sizes","size",cart[cart.findIndex(car=>car.name===fi.name)].size,sizes,0,true)}
                                            </div>
                                            <div className="col-6">
                                            {this.makeDD("Select Crusts","crust",cart[cart.findIndex(car=>car.name===fi.name)].crust,crusts,0,true)}
                                            </div>
                                            </React.Fragment>
                                        )}
                                    </div>
                                    {cart.length===0 || cart.findIndex(cr=>cr.name===fi.name)<0
                                        ?<button className="btn btn-primary m-2" onClick={() => this.addCart(fi)}>Add to Cart</button>
                                        :<React.Fragment>
                                        <button  class="btn btn-danger" onClick={() => this.remove(fi.name)}>
                                        <i className="fas fa-minus"></i>
                                        </button><button class="btn btn-secondary ">{cart.find(cr=>cr.name===fi.name).quantity}</button>
                                        <button class="btn btn-success" onClick={() => this.addCart(fi)}>
                                        <i className="fas fa-plus"></i>
                                        </button>
                                        </React.Fragment>}
                                </div>
                            )}
                        </div>
                    ) :view===2 || view===3
                    ? (
                        <div className="row">
                            {filter.map(fi=>
                            <div className="col-6 text-center">
                                <img src={fi.image} style={{width: "70%"}} /><br/>
                                <label style={{fontWeight: "bold"}}>{fi.name}</label><br/>
                                <label className="mt-0">{fi.desc}</label><br/>
                                
                                    {cart.length===0 || cart.findIndex(cr=>cr.name===fi.name)<0
                                        ?<button className="btn btn-primary m-2" onClick={() => this.addCart2(fi)}>Add to Cart</button>
                                        :<React.Fragment>
                                        <button  class="btn btn-danger" onClick={() => this.remove(fi.name)}>
                                        <i className="fas fa-minus"></i>
                                        </button><button class="btn btn-secondary ">{cart.find(cr=>cr.name===fi.name).quantity}</button>
                                        <button class="btn btn-success" onClick={() => this.addCart(fi)}>
                                        <i className="fas fa-plus"></i>
                                        </button>
                                        </React.Fragment>}
                                </div>
                            )}
                        </div>
                        )
                    :""
                    }
                    </div>
                    {view>=0 ? 
                    <div className="col-4 border">
                        <div className="row">
                            <div className="col-12 text-center">
                                <h6>Cart {cart.length===0 ? "Empty" : ""}</h6>
                            </div>
                            <div className="row">
                                {cart.map(car=>
                                <React.Fragment>
                                    <div className="col-4 text-center">
                                        <img src={car.image} style={{width: "70%", marginTop: "2.5em"}}/>
                                    </div>
                                    <div className="col-8">
                                        <label style={{fontWeight: "bold"}}>{car.name}</label>
                                        <label>{car.desc}</label>
                                        <label style={{fontSize: "18px"}}>{car.size}|{car.crust}</label><br/>
                                        <button  class="btn btn-danger" onClick={() => this.remove(car.name)}>
                                        <i className="fas fa-minus"></i>
                                        </button><button class="btn btn-secondary ">{car.quantity}</button>
                                        <button class="btn btn-success" onClick={() => this.addCart(car)}>
                                        <i className="fas fa-plus"></i>
                                        </button>
                                    </div>
                                    </React.Fragment>
                                    )}
                            </div>
                        </div>
                    </div>
                    : ""}
                </div>
            </div>
        )
    }
    makeDD = (label,name,value,arr,index,bool="") =>{
        console.log(bool,"BOOL")
        return(
        <select id={name} name={name} onChange={(evt) => this.handleChange(index,evt)} value={value} className="form-control" disabled={bool}>
            <option value="" disabled>{label}</option>
            {arr.map(ar=><option>{ar}</option>)}
        </select>
        )
    }
    remove = (name) => {
        let s1 = {...this.state};
        console.log(name)
        let index = s1.cart.findIndex(car=>car.name===name)
        console.log(index)
        if(index>=0){
            if(s1.cart[index].quantity<=1){
                s1.cart.splice(index,1)
            }else{
                s1.cart[index].quantity--
            }
        }
        this.setState(s1)
    }
    
    
}
export default MainComponent;