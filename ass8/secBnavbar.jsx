import React, {Component} from "react";
class NavBar extends Component{
    render(){
        let { handleView,active } = this.props;
        let ss={
            textDecoration: "none",
            color: "white"
        }
        return(
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand">
                MyFavPizza
            </a>
            <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" style={ss} onClick={() => handleView(0)}>
                    Veg Pizzas 
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style={ss} onClick={() => handleView(1)}>
                    Non-Veg Pizzas 
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style={ss} onClick={() => handleView(2)}>
                    Side Dishes 
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style={ss} onClick={() => handleView(3)}>
                    Other Dishes 
                    </a>
                </li>
                </ul>
            </div>
            </nav>
            )
        }
    }
    export default NavBar;