import React, { Component } from "react";
import authservice from "./services/authservice";
import httpSevices from "./services/httpSevices";
class Details extends Component {
  state = {
    form: {gender: "",dob: "",about: ""},
    Data: {day:'',month:"",year:""},
    Day:[],
    Month:["January","February","March","April","May","June","July","August","September","October","November","December"],
    Year:[],
  };
  async componentDidMount(){
      let user = authservice.getUser()
      let response = await httpSevices.get(`/getStudentDetails/${user.name}`)
      let {data} = response
      console.log(data)
      let day=""
      let Month=""
      let year=""
        if(data.dob){
            let startPose=0;
            let indexD = data.dob.indexOf("-",startPose)
            if(indexD>=0){
                for(let i=0; i<indexD; i++){
                    day+=data.dob[i]
                }
            }
            startPose=indexD+1
            let indexM = data.dob.indexOf("-",startPose)
            if(indexM>=0){
                for(let i=indexD+1; i<indexM; i++){
                    Month+=data.dob[i]
                }
            }
                for(let i=indexM+1; i<data.dob.length; i++){
                    year+=data.dob[i]
                }
        
            }
            this.setState({form:data,Data:{day:+day,month:Month,year:+year}})
    }
    handleChange = (e) => {
      let {currentTarget:input} = e;
    let s1 = {...this.state};
    input.type==="checkbox"
    ?s1.customer[input.name] = input.checked
    :input.name==="day" || input.name==="month" || input.name==="year"
    ?s1.Data[input.name] = input.value
    :s1.form[input.name] = input.value
    this.setState(s1)
}
  MakeSDay = (year,day,month,Month,Year,Day) => {
    if(year && ((+year)%4===0 && month==="February")){
        for(let i=1; i<=29; i++){
            Day.push(i)
        }
    }else if(year && ((+year)%4>0 && month==="February")){
        for(let i=1; i<=28; i++){
            Day.push(i)
        }
    }else if(year && (year && (month=="August" || month=="July" || Month.findIndex(mon=> mon===month)%2==0))){
        for(let i=1; i<=31; i++){
            Day.push(i)
        }
    }else if(year && (Month.findIndex(mon=> mon===month)%2==1)){
        for(let i=1; i<=30; i++){
            Day.push(i)
        }
    }
}
handleSubmit=(name)=>{
    let {gender,dob,myself} = this.state.form;
    let {year,day,month} = this.state.Data
    let pr = {name: name,gender: gender,myself: myself,dob : day+"-"+(month)+"-"+year}
    this.postData("/postStudentDetails",pr)
}
async postData(url,obj) {
    try{
    let response = await httpSevices.post(url,obj)
    alert("successfully Inserted")
    }
    catch(ex){
        alert("Sometyhing goes wrong")
    }
}
  render() {
    let {Data,Day,Month,Year,form} = this.state; 
    let {year,day,month} = Data
    Day=[]
    Year=[]
    let user = authservice.getUser()
    for(let i=1995; i<=2010; i++){
        Year.push(i)
    }
    this.MakeSDay(year,day,month,Month,Year,Day)
    return (
      <div className="container">
        <h6>Student Details</h6>
        <div className="row">
          <div className="col-2">
            <h6>Gender</h6>
          </div>
          <div className="col-2"></div>
          <div className="col-2">
            <input
              name="gender"
              type="radio"
              onChange={this.handleChange}
              value="male"
              checked ={form.gender==="male"}
              className="check-input"
            />
            <h6>Male</h6>
          </div>
          <div className="col-2"></div>
          <div className="col-2">
            <input
              name="gender"
              type="radio"
              onChange={this.handleChange}
              value="female"
              checked ={form.gender==="female"}
              className="check-input"
            />
            <h6>Female</h6>
          </div>
          <div className="col-2"></div>
        </div>
        <h6>Date Of Birth</h6>
        <div className="row">
          <div className="col-4">
            {this.DD(Year, "Select Year", year, "year")}
              </div>
          <div className="col-4">
            {this.DD(Month, "Select Month", month, "month")}
          </div>
          <div className="col-4">
          {this.DD(Day, "Select Day", day, "day")}
          </div>
        </div>
        <h6>About MySelf</h6>
        {this.textField("about", form.about, "Enter About")}
        <button className="btn btn-primary" onClick={()=> this.handleSubmit(user.name) } disabled={!this.chekError()}>Add Details</button>

      </div>
    );
  }
  textField = (name,value,label) => {
    return(
        <React.Fragment>
            <input
            type="text"
            className="form-control"
            name={name}
            value={value}
            placeholder={label}
            onChange = {this.handleChange}
            />

        </React.Fragment>
    )
}
chekError = () => {
    let errors = this.validateAll()
    return this.isValid(errors)
}
isValid = (errors) => {
    let keys = Object.keys(errors);
    let count = keys.reduce((acc, curr) => errors[curr] ? acc+1 : acc ,0);
    console.log(count)
    return count === 0;
  };
validateAll = () => {
    let {myself,gender,dob} = this.state.form;
    let {year,day,month} = this.state.Data
    let errors = {}
    errors.date = this.val1(year,day,month);
    errors.gender = this.val2(gender);
    errors.relationship = this.val3(myself);
    console.log(errors)
    return errors
}

val1 = (year,day,month) => 
    year && day && month ? "" : "Please select date"

val2 = (gender) => 
    gender ? "" : "Please select Gender"

val3 = (myself) => 
    myself ? "" : "please enter myself"

DD = (arr,label,value,name,err="") =>{
    return( 
    <React.Fragment>
                <select id={name} name={name} value={value} className="form-control" onChange={this.handleChange}>
                    <option value="" disabled>{label}</option>
                    {arr.map(ar=><option>{ar}</option>)}
                </select>
    <div className="row">
    {err ? <span className="text-danger text-center">{err}</span> : ""}
    </div>
    </React.Fragment>
    )};
}
export default Details;
