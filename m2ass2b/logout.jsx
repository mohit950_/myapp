import React, { Component } from "react";
import authservice from "./services/authservice";
import httpSevices from "./services/httpSevices";
class Logout extends Component{
    async componentDidMount() {
        authservice.logout()
        window.location = "/login"
    }
    render() {
        return ""
    }
}
export default Logout;