import React, {Component} from "react";
import http from "./services/httpSevices"
import queryString from "query-string";
class AllFaculty extends Component{
    state={
        faculty: [],
        Course: ["ANGULAR","JAVASCRIPT","REACT","BOOTSTRAP","CSS","REST AND MICROSERVICES","NODE"],
        courses: {course: ""}
    }
    async componentDidMount() {
        let response = await http.get("/getFaculties?page=1")
        let {data} = response
        console.log(response)
        console.log(data)
        this.setState( { faculty : data } )
    }
    async fetchData() {
        console.log("gvefachkbahdcqviq")
        let queryParams = queryString.parse(this.props.location.search)
        let searchStr = this.makeSearchString(queryParams)
        let response = await http.get(`/getFaculties${searchStr}`)
        let {data} = response
        console.log(response)
        console.log(data)
        this.setState( { students : data } )
    }

    handleChange = (e) => {
        let {currentTarget:input} = e;
        let s1 = {...this.state}
        s1.courses[input.name] = this.updateCbs(input.value,input.checked,s1.courses[input.name])
        this.setState(s1)
        this.callUrl("/allfaculty",s1.courses)
    }
    
    updateCbs = (value,checked,name)=> {
        let inpuArr = name ? name.split(",") : []
        if(checked) inpuArr.push(value)
        else{
            let index = inpuArr.findIndex(fi=> fi===value)
            if(index>=0){
                inpuArr.splice(index,1)
            }
        }
        return inpuArr.join(",")
    }
    callUrl = (url,options) => {
        console.log(options)
        let searchStr = this.makeSearchString(options)
        this.props.history.push({
            pathname: url,
            search: searchStr
        })
    }
    makeSearchString = (options) => {
        let {course,page="1"} = options
        let searchString=""
        searchString = this.addQueryString(searchString,"page",page)
        searchString = this.addQueryString(searchString,"course",course)
        return searchString;
    }
    akeSearchString = (options) => {
        let {course} = options
        let queryParams = queryString.parse(this.props.location.search)
        let {page=1} = queryParams
        let searchString=""
        searchString = this.addQueryString(searchString,"page",page)
        searchString = this.addQueryString(searchString,"course",course)
        return searchString;
    }
    addQueryString=(str,name,value) => 
        value
        ?str
        ?`${str}&${name}=${value}`
        :`${name}=${value}`
        :str

    render(){
        let {faculty,Course} = this.state;
        let {items=[],page,totalItems} = faculty
        return(
            <div className="container">
                <div className="row">
                    <div className="col-3">
                        <h6 className="m-3">Options</h6>
                        {this.CB(Course,"course")}
                    </div>
                    <div className="col-9">
                        <h5>All faculty</h5>
                        <div className="row border text-center">
                            <div className="col-1"><h6>Id</h6></div>
                            <div className="col-2"><h6>Name</h6></div>
                            <div className="col-3"><h6>Date of Birth</h6></div>
                            <div className="col-3"><h6>About</h6></div>
                            <div className="col-3"><h6>Courses</h6></div>
                        </div>
                        {items.map(st=>
                            <div className="row bg-warning border text-center">
                            <div className="col-1">{st.id}</div>
                            <div className="col-2">{st.name}</div>
                            <div className="col-3">{st.dob}</div>
                            <div className="col-3">{st.about}</div>
                            <div className="col-3">{st.courses.map(s=><label>{s}</label>)}</div>
                        </div>
                        )}
                    <div className="row">
                        {page>1?
                        <div className="col-1">
                            <button className="btn btn-warning" onClick={()=>this.handlePage(-1)}>Prev</button>
                        </div>
                        :""}

                        <div className="col-9"></div>
                        {totalItems>=3?
                        <div className="col-2">
                            <button className="btn btn-warning" onClick={()=>this.handlePage(+1)}>Next</button>
                            </div>:""}
                    </div>

                    </div>
                    
                </div>
            </div>
        )
    }
    handlePage = (incr) => {
        let queryParams = queryString.parse(this.props.location.search)
        let {page="1"} = queryParams
        let newpage = +page+incr
        queryParams.page = newpage
        this.callUrl("/allstudents",queryParams)   

    }
    CB = (arr,name,value) => {
        return(
            arr.map(pr=>
                <div className="m-3">
                <input type="checkbox" name={name} value={pr} onChange={this.handleChange} className="check-input" />
                <label>{pr}</label><br/>
                </div>
                )
        )
    }
}
export default AllFaculty;