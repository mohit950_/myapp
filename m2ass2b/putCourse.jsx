import React, {Component} from "react";
import httpSevices from "./services/httpSevices";
class  putCourse extends Component{
    state={
        cor:{name:"",code:"",description:"",students:""},
        std:[],
    }
    async componentDidMount() {
        console.log("data")
        let response = await httpSevices.get("/getCourses")
        let {data} = response
        console.log(data)
        let pt = {name:data.name, code:data.code, description:data.description, students: data.students}
        console.log(pt)
        this.setState( {cor: data} )
    }
    async getData() {
        let response = await httpSevices.get("/getStudentNames")
        let {data} = response
        this.setState({std:data})
    }
    handleChange=(e)=> {
        let {currentTarget:input}= e;
        let s1 = {...this.state}
        s1.cor[input.name] = input.value
        this.setState(s1)
    }
    render(){
        let {cor,std} = this.state;
        let {name,code,description,students} = cor
        if(std.length===0)this.getData()
        return(
            <div className="container">
                <h6>Add Students to Course</h6>
                <br/>
                <h6>Edit the Course</h6>
                {this.textField("Name","name",name)}
                {this.textField("Course Code","code",code)}
                {this.textField("Description","description",description)}
                {this.CB(std,"students",students)}
                <button className="brn btn-primary">Submit</button>

            </div>        
        )
    }
    textField=(label,name,value,holder,err="")=>{
        return(
            <div className="row">
                
                <label>{label}</label>
                <input
                type="text"
                name={name}
                placeholder={holder}
                className="form-control"
                onBlur={this.validate}
                onChange={this.handleChange}
                />
                {err ? <span className="bg-danger text-light">{err}</span> : ""}
            </div>
        )
    }
    CB = (arr,name,value,err="") => {
        console.log(value)
        console.log(arr)
        return(
            <React.Fragment>
            <label>Students<i className="fa fa-asterisk" style={{fontSize:"5px",color:"red",position: "relative",bottom: "30%"}}></i></label>
            {arr.map(ar=>
                <div className="m-3">
                <input type="checkbox" name={name} value={ar} onChange={this.handleChange} className="check-input" />
                <label>{ar}</label>
                {err ? <span className="bg-danger text-light">{err}</span>:""}
                </div>
            )}
            </React.Fragment>
        )
    }
}

export default putCourse;