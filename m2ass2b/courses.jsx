import React, {Component} from "react";
import authservice from "./services/authservice";
import httpSevices from "./services/httpSevices";
class Courses extends Component{
    state={
        data: []
    }
    async componentDidMount(){
        let user = authservice.getUser()
        let response = await httpSevices.get(`/getFacultyCourse/${user.name}`)
        let {data} = response
        this.setState({data:data})
    }
    render(){
        let {data} = this.state;
        return(
            <div className="container">
                <h6>Course Assigned</h6>
                <div className="row border bg-light">
                    <div className="col-2">Course Id</div>
                    <div className="col-2">Course Name</div>
                    <div className="col-2">Course Code</div>
                    <div className="col-6">Description</div>
                </div>
                {data.map(dt=>
                    <div className="row border bg-primary">
                    <div className="col-2">{dt.courseId}</div>
                    <div className="col-2">{dt.name}</div>
                    <div className="col-2">{dt.code}</div>
                    <div className="col-6">{dt.description}</div>
                </div>
                    )}
            </div>
        )
    }
}
export default Courses;