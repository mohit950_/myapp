import React, {Component} from "react"
import authservice from "./services/authservice"
import httpSevices from "./services/httpSevices"
class AllSchedule extends Component{
    state={
        data:[]
    }
    async componentDidMount(){
        let user = authservice.getUser()
        let response = await httpSevices.get(`/getFacultyClass/${user.name}`)
        let {data} = response
        this.setState({data:data})
    }
    handleEdit=(id)=>{
        if(!id) this.props.history.push(`/addSchedule`)
        else this.props.history.push(`/addSchedule/${id}`)
    }
    render(){
        let {data} = this.state;
        let user = authservice.getUser()
        return(
            <div className="container">
                <h5> All Scheduled Classes</h5>
                <div className="row border bg-light">
                    <div className="col-2">Course Name</div>
                    <div className="col-2">Start Time</div>
                    <div className="col-2">End Time</div>
                    <div className="col-3">Topic</div>
                    <div className="col-3"></div>
                </div>
                {data.map(dt=>
                    <div className="row border bg-warning">
                    <div className="col-2">{dt.course}</div>
                    <div className="col-2">{dt.time}</div>
                    <div className="col-2">{dt.endTime}</div>
                    <div className="col-3">{dt.topic}</div>
                    <div className="col-3">
                        <button className="btn btn-secondary btn-sm" onClick={()=>this.handleEdit(dt.classId)}>Edit</button>

                    </div>

                </div>
                    )}
                        <button className="btn btn-primary btn-sm" onClick={()=>this.handleEdit()}>Add Schedule</button>
            </div>
        )
    }
}
export default AllSchedule;