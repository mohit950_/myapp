import React, {Component} from "react"
import httpSevices from "./services/httpSevices"
class SttoCourse extends  Component{
    state={
        data: [],
    }
    async componentDidMount(){
        let response = await httpSevices.get("/getCourses")
        let {data} = response
        console.log(data)
        this.setState({ data : data})
    }
    render(){
        let {data} =this.state;
        return(
            <div className="container">
                <h6>Add Student to Course</h6>
                <div className="row border">
                    <div className="col-1" style={{fontWeight: "bold"}}>Id</div>
                    <div className="col-2" style={{fontWeight: "bold"}}>Name</div>
                    <div className="col-2" style={{fontWeight: "bold"}}>Course Code</div>
                    <div className="col-3" style={{fontWeight: "bold"}}>Description</div>
                    <div className="col-2" style={{fontWeight: "bold"}}>Students</div>
                    <div className="col-2"></div>
                </div>
                {data.map(it=>
                    <div className="row border bg-warning">
                        <div className="col-1">{it.courseId}</div>
                        <div className="col-2">{it.name}</div>
                        <div className="col-2">{it.code}</div>
                        <div className="col-3">{it.description}</div>
                        <div className="col-2">
                            {it.students.map(st=>
                            <React.Fragment>
                            <label>{st}</label><br/>
                            </React.Fragment>
                            )}
                        </div>
                        <div className="col-2">
                            <button className="btn btn-secondary" onClick={()=>this.handleEdit()}>Edit</button>
                        </div>
                    </div>
                    )}
            </div>

        )
    }
    handleEdit = () => {
        this.props.history.push("/putCourse")
    }
}
export default SttoCourse;