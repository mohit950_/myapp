import React, {Component} from "react";
import { Route, Switch } from "react-router-dom";
import NavBar from "./navbar";
import Login from "./login";
import authservice from "./services/authservice";
import Admin from "./Admin"
import Logout from "./logout";
import AllStudents from "./Allstudents"
import AllFaculty from "./Allfaculty"
import Register from "./register";
import SttoCourse from "./studentToCourse"
import putCourse from "./putCourse"
import FTtoCourse from "./facultytocourse";
import AllCourse from "./allcourse"
import AllClass from "./allclasses"
import Details from "./studentDetails"
import Courses from "./courses";
import AllSchedule from "./allscheduledclasses"
import addSchedule from "./addSchedule"
class MainComponent extends Component{
    render() {
        let user = authservice.getUser()
        return(
            <div className="container">
            <NavBar />
            <Switch>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path="/logout" component={Logout}/>
                <Route path="/admin" component={Admin} />
                <Route path="/addStudent" component={SttoCourse}/>
                <Route path="/addFaculty" component={FTtoCourse}/>
                <Route path="/putCourse" component={putCourse}/>
                <Route path="/student" component={AllStudents} />
                <Route path="/allfaculty" component={AllFaculty} />

                <Route path="/allCourse" component={AllCourse} />
                <Route path="/allClasses" component={AllClass} />
                <Route path="/studentDetails" component={Details} />

                <Route path="/courses" component={Courses} />
                <Route path="/allSchedulkedClasses" component={AllSchedule} />
                <Route path="/addSchedule" component={addSchedule} />
                
            </Switch>

            </div>
        )
    }
}
export default MainComponent;