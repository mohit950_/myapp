import React, { Component } from "react";
import { Navbar,Container,Nav,NavDropdown } from 'react-bootstrap'
import { Link } from "react-router-dom";
import authservice from "./services/authservice"
class NavBar extends Component {
  render() {
     let user = authservice.getUser()
    return (
        <Navbar collapseOnSelect expand="lg" bg="success" variant="light">
        <Container>
        <Navbar.Brand>Home</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
        {user && user.role==="admin"
            ?(
            <React.Fragment>
            <Nav.Link href="/register">Register</Nav.Link>
            <NavDropdown title="Admin" id="collasible-nav-dropdown">
            <NavDropdown.Item href="addStudent">Student to Course</NavDropdown.Item>
            <NavDropdown.Item href="/addFaculty">Faculty to Course</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="View" id="collasible-nav-dropdown">
            <NavDropdown.Item href="/allstudents">All Students</NavDropdown.Item>
            <NavDropdown.Item href="/allfaculty">All Faculty</NavDropdown.Item>
            </NavDropdown>
            </React.Fragment>
            )
            :user && user.role==="student"
            ?(
              <React.Fragment>
              <Nav.Link href="/studentDetails">Student Details</Nav.Link>
              <Nav.Link href="/allClasses">All Classes</Nav.Link>
              <Nav.Link href="/allCourse">All Course</Nav.Link>
              </React.Fragment>
            )
            :user && user.role==="faculty"
            ?(
              <React.Fragment>
              <Nav.Link href="/courses">Courses</Nav.Link>
              <NavDropdown title="Class Details" id="collasible-nav-dropdown">
            <NavDropdown.Item href="/addSchedule">Schedule a Class</NavDropdown.Item>
            <NavDropdown.Item href="/allSchedulkedClasses">All Schedule Classes</NavDropdown.Item>
            </NavDropdown>
            </React.Fragment>
            )
            : ""
        }
            {/* <Nav.Link href="#pricing">Pricing</Nav.Link>
            <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown> */}
          </Nav>
          <Nav>
            {!user
            ?<Nav.Link href="/login">Login</Nav.Link>
            :user && user.role==="admin"
            ?(
            <React.Fragment>
            <Nav.Link>Welcome Admin</Nav.Link>
            <Nav.Link eventKey={2} href="/logout">Logout</Nav.Link>
            </React.Fragment>
            )
            :(
            <React.Fragment>
                <Nav.Link>Welcome {user.name}</Nav.Link>
                <Nav.Link eventKey={2} href="/logout">Logout</Nav.Link>
            </React.Fragment>
            )
            }
          </Nav>
        </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
}
export default NavBar;
