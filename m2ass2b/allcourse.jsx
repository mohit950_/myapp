import React, {Component} from "react";
import authservice from "./services/authservice";
import httpSevices from "./services/httpSevices";
class AllCourse extends Component{
    state={
        data:[]
    }
    async componentDidMount() {
        let user = authservice.getUser()
        console.log(user.name)
        let res = await httpSevices.get(`/getStudentCourse/${user.name}`)
        let {data} =  res
        console.log(data)
        console.log(res)
        this.setState({ data: data })
    }

    render(){
        let {data} = this.state;
        return(
            <div className="container">
                <h5>Course Assigned</h5>
                <div className="row border bg-light">
                    <div className="col-2">Course Id</div>
                    <div className="col-2">Course Name</div>
                    <div className="col-2">Course Code</div>
                    <div className="col-6">Description</div>
                </div>
                {data.map(dt=>
                    <div className="row border bg-primary">
                    <div className="col-2">{dt.courseId}</div>
                    <div className="col-2">{dt.name}</div>
                    <div className="col-2">{dt.code}</div>
                    <div className="col-6">{dt.description}</div>
                </div>
                    )}
            </div>
        )
    }
}
export default AllCourse;