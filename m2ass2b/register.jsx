import React, {Component} from "react";
import http from "./services/httpSevices"
class Register extends Component{
    state={
        form: {name:"",password: "",cpassword: "",email: "",role: ""},
        errors: {},
        Role: ["Faculty","Student"]
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let errors = this.validateAll()
        if(this.isFormValid(errors)){
            this.postData("/register",this.state.form)
        }
    }
    async postData(url,obj) {
        try{
            let response = await http.post(url,obj)
            alert("Succesfully Submitted")
            this.setState({from:{name:"",password: "",cpassword: "",email: "",role: ""}})
        }
        catch(ex){
            alert("Something Went wrong")
        }
    }
    validateAll = () => {
        let { name, password, cpassword, email, role, } = this.state.form;
        let errors = {};
        errors.name = this.nameError(name);
        errors.password = this.namePassword(password);
        errors.cpassword = this.nameCpassword(cpassword);
        errors.email = this.nameEmail(email);
        errors.role = this.nameRole(role);
        return errors;
      }
      isValid = (errors) => {
        let keys = Object.keys(errors);
        let count = keys.reduce((acc, curr) => errors[curr] ? acc+1 : acc ,0);
        console.log(count)
        return count === 0;
      };
    isFormValid = () => {
        let errors = this.validateAll();
        return this.isValid(errors)
    }
    render() {
        let {name,password,cpassword,email,role} = this.state.form;
        let {errors,Role} = this.state
        return(
            <div className="container">
                <h6>Register</h6>
                {this.textField("Name","name",name,"Please enter name",errors.name)}
                {this.textField("Password","password",password,"Please enter password",errors.password)}
                {this.textField("Cpassword","cpassword",cpassword,"Please enter Re-enter password",errors.cpassword)}
                {this.textField("Email","email",email,"Please enter email",errors.email)}
                {this.CB(Role,"role",role,errors.role)}
                <button className="btn btn-primary" onClick={this.handleSubmit} disabled={!this.isFormValid()}>Submit</button>
            </div>
        )
    }

    handleChange = (e) => {
        const {currentTarget:input}=e;
        let s1 = {...this.state};
        s1.form[input.name] = input.value;
        this.setState(s1)
        console.log(s1.form)
    };
    // handleSubmit = (e) => {
    //     e.preventDefault();
    //     this.postData("/register",this.state.form)
        
    // }
    validate = (e) => {
        let {currentTarget:input} = e;
        let s1 = {...this.state}
        console.log(input.name)
        switch (input.name) {
            case "name":
                s1.errors.name = this.nameError(input.value)
                break;
            case "password":
                s1.errors.password = this.namePassword(input.value)
                break;
            case "cpassword":
                s1.errors.cpassword = this.nameCpassword(input.value)
                break;
            case "email":
                s1.errors.email = this.nameEmail(input.value)
                break;
            case "role":
                s1.errors.role = this.nameRole(input.role)
                break;
            default:
                break;
        }
        this.setState(s1)
        console.log(this.state.errors)
    }
    nameError=(name)=>
    name ? "" : "Please Enter Name"

    namePassword=(password)=>
    password ? "" : "Please Enter Password"

    nameCpassword=(cpassword)=>{
        if(cpassword){
            if(cpassword===(this.state.form.password)){
                return ""
            }
            else{
                return "Password is not match"
            }
        }
        else{
            return "Please Enter Cpassword"
        }
    }
    nameEmail=(email)=>
    email ? "" : "Please Enter Email"

    nameRole=(role)=>
    role ? "" : "Please Select Role"

    textField=(label,name,value,holder,err="")=>{
        return(
            <div className="row">
                
                <label>{label}<i className="fa fa-asterisk" style={{fontSize:"5px",color:"red",position: "relative",bottom: "30%"}}></i></label>
                <input
                type="text"
                name={name}
                placeholder={holder}
                className="form-control"
                onBlur={this.validate}
                onChange={this.handleChange}
                />
                {err ? <span className="bg-danger text-light">{err}</span> : ""}
            </div>
        )
    }
    CB = (arr,name,value,err="") => {
        return(
            arr.map(ar=>
                <div className="m-3">
                <input type="checkbox" name={name} value={ar} onChange={this.handleChange} className="check-input" />
                <label>{ar}</label><br/>
                {err ? <span className="bg-danger text-light">{err}</span>:""}
                </div>
            )
        )
    }
}
export default Register;