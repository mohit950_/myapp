import React, {Component} from "react";
import httpSevices from "./services/httpSevices";
class addSchedule extends Component{
    state={
        form: {course:"",time: "",endTime: "",topic: ""},
        Courses: ["ANGULAR","REACT","BOOTSTRAP"]
    }
    async componentDidMount() {
        console.log(this.props)
        let id = this.props.match.params
        console.log(this.props.match.params)
        if(id){
            let response = await httpSevices.get(`/getClassName/1`)
            let {data} = response
            console.log(data)
            this.setState({form:data})
        }
        else{
            this.setState({form:{course:"",time: "",endTime: "",topic: ""}})
        }
    }
    handleChange = (e) => {
        let {currentTarget:input} = e;
        let s1 = {...this.state}
        s1.form[input.name] = input.value
        this.setState(s1)
        console.log(s1.form)
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        this.postData("/postClass",this.state.form)
    }
    async postData(url,obj) {
        try{
        let response = await httpSevices.post(url,obj)
        alert("succesfully Inserted")
        window.location = "/allSchedulkedClasses"
        }
        catch(ex){
            alert("Something went Wrong")
        }

    }
    render(){
        let {Courses,form} = this.state;
        let {course,time,endTime,topic} = form
        return(
            <div className="container">
                <select name="course" value={course} className="form-control mt-2" onChange={this.handleChange}>
                    <option value="" disabled>Select Course</option>
                    {Courses.map(cr=><option>{cr}</option>)}
                </select>
                {this.timeField("Time","time",time)}
                {this.timeField("End Time","endTime",endTime)}
                {this.textField("Topic","topic",topic,"Enter Class Topic")}
                <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
            </div>
        )
    }
    timeField=(label,name,value,holder,err="")=>{
        return(
            <div className="row">
                
                <label>{label}<i className="fa fa-asterisk" style={{fontSize:"5px",color:"red",position: "relative",bottom: "30%"}}></i></label>
                <input
                type="time"
                name={name}
                placeholder={holder}
                className="form-control"
                onBlur={this.validate}
                onChange={this.handleChange}
                />
                {err ? <span className="bg-danger text-light">{err}</span> : ""}
            </div>
        )
    }
    textField=(label,name,value,holder,err="")=>{
        return(
            <div className="row">
                
                <label>{label}<i className="fa fa-asterisk" style={{fontSize:"5px",color:"red",position: "relative",bottom: "30%"}}></i></label>
                <input
                type="text"
                name={name}
                placeholder={holder}
                className="form-control"
                onBlur={this.validate}
                onChange={this.handleChange}
                />
                {err ? <span className="bg-danger text-light">{err}</span> : ""}
            </div>
        )
    }
}
{/* <input type="time" id="appt" name="appt"></input> */}
export default addSchedule;