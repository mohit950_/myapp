1:- addschedule.jsx //use for adding the schedule by faculty
2:- Admin.jsx // showing Admin dashboard
3:- allclasses.jsx // student class showing by student name
4:- allcourse.jsx // student courses showing by student name
5:- AllFaculty.jsx // all Faculty Table
6:- allscheduleclasses.jsx // all schedule and class by faculty
7:- Allstudents.jsx // all students table
8:- courses.jsx // faculty courses by name
9:- facultytocourse.jsx // add faculty to course
10:- login.jsx // for login (admin,student,faculty)
11:- logout.jsx // for loguut (admin,student,faculty)
12:- main.jsx // main file Route file
13:- navbar.jsx // navbar
14:- putCourse // for course put
15:- register.jsx // register form student or faculty by admin
16:- studentDetails.jsx // Details of student
17:- studenttocourse.jsx // add student to course
18:- server.js // api or server