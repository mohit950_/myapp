import React, {Component} from "react"
import authservice from "./services/authservice"
import http from   "./services/httpSevices"
class Login extends Component{
    state={
        form:  {email: "",password: ""},
        errors: null,
        edit: false,

    };
    async login(url,obj){
        try{
            let user = authservice.getUser()
            let response = await http.post(url,obj);
            let { data } = response;
            authservice.login(data);
            window.location = `/${user.role}`
        }
        catch (ex){
            if(ex.response && ex.response.status===401){
                let errors = {}
                errors = ex.response.data 
                console.log(errors)
                this.setState( { errors : errors } )
            }
        }
    }
    handleChange = (e) => {
        const {currentTarget:input}=e;
        let s1 = {...this.state};
        s1.form[input.name] = input.value;
        this.setState(s1)
        console.log(s1.form)
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.login("/login",this.state.form)
        
    }
    render() {
        let {email,password} = this.state.form
        let {errors=null} = this.state;
        return(
            <div className="container">
                <h5 className="text-center mt-3">Login</h5>
                <h6 className=" text-center text-danger">{errors}</h6>
            <div className="row">
                <div className="col-4"></div>
                <div className="col-1 text-center">
                    <h6>Email: </h6>
                </div>
                <div className="col-5">
                    <input
                    type="text"
                    name="email"
                    id="email"
                    className="form-control"
                    value={email}
                    placeholder="Enter Your email"
                    onChange={this.handleChange}
                    />
                    {errors && errors.email && <span className="text-danger">{errors.email}</span>}<br/>
            </div>
            <div className="col-7">
            </div>
            </div>
            <div className="row">
                <div className="col-4"></div>
                <div className="col-1 text-center">
                    <h6>Password: </h6>
                </div>
                <div className="col-5">
                    <input
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    value={password}
                    placeholder="Enter Your Password"
                    onChange={this.handleChange}
                    />
            </div>
        </div>
        <div className="row text-center">
            <div className="col-5"></div>
            <div className="col-2">
                <button className="btn btn-primary m-2 text-center" onClick={this.handleSubmit}>Login</button>
            </div>
            <div className="col-5"></div>
        </div>
            </div>
        )
    }
}
export default Login;
