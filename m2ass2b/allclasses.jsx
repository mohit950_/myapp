import React, {Component} from "react";
import authservice from "./services/authservice";
import httpSevices from "./services/httpSevices";
class AllClases extends Component{
    state={
        data:[]
    }
    async componentDidMount() {
        let user = authservice.getUser()
        console.log(user.name)
        let res = await httpSevices.get(`/getStudentClass/${user.name}`)
        let {data} =  res
        console.log(data)
        console.log(res)
        this.setState({ data: data })
    }

    render(){
        let {data} = this.state;
        return(
            <div className="container">
                <h5>All Scheduled Classes</h5>
                <div className="row border bg-light">
                    <div className="col-2">Course Name</div>
                    <div className="col-2">Start Time</div>
                    <div className="col-2">End Time</div>
                    <div className="col-3">Faculty Name</div>
                    <div className="col-3">Topic</div>
                </div>
                {data.map(dt=>
                    <div className="row border bg-warning">
                    <div className="col-2">{dt.course}</div>
                    <div className="col-2">{dt.time}</div>
                    <div className="col-2">{dt.endTime}</div>
                    <div className="col-3">{dt.facultyName}</div>
                    <div className="col-3">{dt.topic}</div>
                </div>
                    )}
            </div>
        )
    }
}
export default AllClases;