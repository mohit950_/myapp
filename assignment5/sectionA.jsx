import React, {Component} from "react";
class SectionA extends Component{
    state={
        products: [
            {"product":"Pepsi", "sales":[2,5,8,10,5]},
            {"product":"Coke", "sales":[3,6,5,4,11,5]},
            {"product":"5Star", "sales":[10,14,22]},
            {"product":"Maggi", "sales":[3,3,3,3,3]},
            {"product":"Perk", "sales":[1,2,1,2,1,2]},
            {"product":"Bingo", "sales":[0,1,0,3,2,6]},
            {"product":"Gems", "sales":[3,3,1,1]}  
        ],
        details:-1,
    };
    sort = (num) => {
        let s1 = {...this.state};
        if(num==0){
            s1.products.sort((a1,a2) => a1.product.localeCompare(a2.product));
        }
        else if(num==1){
            s1.products.sort((a1,a2) => a1.total-a2.total);
        }else if(num==2){
            s1.products.sort((p1,p2) => p2.total-p1.total);
        }
        this.setState(s1)
    }
    Details = (index) => {
        let s1 = {...this.state};
        s1.details=index;
        this.setState(s1)
    }
    render() {
        let {products,details} = this.state;
        let total = products.map((ele,index) =>  
            products[index].total=(ele.sales.reduce((acc,curr) => curr+acc ,0)))
        return(
            <div className="container">
                <button className="btn btn-primary m-1 btn-sm" onClick={() => this.sort(0)}>Sort By Product</button>
                <button className="btn btn-primary m-1 btn-sm" onClick={() => this.sort(1)}>Total Sales Asc</button>
                <button className="btn btn-primary m-1 btn-sm" onClick={() => this.sort(2)}>Total Sales Desc</button>
                <br/>
                <div className="row text-center bg-primary">
                    <div className="col-4 border">Product</div>
                    <div className="col-4 border">Total Sale</div>
                    <div className="col-4 border">Details</div>
                </div>
            <div className="row bg-light text-center">
                {products.map((pr,index)=>
                <React.Fragment>
                    <div className="col-4 border">{pr.product}</div>
                    <div className="col-4 border">
                        {pr.total}
                    </div>
                    <div className="col-4 border">
                        <button className="btn btn-primary btn-sm"onClick={() => this.Details(index)}>Details</button>
                    </div>
                    </React.Fragment>
                )}
                        </div>
                {details>=0 ? (
                    <ul>
                        <li>
                            Product : {products[details].product} Sales : {products[details].sales.join(",")}
                        </li>
                    </ul>
                ) : (
                    ""
                )}
    </div>
        )
    }
}
export default SectionA;