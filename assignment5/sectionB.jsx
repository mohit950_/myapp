import React, {Component} from "react";
import {getQuizQuestions} from "./cquestions";
class SectionB extends Component{
    state={
        person:[
            {name: "James", points: 0},
            {name: "Julia", points: 0},
            {name: "Martha", points: 0},
            {name: "Jack", points: 0},
        ],
        questions: getQuizQuestions(),
        currentQuestion: 0,
        playerBuzzer: null,
        
    };
    checkAnswers = (current,index,buzzer) => {
        console.log(current,index)
        let s1 = {...this.state};
        console.log(buzzer)
        console.log(s1.questions[current].answer)
        if(index+1 === s1.questions[current].answer && buzzer>=0){
            alert("Correct Answer. You get 3 points")
            s1.person[buzzer].points = s1.person[buzzer].points+3
            s1.currentQuestion++;
            s1.playerBuzzer=null
        }else if(buzzer>=0){
            alert("Wrong Answer. You loose 1 point")
            s1.person[buzzer].points = s1.person[buzzer].points-1
            s1.currentQuestion++;
            s1.playerBuzzer=null
        }
        this.setState(s1)
    }
    giveAnswer = (index) => {
        let s1 = {...this.state}
        s1.playerBuzzer=index;
        this.setState(s1)
    }
    getcolor = (playerBuzzer,index) => 
        playerBuzzer==index ? "bg-success" : "bg-warning"
    
    render() {
        let {person,questions,currentQuestion,playerBuzzer} = this.state;
        let score = person.reduce((acc,curr) => acc.points>curr.points ? acc : curr)
        let filter = person.filter(fi => fi.points===score.points)
        return(
            <div className="container text-center">
                <h4>Welcome to the Quiz Contest</h4>
                <h5>Participants</h5>
                <div className="row">
                    {person.map((pr,index)=>
                    <div className={"col-3 border text-center "+this.getcolor(playerBuzzer,index)}>
                            <h6>Name : {pr.name}</h6>
                            <h6>Score : {pr.points}</h6>
                            <button className="btn btn-light" onClick={() => this.giveAnswer(index)}>Buzzer</button>
                        </div>
                    )}
                </div>
                {questions.map(qns=><h6>{qns.txt}</h6>)}
                {currentQuestion<6
                ? (
                    <React.Fragment>
                        <h5>Question Number : {currentQuestion+1}</h5>
                        <h6>{questions[currentQuestion].text}</h6>
                        {questions[currentQuestion].options.map((opt,index)=>
                            <button className="btn btn-primary mx-2 btn-sm" onClick={() => this.checkAnswers(currentQuestion,index,playerBuzzer)}>{opt}</button>
                            )}
                    </React.Fragment>
                ):(
                    <h5>Game Over</h5>
                    )}
                {currentQuestion===6 && filter.length===1 
                ? "The winner is,"+filter.map(fi=>fi.name)
                : currentQuestion===6 && filter.length>1
                ? "The winner is,"+filter.map(fi=>fi.name)
                : ""}
            </div>
        )
    }
}
export default SectionB;